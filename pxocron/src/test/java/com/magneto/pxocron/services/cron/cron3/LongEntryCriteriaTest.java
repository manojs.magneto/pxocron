package com.magneto.pxocron.services.cron.cron3;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.magneto.pxocron.dao.ChartsAndTradesDao;
import com.magneto.pxocron.entities.ChartDetails;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.entities.TradeDetails;
import com.magneto.pxocron.repositories.Generationcharts;
import com.magneto.pxocron.repositories.TradeDetailsRepo;
import org.mockito.AdditionalMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class LongEntryCriteriaTest {

	@Mock
	TradeDetailsRepo tradeDetailsRepo;
	
	@Mock
	Generationcharts chartData;
	 

	@InjectMocks
	ChartsAndTradesDao chartsAndTrades;
	
	
	
	

	@BeforeEach
	void init_mocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGeneratinLongEntry() {

		System.out.println("long entry->");

		List<TradeDetails> tradeList = Arrays.asList(
				new TradeDetails(345, "2021-09-29", "AAPL", 67.0, 56.0, "green", 0),

				new TradeDetails(678, "2021-09-29", "TSLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "CLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "PLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "GLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "LLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "SLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "MLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "XLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "MsLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "QLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "TPLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "POLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "OPLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "KLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "SkLA", 98.0, 44.0, "green", 0),
				new TradeDetails(678, "2021-09-29", "ANGLA", 98.0, 44.0, "green", 0)

		);
		List<ChartDetails> chartList = Arrays.asList(
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")));

		when(tradeDetailsRepo.findAllByDate(anyString())).thenReturn(tradeList);
		when(chartData.findAllByDate(anyString())).thenReturn(chartList);

		// tradeDetailsRepo.findAllByDate("currentDate")
		List<TradeDetails> tradeDetailsList = chartsAndTrades.tradeData("currentDate");
		List<ChartDetails> chartsList = chartsAndTrades.chartData("yesterDAte");

		Map<String, Double> tempvalues = tradeDetailsList.stream()
				.collect(Collectors.toMap(TradeDetails::getSymbol, TradeDetails::getTempentryHighValue));

		Map<String, Integer> countValues = tradeDetailsList.stream()
				.collect(Collectors.toMap(TradeDetails::getSymbol, TradeDetails::getCountdays));

		tradeDetailsList.forEach(eachTrade ->

		{
			System.out.println("eachTrade" + eachTrade.toString());

		});

		assertEquals(17, tradeDetailsList.size());

		chartList.forEach(eachChart ->

		{
			System.out.println("eachChart" + eachChart.toString());
			System.out.println("eachChart symbol data" + eachChart.getSymbolData());

		});

	

	

	} 

}

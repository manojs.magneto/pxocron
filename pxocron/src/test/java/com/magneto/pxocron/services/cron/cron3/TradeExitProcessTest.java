package com.magneto.pxocron.services.cron.cron3;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import com.magneto.pxocron.entities.ChartDetails;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.entities.TradeReport;
import com.magneto.pxocron.repositories.Generationcharts;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.repositories.TradeReportRepo;

class TradeExitProcessTest {

	List<TradeReport> tradereportList;

	List<SymbolData> symbolDataList;

	List<ChartDetails> chartDetailList;


	@Mock
	Generationcharts generationCharts;
	@Mock
	TradeReportRepo tradeReportRepo;
	@Mock
	SymbolDataRepo symbolDataRepo; 
	@InjectMocks
	TradeExitProcess tradeExitProcess;
	@BeforeEach
	void init_mocks() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Before
	public void setUp() {

		tradereportList = Arrays.asList(
				new TradeReport(3475, "AAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "BAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "CAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "DAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "EAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "FAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "GAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "HAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "IAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "JAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "KAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "LAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "MAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "NAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "OAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "PAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "QAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"));

		chartDetailList = Arrays.asList(
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "BAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "BAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "CAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "CAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "DAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "DAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "EAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "EAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "FAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "FAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "GAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "GAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "HAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "HAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "GLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "GLA", 100.5, 100.5, 100.5, 100.5, "2021-12-12")),
				new ChartDetails(122342, "MsLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "MsLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "PLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "PLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "OPLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "OPLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "LLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "LLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "XLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "XLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")));
		symbolDataList = Arrays.asList(

				new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "BAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "CAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "DAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "EAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "FAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "GAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "HAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "IAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "JAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "KAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "LAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "MAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "NAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "OAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "PAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")

		);

	} 

	@Test
	void testTargetProfitAndstopLossCalculation() {
		tradereportList = Arrays.asList(
				new TradeReport(3475, "AAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "BAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "CAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "DAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "EAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "FAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "GAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "HAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "IAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "JAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "KAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "LAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "MAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"),
				new TradeReport(3475, "NAPL", "2021-12-12", null, 98.0, 0.0d, 0.0d, 0.0d, "todayDate", "long"));

		chartDetailList = Arrays.asList(
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "BAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "BAPL", 35.5, 68.0, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "CAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "CAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "DAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "DAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "EAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "EAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "FAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "FAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "GAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "GAPL", 35.5, 35.5, 34.0, 35.5, "2021-12-12")),
				new ChartDetails(122342, "HAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "HAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "IAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "IAPL", 100.5, 100.5, 100.5, 100.5, "2021-12-12")),
				new ChartDetails(122342, "JAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "JAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "KAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "KAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "LAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "LAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "MAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "MAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "NAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "NAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")));
		symbolDataList = Arrays.asList(

				new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "BAPL", 35.5, 68.0, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "CAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "DAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "EAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "FAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "GAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "HAPL", 35.5, 35.5, 34.0, 35.5, "2021-12-12"),
				new SymbolData(1234, "IAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "JAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "KAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "LAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "MAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12"),
				new SymbolData(1234, "NAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")
				);


		when(generationCharts.findAllByDate(anyString())).thenReturn(chartDetailList);
		when(tradeReportRepo.findAllByDate(anyString())).thenReturn(tradereportList);
		when(symbolDataRepo.findAllByEodDate(anyString())).thenReturn(symbolDataList);
		
		
		tradeExitProcess.targetProfitAndstopLossCalculation();
		
		
		
		
		

	}

	@Test
	void testExitCalculationforLongTrades() {

		//fail("Not yet implemented");
	}

	@Test
	void testExitCalculationforShortTrades() {
		//fail("Not yet implemented");
	}

	@Test
	void testProfitCalculation() {
		//fail("Not yet implemented");
	}

}

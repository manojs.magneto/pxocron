package com.magneto.pxocron.services;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import com.magneto.pxocron.PxocronApplication;
import com.magneto.pxocron.dao.StockExchangeDao;
import com.magneto.pxocron.dto.StockDetails;
import com.magneto.pxocron.repositories.StockDetailsRepo;
class StochasticCalculatorTest {
	
	@Mock
	StockExchangeDao stockExchangeDao;
	@InjectMocks
	StochasticCalculator stochasticCalculator;
	
	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testStochasticFormulaCalculatorwithDifferentDates() throws Exception {
		
		StockDetails[] stockDetails=new StockDetails[] {
				
				new StockDetails(Integer.valueOf(1), "0.445", "0.445", "AATV", "2020-10-16", "US", "0.445", "0.445", "0.445", "0")	,
				new StockDetails(Integer.valueOf(2), "0.445", "0.445", "AATV", "2020-10-15", "US", "0.445", "0.445", "0.445", "0"),
				new StockDetails(Integer.valueOf(3), "106.7", "106.7", "A", "2020-10-14", "US", "107.54", "105.71", "105.95", "1039400"),
				new StockDetails(Integer.valueOf(4), "12.62", "12.62", "AA", "2020-10-13", "US", "12.66", "12.03", "12.31", "5378000"),
				new StockDetails(Integer.valueOf(5), "24.91", "24.91", "AAA", "2020-10-12", "US", "24.91", "24.9", "24.906", "600")

		};
		
		when(stockExchangeDao.StockRepoInvoker()).thenReturn( Arrays.asList(stockDetails));
		//stochasticCalculator.highestOfHighDaysCalculation();
		
	
	}
	
	
	@Test
	void testStochasticFormulaCalculatorwithSameDates() throws Exception {
		
		StockDetails[] stockDetails=new StockDetails[] {
				
				new StockDetails(Integer.valueOf(1), "0.445", "0.445", "AATV", "2020-10-15", "US", "0.445", "0.445", "0.445", "0")	,
				new StockDetails(Integer.valueOf(2), "0.445", "0.445", "AATV", "2020-10-15", "US", "0.445", "0.445", "0.445", "0"),
				new StockDetails(Integer.valueOf(3), "106.7", "106.7", "A", "2020-10-15", "US", "107.54", "105.71", "105.95", "1039400"),
				new StockDetails(Integer.valueOf(4), "12.62", "12.62", "AA", "2020-10-15", "US", "12.66", "12.03", "12.31", "5378000"),
				new StockDetails(Integer.valueOf(5), "24.91", "24.91", "AAA", "2020-10-15", "US", "24.91", "24.9", "24.906", "600")

		};
		
		when(stockExchangeDao.StockRepoInvoker()).thenReturn( Arrays.asList(stockDetails));
		//stochasticCalculator.highestOfHighforLast13DaysCalculation();
		
	
	}


}

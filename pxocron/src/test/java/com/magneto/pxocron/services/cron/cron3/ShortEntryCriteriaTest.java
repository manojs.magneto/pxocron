package com.magneto.pxocron.services.cron.cron3;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.magneto.pxocron.dao.ChartsAndTradesDao;
import com.magneto.pxocron.entities.ChartDetails;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.entities.TradeDetails;
import com.magneto.pxocron.repositories.Generationcharts;
import com.magneto.pxocron.repositories.TradeDetailsRepo;
import com.magneto.pxocron.services.TradeDetailsBatchInsertion;
import com.magneto.pxocron.services.TradeReportBatchInsertion;

import org.mockito.AdditionalMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class ShortEntryCriteriaTest {

	@Mock
	ChartsAndTradesDao chartsAndTrades;

	@InjectMocks
	ShortEntryCriteria criteria;

	@Mock
	TradeDetailsBatchInsertion tradeBatch;

	@Mock
	TradeReportBatchInsertion reportBatch;

	@BeforeEach
	void init_mocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGeneratinLongEntry() {

		System.out.println("long entry->");

		List<TradeDetails> tradeList = Arrays.asList(new TradeDetails(345, "2021-09-29", "AAPL", 67.0, 56.0, "red", 0),

				new TradeDetails(678, "2021-09-29", "TSLA", 98.0, 44.0, "red", 0),
				new TradeDetails(678, "2021-09-29", "CLA", 98.0, 44.0, "red", 1),
				new TradeDetails(678, "2021-09-29", "PLA", 98.0, 44.0, "red", 0),
				new TradeDetails(678, "2021-09-29", "GLA", 98.0, 44.0, "red", 3),
				new TradeDetails(678, "2021-09-29", "LLA", 98.0, 44.0, "red", 0),
				new TradeDetails(678, "2021-09-29", "SLA", 98.0, 44.0, "red", 0),
				new TradeDetails(678, "2021-09-29", "MLA", 98.0, 44.0, "red", 2),
				new TradeDetails(678, "2021-09-29", "XLA", 98.0, 44.0, "red", 4),
				new TradeDetails(678, "2021-09-29", "MsLA", 98.0, 44.0, "red", 0),
				new TradeDetails(678, "2021-09-29", "QLA", 98.0, 44.0, "red", 5),
				new TradeDetails(678, "2021-09-29", "TPLA", 98.0, 44.0, "red", 0),
				new TradeDetails(678, "2021-09-29", "POLA", 98.0, 44.0, "red", 0),
				new TradeDetails(678, "2021-09-29", "OPLA", 98.0, 44.0, "red", 0),
				new TradeDetails(678, "2021-09-29", "KLA", 98.0, 44.0, "red", 0),
				new TradeDetails(678, "2021-09-29", "SkLA", 98.0, 44.0, "red", 0),
				new TradeDetails(678, "2021-09-29", "ANGLA", 98.0, 44.0, "red", 0)

		);
		List<ChartDetails> chartList = Arrays.asList(
				new ChartDetails(122342, "AAPL", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "AAPL", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "TSLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "TSLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "CLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "CLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "KLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "KLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "ANGLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "ANGLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "POLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "POLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "SkLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "SkLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "MLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "red", 2, 2, 2,
						new SymbolData(1234, "MLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "GLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "GLA", 100.5, 100.5, 100.5, 100.5, "2021-12-12")),
				new ChartDetails(122342, "MsLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "MsLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "PLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "PLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "OPLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "OPLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "LLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "LLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")),
				new ChartDetails(122342, "XLA", "2021-12-12", 89.0, 45.0, 54.0, 34.0, "green", 2, 2, 2,
						new SymbolData(1234, "XLA", 35.5, 35.5, 35.5, 35.5, "2021-12-12")));

		when(chartsAndTrades.tradeData(anyString())).thenReturn(tradeList);
		when(chartsAndTrades.chartData(anyString())).thenReturn(chartList);
		// when(chartsAndTrades.chartData(anyString())).thenReturn(chartList);
		// when(chartsAndTrades.chartData(anyString())).thenReturn(chartList);

		// tradeDetailsRepo.findAllByDate("currentDate")

	//	criteria.generatingShortEntry();

	}

}

package com.magneto.pxocron.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.dto.StockDetails;
import com.magneto.pxocron.entities.RelativeStrength;
import com.magneto.pxocron.entities.Symbol;
import com.magneto.pxocron.repositories.RelativeStrengthOutput;
import com.magneto.pxocron.repositories.StockDetailsRepo;
import com.magneto.pxocron.repositories.SymbolRepo;


@Service
public class StockExchangeDao {
	
	@Autowired
	StockDetailsRepo stockRepo;
	
	@Autowired
	RelativeStrengthOutput relativeStrengthOutput;
	
	
	@Autowired
	SymbolRepo symbolRepo;
	
	
	public List<StockDetails> StockRepoInvoker()
	{
		
		return stockRepo.findAll();
	}
	
	public List<String> symbolList()
	{
		
		return symbolRepo.symbolListFromExchange();
	}
	
	
	public List<RelativeStrength> relativeStrengthData(String date)
	{
		
		return relativeStrengthOutput.findAllByEodDate(date);
		
		
	}
	
	public List<RelativeStrength> relativeStrengthbetweenData(String start,String end)
	{
		
		return relativeStrengthOutput.getAllBetweenEodDates(start,end);
		
		
	}
	
	
	
	

}

package com.magneto.pxocron.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.entities.ChartDetails;
import com.magneto.pxocron.entities.TradeDetails;
import com.magneto.pxocron.repositories.Generationcharts;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.repositories.TradeDetailsRepo;


@Service
public class ChartsAndTradesDao {
	
	
	@Autowired
	Generationcharts chartData;
	 
	@Autowired
	TradeDetailsRepo traderepo;
	

	@Autowired
	SymbolDataRepo ohlcRepo;
	
	

	
	public TradeDetailsRepo getTraderepo() {
		return traderepo;
	}


	public void setTraderepo(TradeDetailsRepo traderepo) {
		this.traderepo = traderepo;
	}


	public List<ChartDetails> chartData(String eodDate) {
		List<ChartDetails> chartDetails = chartData.findAllByEodDate(eodDate);

		return chartDetails;

	}
	
	
	public List<TradeDetails> tradeData(String date)
	{
		
		List<TradeDetails> tradeList = traderepo.findAllByDate(date);
		
		return tradeList;
		
	}
	
	public List<TradeDetails> tradeDatawithoutParam()
	{
		
		List<TradeDetails> tradeList = traderepo.findAll();
		
		return tradeList;
		
	}
	
	
	

}

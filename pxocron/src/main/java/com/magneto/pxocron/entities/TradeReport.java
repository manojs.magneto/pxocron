package com.magneto.pxocron.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class TradeReport {
	@Id
	private long id = System.nanoTime();
	private String symbol;
	private String entry_date;
	private String exit_date;
	private double entry_price;
	private double target_profit;
	private double stop_loss;
	private double exit_price;
	private String date;
	private String tradeType;
	
	@OneToOne(fetch = FetchType.LAZY, optional=true)
    @JoinColumn(name = "chartId",unique=false)
	private ChartDetails chartDetails;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getEntry_date() {
		return entry_date;
	}

	public void setEntry_date(String entry_date) {
		this.entry_date = entry_date;
	}

	public String getExit_date() {
		return exit_date;
	}

	public void setExit_date(String exit_date) {
		this.exit_date = exit_date;
	}

	public double getEntry_price() {
		return entry_price;
	}

	public void setEntry_price(double entry_price) {
		this.entry_price = entry_price;
	}

	public double getTarget_profit() {
		return target_profit;
	}

	public void setTarget_profit(double target_profit) {
		this.target_profit = target_profit;
	}

	public double getStop_loss() {
		return stop_loss;
	}

	public void setStop_loss(double stop_loss) {
		this.stop_loss = stop_loss;
	}

	public double getExit_price() {
		return exit_price;
	}

	public void setExit_price(double exit_price) {
		this.exit_price = exit_price;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public ChartDetails getChartDetails() {
		return chartDetails;
	}

	public void setChartDetails(ChartDetails chartDetails) {
		this.chartDetails = chartDetails;
	}

	@Override
	public String toString() {
		return "TradeReport [id=" + id + ", symbol=" + symbol + ", entry_date=" + entry_date + ", exit_date="
				+ exit_date + ", entry_price=" + entry_price + ", target_profit=" + target_profit + ", stop_loss="
				+ stop_loss + ", exit_price=" + exit_price + ", date=" + date + ", tradeType=" + tradeType + "]";
	}

	public TradeReport(long id, String symbol, String entry_date, String exit_date, double entry_price,
			double target_profit, double stop_loss, double exit_price, String date, String tradeType) {
		super();
		this.id = id;
		this.symbol = symbol;
		this.entry_date = entry_date;
		this.exit_date = exit_date;
		this.entry_price = entry_price;
		this.target_profit = target_profit;
		this.stop_loss = stop_loss;
		this.exit_price = exit_price;
		this.date = date;
		this.tradeType = tradeType;
	}

	public TradeReport() {
		// TODO Auto-generated constructor stub
	}

	

}

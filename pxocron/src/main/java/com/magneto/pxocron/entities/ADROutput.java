package com.magneto.pxocron.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Version;

import org.hibernate.annotations.SQLInsert;


@Entity
@SQLInsert(sql = "insert ignore into ADROutput (Adr, date, symbol, symbol_id, version, id) values (?, ?, ?, ?, ?, ?)")
public class ADROutput {

	@Id
	private long id = System.nanoTime();

	private String date;
	private String symbol;
	private Double Adr;
	@OneToOne(fetch = FetchType.LAZY, optional=true)
	@JoinColumn(name = "symbol_id",unique=false)
	private SymbolData symbolData;
	@Version 
    private Short version;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Double getAdr() {
		return Adr;
	}

	public void setAdr(Double adr) {
		Adr = adr;
	}

	public SymbolData getSymbolData() {
		return symbolData;
	}

	public void setSymbolData(SymbolData symbolData) {
		this.symbolData = symbolData;
	}

	public Short getVersion() {
		return version;
	}

	public void setVersion(Short version) {
		this.version = version;
	}

}

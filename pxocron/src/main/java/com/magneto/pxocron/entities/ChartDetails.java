package com.magneto.pxocron.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLInsert;

@Entity
@Table(name = "chart_data_us")
@SQLInsert(sql = "insert ignore into chart_data_us (adr, cons_bullet_color, cons_bullet_type, cons_entry_exit, cons_gap_field, cons_position, created_on, date, eodDate, exit_1030_bullet_color, exit_1030_bullet_type, exit_1030_entry_exit, exit_1030_gap_field, exit_1030_position, exit_1040_bullet_color, exit_1040_bullet_type, exit_1040_entry_exit, exit_1040_gap_field, exit_1040_position, exit_1050_bullet_color, exit_1050_bullet_type, exit_1050_entry_exit, exit_1050_gap_field, exit_1050_position, exit_1530_bullet_color, exit_1530_bullet_type, exit_1530_entry_exit, exit_1530_gap_field, exit_1530_position, exit_1545_bullet_color, exit_1545_bullet_type, exit_1545_entry_exit, exit_1545_gap_field, exit_1545_position, exit_1560_bullet_color, exit_1560_bullet_type, exit_1560_entry_exit, exit_1560_gap_field, exit_1560_position, exit_2040_bullet_color, exit_2040_bullet_type, exit_2040_entry_exit, exit_2040_gap_field, exit_2040_position, exit_2060_bullet_color, exit_2060_bullet_type, exit_2060_entry_exit, exit_2060_gap_field, exit_2060_position, macd, macd_color, macd_height, opt_bullet_color, opt_bullet_type, opt_entry_exit, opt_gap_field, opt_position, rsi, rsi_color, rsi_height, stoc_color, stoc_height, stock_chart_color, stockastic, symbol, symbol_id, symbols_id, updated_on, version, id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
public class ChartDetails {

	private static final long serialVersionUID = 1L;

	@Id
	private long id = System.nanoTime();

	private int symbols_id;
	private String symbol;
	private long date;
	private double stockastic;
	private double rsi;
	private double macd;
	private double adr;
	private Integer stock_chart_color;
	private Integer stoc_color;
	private Integer rsi_color;
	private Integer macd_color;
	private int stoc_height;
	private int rsi_height;
	private int macd_height;
	private int cons_position;
	private float cons_entry_exit;
	private int cons_bullet_type;
	private int cons_bullet_color;
	private int cons_gap_field;
	private int opt_position;
	private float opt_entry_exit;
	private int opt_bullet_type;
	private int opt_bullet_color;
	private int opt_gap_field;
	private int exit_1030_position;
	private float exit_1030_entry_exit;
	private int exit_1030_bullet_type;
	private int exit_1030_bullet_color;
	private int exit_1030_gap_field;
	private int exit_1040_position;
	private float exit_1040_entry_exit;
	private int exit_1040_bullet_type;
	private int exit_1040_bullet_color;
	private int exit_1040_gap_field;
	private int exit_1050_position;
	private float exit_1050_entry_exit;
	private int exit_1050_bullet_type;
	private int exit_1050_bullet_color;
	private int exit_1050_gap_field;
	private int exit_1530_position;
	private float exit_1530_entry_exit;
	private int exit_1530_bullet_type;
	private int exit_1530_bullet_color;
	private int exit_1530_gap_field;
	private int exit_1545_position;
	private float exit_1545_entry_exit;
	private int exit_1545_bullet_type;
	private int exit_1545_bullet_color;
	private int exit_1545_gap_field;
	private int exit_1560_position;
	private float exit_1560_entry_exit;
	private int exit_1560_bullet_type;
	private int exit_1560_bullet_color;
	private int exit_1560_gap_field;
	private int exit_2040_position;
	private float exit_2040_entry_exit;
	private int exit_2040_bullet_type;
	private int exit_2040_bullet_color;
	private int exit_2040_gap_field;
	private int exit_2060_position;
	private float exit_2060_entry_exit;
	private int exit_2060_bullet_type;
	private int exit_2060_bullet_color;
	private int exit_2060_gap_field;
	private String created_on;
	private String updated_on;
	private String eodDate;
	@OneToOne(fetch = FetchType.LAZY, optional=true)
	@JoinColumn(name = "symbol_id",unique=false)
	private SymbolData symbolData;
	
	
	@OneToOne(mappedBy = "chartDetails", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private TradeDetails tradeDetails;
	
	@OneToOne(mappedBy = "chartDetails", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private TradeReport tradeReports;
	@Version 
    private Short version;
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEodDate() {
		return eodDate;
	}

	public void setEodDate(String eodDate) {
		this.eodDate = eodDate;
	}

	public int getSymbols_id() {
		return symbols_id;
	}

	public void setSymbols_id(int symbols_id) {
		this.symbols_id = symbols_id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public double getAdr() {
		return adr;
	}

	public void setAdr(double adr) {
		this.adr = adr;
	}

	

	public Integer getStoc_color() {
		return stoc_color;
	}

	public void setStoc_color(Integer stoc_color) {
		this.stoc_color = stoc_color;
	}

	public Integer getRsi_color() {
		return rsi_color;
	}

	public void setRsi_color(Integer rsi_color) {
		this.rsi_color = rsi_color;
	}

	public Integer getMacd_color() {
		return macd_color;
	}

	public void setMacd_color(Integer macd_color) {
		this.macd_color = macd_color;
	}

	public int getStoc_height() {
		return stoc_height;
	}

	public void setStoc_height(int stoc_height) {
		this.stoc_height = stoc_height;
	}

	public int getRsi_height() {
		return rsi_height;
	}

	public void setRsi_height(int rsi_height) {
		this.rsi_height = rsi_height;
	}

	public int getMacd_height() {
		return macd_height;
	}

	public void setMacd_height(int macd_height) {
		this.macd_height = macd_height;
	}

	public int getCons_position() {
		return cons_position;
	}

	public void setCons_position(int cons_position) {
		this.cons_position = cons_position;
	}

	public float getCons_entry_exit() {
		return cons_entry_exit;
	}

	public void setCons_entry_exit(float cons_entry_exit) {
		this.cons_entry_exit = cons_entry_exit;
	}

	public int getCons_bullet_type() {
		return cons_bullet_type;
	}

	public void setCons_bullet_type(int cons_bullet_type) {
		this.cons_bullet_type = cons_bullet_type;
	}

	public int getCons_bullet_color() {
		return cons_bullet_color;
	}

	public void setCons_bullet_color(int cons_bullet_color) {
		this.cons_bullet_color = cons_bullet_color;
	}

	public int getCons_gap_field() {
		return cons_gap_field;
	}

	public void setCons_gap_field(int cons_gap_field) {
		this.cons_gap_field = cons_gap_field;
	}

	public int getOpt_position() {
		return opt_position;
	}

	public void setOpt_position(int opt_position) {
		this.opt_position = opt_position;
	}

	public float getOpt_entry_exit() {
		return opt_entry_exit;
	}

	public void setOpt_entry_exit(float opt_entry_exit) {
		this.opt_entry_exit = opt_entry_exit;
	}

	public int getOpt_bullet_type() {
		return opt_bullet_type;
	}

	public void setOpt_bullet_type(int opt_bullet_type) {
		this.opt_bullet_type = opt_bullet_type;
	}

	public int getOpt_bullet_color() {
		return opt_bullet_color;
	}

	public void setOpt_bullet_color(int opt_bullet_color) {
		this.opt_bullet_color = opt_bullet_color;
	}

	public int getOpt_gap_field() {
		return opt_gap_field;
	}

	public void setOpt_gap_field(int opt_gap_field) {
		this.opt_gap_field = opt_gap_field;
	}

	public int getExit_1030_position() {
		return exit_1030_position;
	}

	public void setExit_1030_position(int exit_1030_position) {
		this.exit_1030_position = exit_1030_position;
	}

	public float getExit_1030_entry_exit() {
		return exit_1030_entry_exit;
	}

	public void setExit_1030_entry_exit(float exit_1030_entry_exit) {
		this.exit_1030_entry_exit = exit_1030_entry_exit;
	}

	public int getExit_1030_bullet_type() {
		return exit_1030_bullet_type;
	}

	public void setExit_1030_bullet_type(int exit_1030_bullet_type) {
		this.exit_1030_bullet_type = exit_1030_bullet_type;
	}

	public int getExit_1030_bullet_color() {
		return exit_1030_bullet_color;
	}

	public void setExit_1030_bullet_color(int exit_1030_bullet_color) {
		this.exit_1030_bullet_color = exit_1030_bullet_color;
	}

	public int getExit_1030_gap_field() {
		return exit_1030_gap_field;
	}

	public void setExit_1030_gap_field(int exit_1030_gap_field) {
		this.exit_1030_gap_field = exit_1030_gap_field;
	}

	public int getExit_1040_position() {
		return exit_1040_position;
	}

	public void setExit_1040_position(int exit_1040_position) {
		this.exit_1040_position = exit_1040_position;
	}

	public float getExit_1040_entry_exit() {
		return exit_1040_entry_exit;
	}

	public void setExit_1040_entry_exit(float exit_1040_entry_exit) {
		this.exit_1040_entry_exit = exit_1040_entry_exit;
	}

	public int getExit_1040_bullet_type() {
		return exit_1040_bullet_type;
	}

	public void setExit_1040_bullet_type(int exit_1040_bullet_type) {
		this.exit_1040_bullet_type = exit_1040_bullet_type;
	}

	public int getExit_1040_bullet_color() {
		return exit_1040_bullet_color;
	}

	public void setExit_1040_bullet_color(int exit_1040_bullet_color) {
		this.exit_1040_bullet_color = exit_1040_bullet_color;
	}

	public int getExit_1040_gap_field() {
		return exit_1040_gap_field;
	}

	public void setExit_1040_gap_field(int exit_1040_gap_field) {
		this.exit_1040_gap_field = exit_1040_gap_field;
	}

	public int getExit_1050_position() {
		return exit_1050_position;
	}

	public void setExit_1050_position(int exit_1050_position) {
		this.exit_1050_position = exit_1050_position;
	}

	public float getExit_1050_entry_exit() {
		return exit_1050_entry_exit;
	}

	public void setExit_1050_entry_exit(float exit_1050_entry_exit) {
		this.exit_1050_entry_exit = exit_1050_entry_exit;
	}

	public int getExit_1050_bullet_type() {
		return exit_1050_bullet_type;
	}

	public void setExit_1050_bullet_type(int exit_1050_bullet_type) {
		this.exit_1050_bullet_type = exit_1050_bullet_type;
	}

	public int getExit_1050_bullet_color() {
		return exit_1050_bullet_color;
	}

	public void setExit_1050_bullet_color(int exit_1050_bullet_color) {
		this.exit_1050_bullet_color = exit_1050_bullet_color;
	}

	public int getExit_1050_gap_field() {
		return exit_1050_gap_field;
	}

	public void setExit_1050_gap_field(int exit_1050_gap_field) {
		this.exit_1050_gap_field = exit_1050_gap_field;
	}

	public int getExit_1530_position() {
		return exit_1530_position;
	}

	public void setExit_1530_position(int exit_1530_position) {
		this.exit_1530_position = exit_1530_position;
	}

	public float getExit_1530_entry_exit() {
		return exit_1530_entry_exit;
	}

	public void setExit_1530_entry_exit(float exit_1530_entry_exit) {
		this.exit_1530_entry_exit = exit_1530_entry_exit;
	}

	public int getExit_1530_bullet_type() {
		return exit_1530_bullet_type;
	}

	public void setExit_1530_bullet_type(int exit_1530_bullet_type) {
		this.exit_1530_bullet_type = exit_1530_bullet_type;
	}

	public int getExit_1530_bullet_color() {
		return exit_1530_bullet_color;
	}

	public void setExit_1530_bullet_color(int exit_1530_bullet_color) {
		this.exit_1530_bullet_color = exit_1530_bullet_color;
	}

	public int getExit_1530_gap_field() {
		return exit_1530_gap_field;
	}

	public void setExit_1530_gap_field(int exit_1530_gap_field) {
		this.exit_1530_gap_field = exit_1530_gap_field;
	}

	public int getExit_1545_position() {
		return exit_1545_position;
	}

	public void setExit_1545_position(int exit_1545_position) {
		this.exit_1545_position = exit_1545_position;
	}

	public float getExit_1545_entry_exit() {
		return exit_1545_entry_exit;
	}

	public void setExit_1545_entry_exit(float exit_1545_entry_exit) {
		this.exit_1545_entry_exit = exit_1545_entry_exit;
	}

	public int getExit_1545_bullet_type() {
		return exit_1545_bullet_type;
	}

	public void setExit_1545_bullet_type(int exit_1545_bullet_type) {
		this.exit_1545_bullet_type = exit_1545_bullet_type;
	}

	public int getExit_1545_bullet_color() {
		return exit_1545_bullet_color;
	}

	public void setExit_1545_bullet_color(int exit_1545_bullet_color) {
		this.exit_1545_bullet_color = exit_1545_bullet_color;
	}

	public int getExit_1545_gap_field() {
		return exit_1545_gap_field;
	}

	public void setExit_1545_gap_field(int exit_1545_gap_field) {
		this.exit_1545_gap_field = exit_1545_gap_field;
	}

	public int getExit_1560_position() {
		return exit_1560_position;
	}

	public void setExit_1560_position(int exit_1560_position) {
		this.exit_1560_position = exit_1560_position;
	}

	public float getExit_1560_entry_exit() {
		return exit_1560_entry_exit;
	}

	public void setExit_1560_entry_exit(float exit_1560_entry_exit) {
		this.exit_1560_entry_exit = exit_1560_entry_exit;
	}

	public int getExit_1560_bullet_type() {
		return exit_1560_bullet_type;
	}

	public void setExit_1560_bullet_type(int exit_1560_bullet_type) {
		this.exit_1560_bullet_type = exit_1560_bullet_type;
	}

	public int getExit_1560_bullet_color() {
		return exit_1560_bullet_color;
	}

	public void setExit_1560_bullet_color(int exit_1560_bullet_color) {
		this.exit_1560_bullet_color = exit_1560_bullet_color;
	}

	public int getExit_1560_gap_field() {
		return exit_1560_gap_field;
	}

	public void setExit_1560_gap_field(int exit_1560_gap_field) {
		this.exit_1560_gap_field = exit_1560_gap_field;
	}

	public int getExit_2040_position() {
		return exit_2040_position;
	}

	public void setExit_2040_position(int exit_2040_position) {
		this.exit_2040_position = exit_2040_position;
	}

	public float getExit_2040_entry_exit() {
		return exit_2040_entry_exit;
	}

	public void setExit_2040_entry_exit(float exit_2040_entry_exit) {
		this.exit_2040_entry_exit = exit_2040_entry_exit;
	}

	public int getExit_2040_bullet_type() {
		return exit_2040_bullet_type;
	}

	public void setExit_2040_bullet_type(int exit_2040_bullet_type) {
		this.exit_2040_bullet_type = exit_2040_bullet_type;
	}

	public int getExit_2040_bullet_color() {
		return exit_2040_bullet_color;
	}

	public void setExit_2040_bullet_color(int exit_2040_bullet_color) {
		this.exit_2040_bullet_color = exit_2040_bullet_color;
	}

	public int getExit_2040_gap_field() {
		return exit_2040_gap_field;
	}

	public void setExit_2040_gap_field(int exit_2040_gap_field) {
		this.exit_2040_gap_field = exit_2040_gap_field;
	}

	public int getExit_2060_position() {
		return exit_2060_position;
	}

	public void setExit_2060_position(int exit_2060_position) {
		this.exit_2060_position = exit_2060_position;
	}

	public float getExit_2060_entry_exit() {
		return exit_2060_entry_exit;
	}

	public void setExit_2060_entry_exit(float exit_2060_entry_exit) {
		this.exit_2060_entry_exit = exit_2060_entry_exit;
	}

	public int getExit_2060_bullet_type() {
		return exit_2060_bullet_type;
	}

	public void setExit_2060_bullet_type(int exit_2060_bullet_type) {
		this.exit_2060_bullet_type = exit_2060_bullet_type;
	}

	public int getExit_2060_bullet_color() {
		return exit_2060_bullet_color;
	}

	public void setExit_2060_bullet_color(int exit_2060_bullet_color) {
		this.exit_2060_bullet_color = exit_2060_bullet_color;
	}

	public int getExit_2060_gap_field() {
		return exit_2060_gap_field;
	}

	public void setExit_2060_gap_field(int exit_2060_gap_field) {
		this.exit_2060_gap_field = exit_2060_gap_field;
	}

	public String getCreated_on() {
		return created_on;
	}

	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}

	public String getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(String updated_on) {
		this.updated_on = updated_on;
	}

	public double getStockastic() {
		return stockastic;
	}

	public void setStockastic(double stockastic) {
		this.stockastic = stockastic;
	}

	public double getRsi() {
		return rsi;
	}

	public void setRsi(double rsi) {
		this.rsi = rsi;
	}

	public double getMacd() {
		return macd;
	}

	public void setMacd(double macd) {
		this.macd = macd;
	}

	public SymbolData getSymbolData() {
		return symbolData;
	}

	public void setSymbolData(SymbolData symbolData) {
		this.symbolData = symbolData;
	}
	
	

	public Short getVersion() {
		return version;
	}

	public void setVersion(Short version) {
		this.version = version;
	}
	

	public Integer getStock_chart_color() {
		return stock_chart_color;
	}

	public void setStock_chart_color(Integer stock_chart_color) {
		this.stock_chart_color = stock_chart_color;
	}

	public ChartDetails(long id, String symbol, String eodDate, double stockastic, double rsi, double macd,
			double adr, String stock_chart_color, Integer rsi_color, Integer macd_color,Integer stoc_color,SymbolData data) {
		super();
		this.id = id;
	
		this.symbol = symbol;
		this.eodDate = eodDate;
		this.stockastic = stockastic;
		this.rsi = rsi;
		this.macd = macd;
		this.adr = adr;
		this.rsi_color = rsi_color;
		this.macd_color = macd_color;
		this.stoc_color=stoc_color;
		this.symbolData=data;
		
	}
	
	

	public TradeReport getTradeReports() {
		return tradeReports;
	}

	public void setTradeReports(TradeReport tradeReports) {
		this.tradeReports = tradeReports;
	}

	public TradeDetails getTradeDetails() {
		return tradeDetails;
	}

	public void setTradeDetails(TradeDetails tradeDetails) {
		this.tradeDetails = tradeDetails;
	}

	public ChartDetails() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "ChartDetails [id=" + id +  ", symbol=" + symbol + ", date=" + date
				+ ", stockastic=" + stockastic + ", rsi=" + rsi + ", macd=" + macd + ", adr=" + adr + ", stoc_color="
				+ stoc_color + ", rsi_color=" + rsi_color + ", macd_color=" + macd_color + "]";
	}

	
}

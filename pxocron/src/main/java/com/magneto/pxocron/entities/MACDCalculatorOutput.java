package com.magneto.pxocron.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.SQLInsert;

@Entity

@Table(name = "macdcalculator_output")

@SQLInsert(sql = "insert ignore into macdcalculator_output (color, ema12, ema26, eodDate, macdHistogram, macdLine, signalLine, symbol, symbol_id, version, id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
public class MACDCalculatorOutput implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private long id = System.nanoTime();

	private Double ema12;
	private Double ema26;
	private Double macdLine;
	private Double signalLine;
	private Double macdHistogram;

	private String symbol;
	private String eodDate;
	private Integer color;
	@Version
	private Short version;
	@OneToOne(fetch = FetchType.LAZY, optional=true)
	@JoinColumn(name = "symbol_id",unique=false)
	private SymbolData symbolData;

	public Double getEma12() {
		return ema12;
	}

	public void setEma12(Double ema12) {
		this.ema12 = ema12;
	}

	public Double getEma26() {
		return ema26;
	}

	public void setEma26(Double ema26) {
		this.ema26 = ema26;
	}

	public Double getMacdLine() {
		return macdLine;
	}

	public void setMacdLine(Double macdLine) {
		this.macdLine = macdLine;
	}

	public Double getSignalLine() {
		return signalLine;
	}

	public void setSignalLine(Double signalLine) {
		this.signalLine = signalLine;
	}

	public Double getMacdHistogram() {
		return macdHistogram;
	}

	public void setMacdHistogram(Double macdHistogram) {
		this.macdHistogram = macdHistogram;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Short getVersion() {
		return version;
	}

	public void setVersion(Short version) {
		this.version = version;
	}

	public String getEodDate() {
		return eodDate;
	}

	public void setEodDate(String eodDate) {
		this.eodDate = eodDate;
	}

	public SymbolData getSymbolData() {
		return symbolData;
	}

	public void setSymbolData(SymbolData symbolData) {
		this.symbolData = symbolData;
	}

	public Integer getColor() {
		return color;
	}

	public void setColor(Integer color) {
		this.color = color;
	}

}

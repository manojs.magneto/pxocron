package com.magneto.pxocron.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "cron_details_US")

public class CronDetailsUs {

	@JsonIgnore
	@Id
	private long id = System.nanoTime();
	private long date;
	private long cron1_start;
	private long cron1_end;
	private int cron1_status;
	private long cron2_start;
	private long cron2_end;
	private int cron2_status;
	private long cron3_start;
	private long cron3_end;
	private int cron3_status;
	private long cron4_start;
	private long cron4_end;
	private int cron4_status;
	private long cron5_start;
	private long cron5_end;
	private int cron5_status;
	private long cron6_start;
	private long cron6_end;
	private int cron6_status;
	private long cron7_start;
	private long cron7_end;
	private int cron7_status;
	private long cron8_start;
	private long cron8_end;
	private int cron8_status;
	private long cron9_start;
	private long cron9_end;
	private int cron9_status;
	private long isSecondTime;

	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public long getCron1_start() {
		return cron1_start;
	}

	public void setCron1_start(long cron1_start) {
		this.cron1_start = cron1_start;
	}

	public long getCron1_end() {
		return cron1_end;
	}

	public void setCron1_end(long cron1_end) {
		this.cron1_end = cron1_end;
	}

	public int getCron1_status() {
		return cron1_status;
	}

	public void setCron1_status(int cron1_status) {
		this.cron1_status = cron1_status;
	}

	public long getCron2_start() {
		return cron2_start;
	}

	public void setCron2_start(long cron2_start) {
		this.cron2_start = cron2_start;
	}

	public long getCron2_end() {
		return cron2_end;
	}

	public void setCron2_end(long cron2_end) {
		this.cron2_end = cron2_end;
	}

	public int getCron2_status() {
		return cron2_status;
	}

	public void setCron2_status(int cron2_status) {
		this.cron2_status = cron2_status;
	}

	public long getCron3_start() {
		return cron3_start;
	}

	public void setCron3_start(long cron3_start) {
		this.cron3_start = cron3_start;
	}

	public long getCron3_end() {
		return cron3_end;
	}

	public void setCron3_end(long cron3_end) {
		this.cron3_end = cron3_end;
	}

	public int getCron3_status() {
		return cron3_status;
	}

	public void setCron3_status(int cron3_status) {
		this.cron3_status = cron3_status;
	}

	public long getCron4_start() {
		return cron4_start;
	}

	public void setCron4_start(long cron4_start) {
		this.cron4_start = cron4_start;
	}

	public long getCron4_end() {
		return cron4_end;
	}

	public void setCron4_end(long cron4_end) {
		this.cron4_end = cron4_end;
	}

	public int getCron4_status() {
		return cron4_status;
	}

	public void setCron4_status(int cron4_status) {
		this.cron4_status = cron4_status;
	}

	public long getCron5_start() {
		return cron5_start;
	}

	public void setCron5_start(long cron5_start) {
		this.cron5_start = cron5_start;
	}

	public long getCron5_end() {
		return cron5_end;
	}

	public void setCron5_end(long cron5_end) {
		this.cron5_end = cron5_end;
	}

	public int getCron5_status() {
		return cron5_status;
	}

	public void setCron5_status(int cron5_status) {
		this.cron5_status = cron5_status;
	}

	public long getCron6_start() {
		return cron6_start;
	}

	public void setCron6_start(long cron6_start) {
		this.cron6_start = cron6_start;
	}

	public long getCron6_end() {
		return cron6_end;
	}

	public void setCron6_end(long cron6_end) {
		this.cron6_end = cron6_end;
	}

	public int getCron6_status() {
		return cron6_status;
	}

	public void setCron6_status(int cron6_status) {
		this.cron6_status = cron6_status;
	}

	public long getCron7_start() {
		return cron7_start;
	}

	public void setCron7_start(long cron7_start) {
		this.cron7_start = cron7_start;
	}

	public long getCron7_end() {
		return cron7_end;
	}

	public void setCron7_end(long cron7_end) {
		this.cron7_end = cron7_end;
	}

	public int getCron7_status() {
		return cron7_status;
	}

	public void setCron7_status(int cron7_status) {
		this.cron7_status = cron7_status;
	}

	public long getCron8_start() {
		return cron8_start;
	}

	public void setCron8_start(long cron8_start) {
		this.cron8_start = cron8_start;
	}

	public long getCron8_end() {
		return cron8_end;
	}

	public void setCron8_end(long cron8_end) {
		this.cron8_end = cron8_end;
	}

	public int getCron8_status() {
		return cron8_status;
	}

	public void setCron8_status(int cron8_status) {
		this.cron8_status = cron8_status;
	}

	public long getCron9_start() {
		return cron9_start;
	}

	public void setCron9_start(long cron9_start) {
		this.cron9_start = cron9_start;
	}

	public long getCron9_end() {
		return cron9_end;
	}

	public void setCron9_end(long cron9_end) {
		this.cron9_end = cron9_end;
	}

	public int getCron9_status() {
		return cron9_status;
	}

	public void setCron9_status(int cron9_status) {
		this.cron9_status = cron9_status;
	}

	public long getIsSecondTime() {
		return isSecondTime;
	}

	public void setIsSecondTime(long isSecondTime) {
		this.isSecondTime = isSecondTime;
	}

}

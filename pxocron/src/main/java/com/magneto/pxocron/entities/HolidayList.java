package com.magneto.pxocron.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "holiday_list")

public class HolidayList {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int id;
	int year;
	String holiday_name;
	String observed_date;

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getHoliday_name() {
		return holiday_name;
	}

	public void setHoliday_name(String holiday_name) {
		this.holiday_name = holiday_name;
	}

	public String getObserved_date() {
		return observed_date;
	}

	public void setObserved_date(String observed_date) {
		this.observed_date = observed_date;
	}

}

package com.magneto.pxocron.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class TradeDetails {

	@Id
	private long id = System.nanoTime();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	private String date;
	private String symbol;
	private Double tempentryHighValue;
	private Double tempentryLowValue;
	private String chartColor;
	private Integer countdays;
	private String tradeType;
	@OneToOne(fetch = FetchType.LAZY, optional=true)
    @JoinColumn(name = "chartId",unique=false)
	private ChartDetails chartDetails;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getChartColor() {
		return chartColor;
	}

	public void setChartColor(String chartColor) {
		this.chartColor = chartColor;
	}

	public Integer getCountdays() {
		return countdays;
	}

	public void setCountdays(Integer countdays) {
		this.countdays = countdays;
	}

	public Double getTempentryHighValue() {
		return tempentryHighValue;
	}

	public void setTempentryHighValue(Double tempentryHighValue) {
		this.tempentryHighValue = tempentryHighValue;
	}

	public Double getTempentryLowValue() {
		return tempentryLowValue;
	}

	public void setTempentryLowValue(Double tempentryLowValue) {
		this.tempentryLowValue = tempentryLowValue;
	}
	
	

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	
	

	
	public ChartDetails getChartDetails() {
		return chartDetails;
	}

	public void setChartDetails(ChartDetails chartDetails) {
		this.chartDetails = chartDetails;
	}

	public TradeDetails(long id, String date, String symbol, Double tempentryHighValue, Double tempentryLowValue,
			String chartColor, Integer countdays) {
		super();
		this.id = id;
		this.date = date;
		this.symbol = symbol;
		this.tempentryHighValue = tempentryHighValue;
		this.tempentryLowValue = tempentryLowValue;
		this.chartColor = chartColor;
		this.countdays = countdays;
	}

	 
	public TradeDetails() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "TradeDetails [id=" + id + ", date=" + date + ", symbol=" + symbol + ", tempentryHighValue="
				+ tempentryHighValue + ", tempentryLowValue=" + tempentryLowValue + ", chartColor=" + chartColor
				+ ", countdays=" + countdays + ", tradeType=" + tradeType + "]";
	}
	
	

}

package com.magneto.pxocron.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "symbol")

public class Symbol {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id; 
	private Integer  exch_id;
	 private String symbol ;
	 private String name;
	  private String exchange;
	  private String status ;
	  private String statusOHLC;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusOHLC() {
		return statusOHLC;
	}
	public void setStatusOHLC(String statusOHLC) {
		this.statusOHLC = statusOHLC;
	}
	public Integer getExch_id() {
		return exch_id;
	}
	public void setExch_id(Integer exch_id) {
		this.exch_id = exch_id;
	}
	  
	  
	  
	  
	  
	  

}

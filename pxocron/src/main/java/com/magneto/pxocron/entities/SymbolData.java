package com.magneto.pxocron.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.SQLInsert;

@Entity
@Table(name = "symbol_US")
@SQLInsert(sql = " insert ignore into symbol_US (close, company, con_status, created_on, eod_date, eod_unix, exchange, high, low, open, opt_status, status, statusOHLC, symbol, updated_on, version, volume, id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
public class SymbolData implements Serializable {
	@Id
	private long id = System.nanoTime();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	private String symbol;
	private String company;
	private int exchange;
	private double open;
	private double high;
	private double low;
	private double close;
	private double volume;
	private int statusOHLC;
	private int status;
	private int con_status;
	private int opt_status;
	private long created_on;
	private long updated_on;
	@Column(name = "eod_date")
	private String eodDate;
	private long eod_unix;

	@OneToOne(mappedBy = "symbolData", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private StochasticOutput stochastic;

	@OneToOne(mappedBy = "symbolData", fetch = FetchType.LAZY, cascade = CascadeType.ALL)

	private RelativeStrength rsi;

	@OneToOne(mappedBy = "symbolData", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private MACDCalculatorOutput macd;

	@OneToOne(mappedBy = "symbolData", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private ChartDetails chartDetails;
	@OneToOne(mappedBy = "symbolData", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private ADROutput adrOutput;

	@Version
	private Long version;

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public double getHigh() {
		return high;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getStatusOHLC() {
		return statusOHLC;
	}

	public void setStatusOHLC(int statusOHLC) {
		this.statusOHLC = statusOHLC;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getCon_status() {
		return con_status;
	}

	public void setCon_status(int con_status) {
		this.con_status = con_status;
	}

	public int getOpt_status() {
		return opt_status;
	}

	public void setOpt_status(int opt_status) {
		this.opt_status = opt_status;
	}

	public long getCreated_on() {
		return created_on;
	}

	public void setCreated_on(long created_on) {
		this.created_on = created_on;
	}

	public long getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(long updated_on) {
		this.updated_on = updated_on;
	}

	public double getOpen() {
		return open;
	}

	public int getExchange() {
		return exchange;
	}

	public void setExchange(int exchange) {
		this.exchange = exchange;
	}

	public void setVolume(float volume) {
		this.volume = volume;
	}

	public void setOpen(float open) {
		this.open = open;
	}

	public void setHigh(float high) {
		this.high = high;
	}

	public void setEod_unix(long eod_unix) {
		this.eod_unix = eod_unix;
	}

	public String getCompany() {
		return company;
	}

	public String getEodDate() {
		return eodDate;
	}

	public void setEodDate(String eodDate) {
		this.eodDate = eodDate;
	}

	public long getEod_unix() {
		return eod_unix;
	}

	public StochasticOutput getStochastic() {
		return stochastic;
	}

	public void setStochastic(StochasticOutput stochastic) {
		this.stochastic = stochastic;
	}

	public RelativeStrength getRsi() {
		return rsi;
	}

	public void setRsi(RelativeStrength rsi) {
		this.rsi = rsi;
	}

	public MACDCalculatorOutput getMacd() {
		return macd;
	}

	public void setMacd(MACDCalculatorOutput macd) {
		this.macd = macd;
	}

	public ChartDetails getChartDetails() {
		return chartDetails;
	}

	public void setChartDetails(ChartDetails chartDetails) {
		this.chartDetails = chartDetails;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public ADROutput getAdrOutput() {
		return adrOutput;
	}

	public void setAdrOutput(ADROutput adrOutput) {
		this.adrOutput = adrOutput;
	}

	@Override
	public String toString() {
		return "SymbolData [id=" + id + ", symbol=" + symbol + ", company=" + company + ", exchange=" + exchange
				+ ", open=" + open + ", high=" + high + ", low=" + low + ", close=" + close + ", volume=" + volume
				+ ", statusOHLC=" + statusOHLC + ", status=" + status + ", con_status=" + con_status + ", opt_status="
				+ opt_status + ", created_on=" + created_on + ", updated_on=" + updated_on + ", eodDate=" + eodDate
				+ ", eod_unix=" + eod_unix + ", stochastic=" + stochastic + ", rsi=" + rsi + ", macd=" + macd
				+ ", chartDetails=" + chartDetails + ", version=" + version + "]";
	}

	public SymbolData(long id, String symbol, float open, float high, float low, float close, String eodDate) {
		super();
		this.id = id;
		this.symbol = symbol;
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
		this.eodDate = eodDate;

	}

	public SymbolData(long id2, String symbol2, double d, double e, double f, double g, String i) {

		this.id = id2;
		this.symbol = symbol2;
		this.open = (float) d;
		this.high = (float) e;
		this.low = (float) f;
		this.close = (float) g;
		this.eodDate = i;

		// TODO Auto-generated constructor stub
	}

	public SymbolData() {
		// TODO Auto-generated constructor stub
	}

}

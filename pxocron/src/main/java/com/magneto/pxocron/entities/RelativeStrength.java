package com.magneto.pxocron.entities;


import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.SQLInsert;

@Entity
@Table(name = "relative_strength_output")
@SQLInsert(sql = " insert ignore into relative_strength_output (color, eodDate, rs, rsi, strengthAvgLoss, strengthAvgProfit, strengthChange, strengthLoss, strengthProfit, symbol, symbol_id, version, id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

public class RelativeStrength implements Serializable{
	 private static final long serialVersionUID = 1L;
	
	 @Id
	 private long id = System.nanoTime();
	
	private Double   strengthChange;
	private Double strengthProfit;
	private Double   strengthLoss;
	private Double strengthAvgProfit;
	private Double strengthAvgLoss;
	private Double rs;
	private Double rsi;
	private String eodDate;
	private String symbol;
	private Integer color;
	
	@Version 
    private Short version;

	@OneToOne(fetch = FetchType.LAZY, optional=true)
    @JoinColumn(name = "symbol_id",unique=false)
	private SymbolData symbolData;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public Double getStrengthChange() {
		return strengthChange;
	}
	public void setStrengthChange(Double strengthChange) {
		this.strengthChange = strengthChange;
	}
	public Double getStrengthProfit() {
		return strengthProfit;
	}
	public void setStrengthProfit(Double strengthProfit) {
		this.strengthProfit = strengthProfit;
	}
	public Double getStrengthLoss() {
		return strengthLoss;
	}
	public void setStrengthLoss(Double strengthLoss) {
		this.strengthLoss = strengthLoss;
	}
	public Double getStrengthAvgProfit() {
		return strengthAvgProfit;
	}
	public void setStrengthAvgProfit(Double strengthAvgProfit) {
		this.strengthAvgProfit = strengthAvgProfit;
	}
	public Double getStrengthAvgLoss() {
		return strengthAvgLoss;
	}
	public void setStrengthAvgLoss(Double strengthAvgLoss) {
		this.strengthAvgLoss = strengthAvgLoss;
	}
	public Double getRs() {
		return rs;
	}
	public void setRs(Double rs) {
		this.rs = rs;
	}
	
	public Short getVersion() {
		return version;
	}
	public void setVersion(Short version) {
		this.version = version;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public Double getRsi() {
		return rsi;
	}
	public void setRsi(Double rsi) {
		this.rsi = rsi;
	}
	public String getEodDate() {
		return eodDate;
	}
	public void setEodDate(String eodDate) {
		this.eodDate = eodDate;
	}
	
	public SymbolData getSymbolData() {
		return symbolData;
	}
	public void setSymbolData(SymbolData symbolData) {
		this.symbolData = symbolData;
	}
	public Integer getColor() {
		return color;
	}
	public void setColor(Integer color) {
		this.color = color;
	}
	
	
	
	

}

package com.magneto.pxocron.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.SQLInsert;


@Entity
@Table(name = "stochastic_output")
@SQLInsert(sql = " insert ignore into stochastic_output (color, date, highestValues, lowestValues, percentageD, percentageDWihtoutSMA, percentageK, symbol, symbol_id, version, id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

public class StochasticOutput implements Serializable {
	@Id
	 private long id = System.nanoTime();
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	private String date;
	private String symbol;
	
	
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) { 
		this.symbol = symbol;
	}
	private Double highestValues;
	private Double lowestValues;

    private Double percentageK;
    private Double percentageDWihtoutSMA;
	private Double percentageD;
	private Integer color;
	@OneToOne(fetch = FetchType.LAZY, optional=true)
    @JoinColumn(name = "symbol_id",unique=false)
	private SymbolData symbolData;
	
	@Version
    private Short version;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getHighestValues() {
		return highestValues;
	}
	public void setHighestValues(Double highestValues) {
		this.highestValues = highestValues;
	}
	public Double getLowestValues() {
		return lowestValues;
	}
	public void setLowestValues(Double lowestValues) {
		this.lowestValues = lowestValues;
	}
	public Double getPercentageK() {
		return percentageK;
	}
	public void setPercentageK(Double percentageK) {
		this.percentageK = percentageK;
	}
	public Double getPercentageD() {
		return percentageD;
	}
	public void setPercentageD(Double percentageD) {
		this.percentageD = percentageD;
	}
	public Double getPercentageDWihtoutSMA() {
		return percentageDWihtoutSMA;
	}
	public void setPercentageDWihtoutSMA(Double percentageDWihtoutSMA) {
		this.percentageDWihtoutSMA = percentageDWihtoutSMA;
	}
	
	public SymbolData getSymbolData() {
		return symbolData;
	}
	public void setSymbolData(SymbolData symbolData) {
		this.symbolData = symbolData;
	}
	public Integer getColor() {
		return color;
	}
	public void setColor(Integer color) {
		this.color = color;
	}

	
	
	
	
	

}

package com.magneto.pxocron.exceptions;

public class CronException extends Exception {
	
	private static final long serialVersionUID = 7718828512143293558L;

	public CronException() {
		super();
	}

	public CronException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CronException(String message, Throwable cause) {
		super(message, cause);
	}

	public CronException(String message) {
		super(message);
	}

	public CronException(Throwable cause) {
		super(cause);
	}
	

}

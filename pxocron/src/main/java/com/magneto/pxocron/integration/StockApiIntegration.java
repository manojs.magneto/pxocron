package com.magneto.pxocron.integration;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.magneto.pxocron.controllers.PxoController;
import com.magneto.pxocron.dto.EntrinoStock;
import com.magneto.pxocron.dto.StockDetails;
import com.magneto.pxocron.dto.entrinoapi_2.EntrinoStockVariation;
import com.magneto.pxocron.repositories.StockDetailsRepo;

@Service
public class StockApiIntegration {
	String line = "";
	String cvsSplitBy = ",";
	@Autowired
	StockDetailsRepo repo;
	@Value("${stockExchangeUrl}")
	String stockExchangeUrl;

	@Value("${intrinoUrl}")
	String intrinoUrl;

	private static final Logger LOGGER = LoggerFactory.getLogger(StockApiIntegration.class);

	RestTemplate restTemplate = new RestTemplate();

	public ResponseEntity<byte[]> stockApiCall() throws IOException {
		restTemplate.getForEntity(stockExchangeUrl, byte[].class);

		ResponseEntity<byte[]> result = restTemplate.getForEntity(stockExchangeUrl, byte[].class);
		System.out.println("THE STOCK API CALL IS CLOSED" + result.getBody());

		return result;

	}

	public ResponseEntity<EntrinoStock> intrinoApiCall(String stock) throws IOException {

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("page_size", "10000000");
		map.add("api_key", "OjkzMzhlNWZmZGFmYzRhYjQyYTUxODNkNDlkMjc1YWUy");
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		final HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map,
				headers);

		String url = "https://api-v2.intrinio.com/securities/" + stock
				+ "/prices?start_date=2019-01-01&end_date=2021-12-28&page_size=1000000&api_key=OjkzMzhlNWZmZGFmYzRhYjQyYTUxODNkNDlkMjc1YWUy";

		ResponseEntity<EntrinoStock> result = restTemplate.exchange(url, HttpMethod.GET, entity, EntrinoStock.class);
		// (intrinoUrl, EntrinoStock.class,params);
		System.out.println("THE STOCK API CALL IS CLOSED" + result);
		LOGGER.info("the intrino api result is {}", result.getBody());

		//storinginFile(result.getBody());

		return result;

		// LOGGER.info("the intrino api url {}",intrinoUrl);

		// String result =
		// restTemplate.getForObject("https://api-v2.intrinio.com/securities/AAPL/prices?start_date=%272019-01-01%27&end_date=%272020-12-30%27&page_size=1000000&api_key=OjkzMzhlNWZmZGFmYzRhYjQyYTUxODNkNDlkMjc1YWUy",
		// String.class);

	}
	
	
	public ResponseEntity<EntrinoStock> intrinoRestApiCall(String stockUrl) throws IOException {

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("page_size", "10000000");
		map.add("api_key", "OjkzMzhlNWZmZGFmYzRhYjQyYTUxODNkNDlkMjc1YWUy");
		

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		final HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map,
				headers);

		String url = stockUrl;
		ResponseEntity<EntrinoStock> result = restTemplate.exchange(url, HttpMethod.GET, entity, EntrinoStock.class);
		// (intrinoUrl, EntrinoStock.class,params);
		System.out.println("THE STOCK API CALL IS CLOSED" + result);
		LOGGER.info("the intrino api result is {}", result.getBody());

		storinginFile(result.getBody(),"specificsymbols-");

		return result;

		// LOGGER.info("the intrino api url {}",intrinoUrl);

		// String result =
		// restTemplate.getForObject("https://api-v2.intrinio.com/securities/AAPL/prices?start_date=%272019-01-01%27&end_date=%272020-12-30%27&page_size=1000000&api_key=OjkzMzhlNWZmZGFmYzRhYjQyYTUxODNkNDlkMjc1YWUy",
		// String.class);

	}
	
	public ResponseEntity<EntrinoStockVariation> intrinoRestApiVersionCall(String stockUrl) throws IOException {

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("page_size", "10000000");
		map.add("api_key", "OjkzMzhlNWZmZGFmYzRhYjQyYTUxODNkNDlkMjc1YWUy");
		

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		final HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map,
				headers);

		String url = stockUrl;
		ResponseEntity<EntrinoStockVariation> result = restTemplate.exchange(url, HttpMethod.GET, entity, EntrinoStockVariation.class);
		// (intrinoUrl, EntrinoStock.class,params);
		System.out.println("THE STOCK API CALL IS CLOSED" + result);
		LOGGER.info("the intrino api result is {}", result.getBody());

		storinginFile(result.getBody(),"allsymbols-");

		return result;

		// LOGGER.info("the intrino api url {}",intrinoUrl);

		// String result =
		// restTemplate.getForObject("https://api-v2.intrinio.com/securities/AAPL/prices?start_date=%272019-01-01%27&end_date=%272020-12-30%27&page_size=1000000&api_key=OjkzMzhlNWZmZGFmYzRhYjQyYTUxODNkNDlkMjc1YWUy",
		// String.class);

	}

 
	
	private void storinginFile(Object body,String prefix) {

		ObjectMapper mapper = new ObjectMapper();

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		File file = new File("src/main/resources/stockdata-"+prefix+ dateFormat.format(date) + ".json");

		System.out.println(file.exists() + " ++++");
		try {
			mapper.writeValue(file, body);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			LOGGER.error("exception occured while storing the cron1 file into the database is {}", e);
		}

	}

}

package com.magneto.pxocron.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.assertj.core.api.Assertions;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.controllers.PxoController;
import com.magneto.pxocron.dao.StockExchangeDao;
import com.magneto.pxocron.dto.IndicatorIntermediate;
import com.magneto.pxocron.dto.StockDetails;
import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.repositories.StochasticCalculatorOutputRepo;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.utils.CurrentDateWeekendChecker;
import com.magneto.pxocron.utils.HolidaysChecker;
import com.magneto.pxocron.utils.SingleDateChecker;
import com.magneto.pxocron.utils.WeekendChecker;

@Service
public class StochasticEntryPoint {

	int i ;

	private static final Logger LOGGER = LoggerFactory.getLogger(StochasticEntryPoint.class);

	@Autowired
	StockExchangeDao stockExchangeDao;

	@Autowired
	StochasticCalculatorOutputRepo stochasticOutputRepo;

	@Autowired
	SymbolDataRepo symbolDataRepo;

	@Autowired

	StochasticBatchInsertion batchInsertion;

	@Autowired
	StochasticCalculatorOutputRepo stochasticRepo;

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Calendar start = Calendar.getInstance();
	Calendar end = Calendar.getInstance();
	Calendar compare = Calendar.getInstance();
	Calendar mapCompare = Calendar.getInstance();
	Calendar CurrentDate = Calendar.getInstance();
	Calendar startCalender = Calendar.getInstance();
	Calendar endCalender = Calendar.getInstance();
	Calendar currentCalender = Calendar.getInstance();
	Calendar tempCalender = Calendar.getInstance();

	Map<String, Double> lastAverage;

	Map<String, Optional<SymbolData>> symbolDataMapMaxValues;

	Map<String, Double> symbolDataMapMaxValuesMapped;

	Map<String, Double> symbolDataMapMinValuesMapped;

	Map<String, Optional<SymbolData>> symbolDataMapMinValues;

	Map<String, Double> temporaryMap;
	Map<String, Double> percentageDwithoutSMA;
	Map<String, Double> percentageDAverage;

	List<Double> lowValues;
	List<Double> highValues;
	List<Double> numerator;
	List<Double> denominator;
	List<Double> percentageK;
	List<Double> percentageDWihtoutSMA;

	Map<String, Double> bulkpercerntageK = null;
	Map<String, Double> bulkwithoutSma = null;
	Map<String, Double> bulkpercentageD = null;
	Map<String, Double> bulkhighValues = null;
	Map<String, Double> bulklowValues = null;

	Map<String, Integer> colorMap = new HashMap<>();

	List<Double> percentageD;
	double high;
	double low;

	Map<String, String> tempcolorHolder;
	Map<String, Object> tempdHolder;

	List<SymbolData> symbolDataList;

	List<SymbolData> filteredSymbolDataList;

	List<String> filteredList;

	List<SymbolData> symbolDataTodayList;

	List<StochasticOutput> stochasticList;

	Map<String, Double> merged;

	TreeMap<String, Map<String, Double>> bulkMerged;
	TreeMap<String, Map<String, Double>> bulkD;
	Map<String, Double> percentageKMap;
	TreeMap<String, Map<String, Double>> bulkPercentageK;
	Map<Integer, Map<String, Double>> percentageKDates;
	List<Map<String, Double>> percentageK2Dates;

	TreeMap<String, Map<String, Double>> bulkHighValues;
	TreeMap<String, Map<String, Double>> bulkLowValues;

	List<Map<String, Double>> averagePercentages=new ArrayList<>();


	WeekendChecker weekendChecker = new WeekendChecker();

	CurrentDateWeekendChecker weekendCheckeDater = new CurrentDateWeekendChecker();

	Map<String, Double> lastPercentageK;

	public IndicatorIntermediate stochasticHighLowCalculation(HashMap<String, Object> paramaeters) throws Exception

	{

		dateFixer(paramaeters);

		bulkMerged = new TreeMap<>();
		bulkD = new TreeMap<>();
		percentageKMap = new HashMap<>();
		bulkPercentageK = new TreeMap<>();
		percentageKDates = new LinkedHashMap<>();
		percentageK2Dates = new ArrayList<>();
		bulkHighValues = new TreeMap<>();
		bulkLowValues = new TreeMap<>();
		averagePercentages = new ArrayList<>();
		i=1;

		int total = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(startCalender, endCalender);

		LOGGER.info("the  total {} value is", total);

		// endCalender.add(Calendar.DATE, -(total));

		LOGGER.info("the  START DATE {} value is", startCalender.getTime());
		LOGGER.info("the END DATE {} value is", endCalender.getTime());

		// weekendCheckeDater.weekendCheckerDate(endCalender);

		System.out.println("start date is===>" + startCalender.getTime());
		System.out.println("end date is===>" + endCalender.getTime());

		while (endCalender.after(startCalender)) {

			LOGGER.info(" inside of each for loop execution");

			Date tempDate = startCalender.getTime();

			Calendar tempCalendar = Calendar.getInstance();
			tempCalendar.setTime(tempDate);

			tempCalendar.add(Calendar.DATE, -13);

			int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(tempCalendar, startCalender);

			if (startCalender.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
					|| startCalender.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
					|| HolidaysChecker.holidaysList().contains(dateFormat.format(startCalender.getTime())))

			{

				startCalender.add(Calendar.DATE, 1);

				continue;
			}

			// LOGGER.info("the before holidays startcalendar {} value is",
			// startCalender.getTime());
			// LOGGER.info("the the before holidays endcalendar {} value is",
			// endCalender.getTime());

			LOGGER.info("the normal startcalendar {} value is", startCalender.getTime());
			LOGGER.info("the  temp endcalendar {} value is", tempCalendar.getTime());

			symbolDataList = symbolDataRepo.findAllByEodDateBetween(dateFormat.format(tempCalendar.getTime()),
					dateFormat.format(startCalender.getTime()));

			symbolDataTodayList = symbolDataRepo.findAllByEodDate(dateFormat.format(startCalender.getTime()));

			filteredList = symbolDataList.stream().map(stock -> stock.getSymbol()).collect(Collectors.toList());

			filteredSymbolDataList = symbolDataList.stream().filter(stock -> filteredList.contains(stock.getSymbol()))
					.collect(Collectors.toList());

			symbolDataMapMaxValuesMapped = new HashMap<String, Double>();
			symbolDataMapMinValuesMapped = new HashMap<String, Double>();
			percentageKMap = new HashMap<>();

			percentageK = new ArrayList<Double>();

			// fetching the maximum of last 14 days

			symbolDataMapMaxValues = filteredSymbolDataList.stream().collect(Collectors.groupingBy(
					SymbolData::getSymbol, TreeMap::new, Collectors.maxBy(Comparator.comparing(SymbolData::getHigh))));

			// fetching the minimum of last 14 days

			symbolDataMapMinValues = filteredSymbolDataList.stream().collect(Collectors.groupingBy(
					SymbolData::getSymbol, TreeMap::new, Collectors.minBy(Comparator.comparing(SymbolData::getLow))));

			// mapping the maximum values to key value pairs where key is symbol and value
			// maximum of high value for last 14 days
			symbolDataMapMaxValues.entrySet().stream().forEach(item -> {

				symbolDataMapMaxValuesMapped.put(item.getKey(), Double.valueOf(item.getValue().get().getHigh()));

			});

			LOGGER.info("the symbolDataMapMaxValuesMapped {} value is", symbolDataMapMaxValuesMapped);

			// mapping the minimum values to key value pairs where key is symbol and value
			// is minimum of low values for last 14 days

			symbolDataMapMinValues.entrySet().stream().forEach(item -> {

				symbolDataMapMinValuesMapped.put(item.getKey(), Double.valueOf(item.getValue().get().getLow()));
				// lowValues.add()

			});

			LOGGER.info("the symbolDataMapMinValuesMapped {} value is", symbolDataMapMinValuesMapped);

			// converting the previously calculated maps in to the lists of values
			// containing high,low values

			// calculating the percentage k value where numberator is todays close value
			// -lowest of low,denominator is highest of high - lowest of low

			for (SymbolData data : symbolDataTodayList) {

				double numerator = Double.valueOf(data.getClose())
						- Double.valueOf(symbolDataMapMinValuesMapped.get(data.getSymbol()));

				double denominator = Double.valueOf(symbolDataMapMaxValuesMapped.get(data.getSymbol()))
						- Double.valueOf(symbolDataMapMinValuesMapped.get(data.getSymbol()));

				double value = (numerator / denominator);

				if (Double.isNaN(value) || Double.POSITIVE_INFINITY == value || Double.NEGATIVE_INFINITY == value) {
					value = 0.0;

				}

				// percentageK.add(value*100);

				percentageKMap.put(data.getSymbol(), value * 100);

			}

			LOGGER.info("the percentageKMap {} value is", percentageKMap);

			percentageKDates.put(i, percentageKMap);

			bulkHighValues.put(dateFormat.format(startCalender.getTime()), symbolDataMapMaxValuesMapped);

			bulkLowValues.put(dateFormat.format(startCalender.getTime()), symbolDataMapMinValuesMapped);

			bulkPercentageK.put(dateFormat.format(startCalender.getTime()), percentageKMap);

			// percentageDCalculationWithoutSMA();

			// resetting the calender

			// startCalender.add(Calendar.DATE, +(14 + count));

			startCalender.add(Calendar.DATE, 1);
			i++;

		}
		endCalender = Calendar.getInstance();
		startCalender = Calendar.getInstance();
		LOGGER.info("the percentageKDates size {} value is", percentageKDates.size());

		LOGGER.info("the percentageKDates {} value is", percentageKDates);
		lastPercentageK = percentageKDates.get(percentageKDates.size());
		LOGGER.info("THE BULK PERCENTAGE K {} value is", bulkPercentageK);
		return percentageDCalculationWithoutSMA();
		// percentageDCalculationWithoutSMA();
	}

	// calculating the percentage d value is average of two value of %k

	public IndicatorIntermediate percentageDCalculationWithoutSMA() throws ParseException {

		ArrayList<String> dates = new ArrayList<String>(bulkPercentageK.keySet());

		for (int i = 1; i <= 3; i++) {

			int end = i + 2;

			for (Integer key : percentageKDates.keySet()) {
				if ((key >= i && key <= end))//
					averagePercentages.add(percentageKDates.get(key));

			}
			String date = dates.get(end - 1);
			LOGGER.info("THE averagePercentages {} value is", averagePercentages);

			Map<String, Double> merged = averagePercentages.stream().map(Map::entrySet).flatMap(Collection::stream)
					.collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.averagingDouble(Map.Entry::getValue)));
			LOGGER.info("THE AVERAGE MERGED VALUES {} value is", merged);

			bulkMerged.put(date, merged);

			LOGGER.info("THE BULK MERGED D {} value is", bulkMerged);

			percentageK2Dates.add(merged);
			averagePercentages = new ArrayList<>();
			LOGGER.info("the lastPercentageK {} value is", lastPercentageK);

			LOGGER.info("the merged {} value is", merged);

		}

		LOGGER.info("the percentageK2Dates {} value is", percentageK2Dates);

		return percentageDCalculation();

	}

	public IndicatorIntermediate percentageDCalculation() {

		LOGGER.info("the percentageK2Dates in d calculation  size {} value is", percentageK2Dates.size());

		lastAverage = IntStream.range(0, percentageK2Dates.size())
				.mapToObj(i -> percentageK2Dates.get(percentageK2Dates.size() - i - 1)).filter(i -> !i.isEmpty())
				.findFirst().get();

		// lastAverage = percentageK2Dates.get(percentageK2Dates.size() - 1);

		LOGGER.info("the lastAverageValue {} value is", lastAverage);

		LOGGER.info("the lastAverage {} value is", lastAverage);

		merged = percentageK2Dates.stream().map(Map::entrySet).flatMap(Collection::stream)
				.collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.averagingDouble(Map.Entry::getValue)));
		LOGGER.info("the  final value d % value   {} value is", merged);

		bulkD.put(bulkMerged.lastKey(), merged);

		LOGGER.info("THE FINAL BULK D   {} value is", bulkD);
		return colorCoding(merged);

		// return savingStochasticOutput();

	}

	public IndicatorIntermediate colorCoding(Map<String, Double> merged) {
		merged.forEach((k, v) -> {
			if (v > 50.0) {

				colorMap.put(k, 1);
			} else {
				colorMap.put(k, 2);

			}

		}

		);
		return savingStochasticOutput();
	}

	private IndicatorIntermediate savingStochasticOutput() {
		List<StochasticOutput> list = new ArrayList<StochasticOutput>();

		bulkLowValues.forEach((key, value) ->

		{

			bulkpercerntageK = null;
			bulkwithoutSma = null;
			bulkpercentageD = null;
			bulkhighValues = bulkHighValues.get(key);
			bulklowValues = bulkLowValues.get(key);
			if (bulkPercentageK.get(key) != null) {
				bulkpercerntageK = bulkPercentageK.get(key);
			}
			if (bulkMerged.get(key) != null) {
				bulkwithoutSma = bulkMerged.get(key);
			}
			if (bulkD.get(key) != null) {
				bulkpercentageD = bulkD.get(key);
			}

			bulklowValues.forEach((k, v) ->

			{

				StochasticOutput stochasticOutput = new StochasticOutput();

				stochasticOutput.setSymbol(k);
				// stochasticOutput.setDate(dateFormat.format(tempCalender.getTime()));

				stochasticOutput.setHighestValues(bulkhighValues.get(k));
				stochasticOutput.setLowestValues(bulklowValues.get(k));
				if (bulkPercentageK != null) {
					stochasticOutput.setPercentageK(bulkpercerntageK.get(k));

				}
				if (bulkwithoutSma != null) {
					stochasticOutput.setPercentageDWihtoutSMA(bulkwithoutSma.get(k));
				}
				// stochasticOutput.setColor(colorMap.get(k));

				if (bulkpercentageD != null) {
					stochasticOutput.setPercentageD(bulkpercentageD.get(k));
				}
				stochasticOutput.setDate(key);

				list.add(stochasticOutput);
				LOGGER.info("the  current endCalender  {} value is", dateFormat.format(endCalender.getTime()));

			}

			);

		}

		);
		batchInsertion.saveStochastic(list);

		IndicatorIntermediate intermediateexchanger = new IndicatorIntermediate();

		intermediateexchanger.setOriginalValues(merged);
		intermediateexchanger.setColors(colorMap);

		return intermediateexchanger;
	}

	public void dateFixer(HashMap<String, Object> paramaeters) throws ParseException {
		Date date;
		String fixedDate = (String) paramaeters.get("date");

		if (fixedDate == null) {
			date = Calendar.getInstance().getTime();
		} else {
			date = date = dateFormat.parse(fixedDate);
		}

		endCalender.setTime(date);
		startCalender.setTime(date);
		tempCalender.setTime(date);
		startCalender.add(Calendar.DATE, -5);
		tempCalender.add(Calendar.DATE, -1);
		HolidaysAnWeekendChecker.singleWeekendChecker(endCalender);
		HolidaysAnWeekendChecker.singleWeekendChecker(tempCalender);

	}

}

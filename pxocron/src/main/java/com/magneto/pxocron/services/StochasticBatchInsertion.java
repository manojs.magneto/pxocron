package com.magneto.pxocron.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magneto.pxocron.controllers.PxoController;
import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.repositories.StochasticCalculatorOutputRepo;

//stochastic batch insertion which increases the performance
@Service
public class StochasticBatchInsertion {

	@Autowired
	StochasticCalculatorOutputRepo stochasticRepo;

	private static final Logger LOGGER = LoggerFactory.getLogger(StochasticBatchInsertion.class);

	@Transactional
	public void saveStochastic(List<StochasticOutput> stochasticList) {
		int size = stochasticList.size();
		int counter = 0;

		List<StochasticOutput> temporaryList = new ArrayList<>();

		for (StochasticOutput output : stochasticList) { 
			temporaryList.add(output);

			if ((counter + 1) % 500 == 0 || (counter + 1) == size) {

				System.out.println("storing the data");
				LOGGER.info("storing the data into database");
				stochasticRepo.saveAll(temporaryList);

				LOGGER.info("inserted the data into database"); 

				temporaryList.clear();
			}

			counter++;
		}

	}

}
package com.magneto.pxocron.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.dto.IndicatorIntermediate;
import com.magneto.pxocron.entities.ChartDetails;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.repositories.Generationcharts;
import com.google.common.collect.Sets;

@Service

public class ChartDetailsGeneration {

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	StochasticEntryPoint stochasticEntryPoint;
	@Autowired

	RsiStockEntryPoint rsiStockEntrPoint;
	@Autowired
	MacdStockEntryPoint macdStockEntryPoint;
	@Autowired
	StochasticCalculator stoCal;

	@Autowired
	RelativeStrengthIndexCalculator relativeStrength;

	@Autowired
	MACDCalculator madeCalculator;
	@Autowired
	ChartDetailsBatchInsertion chartRepo;

	@Autowired
	ADRCalculation adrCalculation;

	CompletableFuture<Void> future1;
	CompletableFuture<Void> future2;
	CompletableFuture<Void> future3;
	IndicatorIntermediate stochasticResult;
	IndicatorIntermediate rsiResult;
	IndicatorIntermediate macdResult;
	IndicatorIntermediate stochasticEntryPointResult;
	IndicatorIntermediate rsiEntrPoint;
	IndicatorIntermediate adrEntryPoint;
	IndicatorIntermediate macdEntryPoint;
	Map<String, Double> stochasticValues;
	Map<String, Integer> stochasticColors;
	Map<String, Double> rsiValues;
	Map<String, Integer> rsiColors;
	Map<String, Double> macdValues;
	Map<String, Integer> macdColors;
	Map<String, SymbolData> stochasticIds;
	Map<String, SymbolData> rsiIds;
	Map<String, SymbolData> adrIds;
	Map<String, Integer> adrColors;

	Map<String, SymbolData> macdIds;
	Map<String, Double> adrMap;
	Set<String> stockSymbols;
	Set<String> rsiSymbols;
	Set<String> macdSymbols;
	List<String> stockList;

	List<String> rsiList;

	List<String> macdList;
	Set<String> adrSymbols;
	Map<String, Double> adrValues;
	List<String> adrList;
	Calendar calender = Calendar.getInstance();


	public void chartGenerationForIndicatorEntryPoints(HashMap<String, Object> paramaeters)
			throws Exception, ExecutionException {

		stochasticEntryPointResult = stochasticEntryPoint.stochasticHighLowCalculation(paramaeters);
		stochasticValues = stochasticEntryPointResult.getOriginalValues();
		stochasticColors = stochasticEntryPointResult.getColors();
		stochasticIds = stochasticEntryPointResult.getIds();

		rsiEntrPoint = rsiStockEntrPoint.rsiCalculation(paramaeters);
		rsiValues = rsiEntrPoint.getOriginalValues();
		rsiColors = rsiEntrPoint.getColors();
		rsiIds = rsiEntrPoint.getIds();

		macdEntryPoint = macdStockEntryPoint.emaCalculation(paramaeters);
		macdValues = macdEntryPoint.getOriginalValues();
		macdColors = macdEntryPoint.getColors();
		macdIds = macdEntryPoint.getIds();

	//	adrEntryPoint = adrCalculation.adrCalculation();
		//adrValues = adrEntryPoint.getOriginalValues();
		//adrIds = adrEntryPoint.getIds();
		
		stockSymbols = stochasticValues.keySet();
		rsiSymbols = rsiValues.keySet();
		macdSymbols = macdValues.keySet();
		//adrSymbols=adrValues.keySet();
		//List<ChartDetails> chartDetailsList = chartEntry(stockSymbols, rsiSymbols, macdSymbols, adrSymbols);
		//chartRepo.chartStorage(chartDetailsList);

	}

	public void chartGenerationForNormalIndicators(HashMap<String, Object> paramaeters)
			throws Exception, ExecutionException {
		
		Date date;
		String fixedDate = (String) paramaeters.get("date");

		if (fixedDate == null) {
			date = Calendar.getInstance().getTime();
		} else {
			date = date = dateFormat.parse(fixedDate);
		}

		calender.setTime(date);
		

		stochasticEntryPointResult = stoCal.stochasticHightLowCalculation(paramaeters);

		stochasticValues = stochasticEntryPointResult.getOriginalValues();
		stochasticColors = stochasticEntryPointResult.getColors();
		stochasticIds = stochasticEntryPointResult.getIds();

		rsiEntrPoint = relativeStrength.relativeStrengthIndexCalculation(paramaeters);

		rsiValues = rsiEntrPoint.getOriginalValues();
		rsiColors = rsiEntrPoint.getColors();
		rsiIds = rsiEntrPoint.getIds();

		macdEntryPoint = madeCalculator.MACDCalculation(paramaeters);

		macdValues = macdEntryPoint.getOriginalValues();
		macdColors = macdEntryPoint.getColors();
		macdIds = macdEntryPoint.getIds();

		stockSymbols = stochasticValues.keySet();
		rsiSymbols = rsiValues.keySet();
		macdSymbols = macdValues.keySet();

		adrEntryPoint = adrCalculation.adrCalculation(paramaeters);
		adrValues = adrEntryPoint.getOriginalValues();
		adrIds = adrEntryPoint.getIds();
		adrSymbols=adrValues.keySet();

		List<ChartDetails> chartDetailsList = chartEntry(stockSymbols, rsiSymbols, macdSymbols, adrSymbols);

		chartRepo.chartStorage(chartDetailsList);
	}

	/**
	 * @param stockSymbols
	 * @param rsiSymbols
	 * @param macdSymbols
	 * @param adrValues2
	 * @return
	 */
	private List<ChartDetails> chartEntry(Set<String> stockSymbols, Set<String> rsiSymbols, Set<String> macdSymbols,
			Set<String> adrSymbols) {
		stockList = new ArrayList<String>(stockSymbols);

		rsiList = new ArrayList<String>(rsiSymbols);

		macdList = new ArrayList<String>(macdSymbols);

		adrList = new ArrayList<String>(adrSymbols);

		System.out.println("stohastic symbols " + stockSymbols);
		System.out.println("rsi symbols " + rsiSymbols);
		System.out.println("macdSymbols symbols " + macdSymbols);

		List<ChartDetails> chartDetailsList = new ArrayList<>();

		stockSymbols.forEach(symbol -> {
			ChartDetails chartDetails = new ChartDetails();
			chartDetails.setStockastic(stochasticValues.get(symbol));
			chartDetails.setStoc_color(stochasticColors.get(symbol));

			chartDetails.setSymbol(String.valueOf(symbol));
			chartDetails.setDate(System.currentTimeMillis());
			chartDetails.setEodDate(dateFormat.format(calender.getTime()));
			if (stochasticIds != null) {
				chartDetails.setSymbolData(stochasticIds.get(symbol));
			}

			chartDetailsList.add(chartDetails);

			System.out.println("data finals chart details are inserted");

		});

		List<ChartDetails> rsimatchedList = chartDetailsList.stream()
				.filter(chart -> rsiList.contains(chart.getSymbol())).collect(Collectors.toList());

		List<ChartDetails> rsiunMatchedList = chartDetailsList.stream()
				.filter(chart -> !rsiList.contains(chart.getSymbol())).collect(Collectors.toList());

		rsimatchedList.forEach(chart ->

		{

			chart.setRsi(rsiValues.get(chart.getSymbol()));

			chart.setRsi_color(rsiColors.get(chart.getSymbol()));

		});

		rsiunMatchedList.forEach(unmatchedChart ->

		{
			ChartDetails chartDetails = new ChartDetails();

			chartDetails.setRsi(rsiValues.get(unmatchedChart.getSymbol()));

			chartDetails.setRsi_color(rsiColors.get(unmatchedChart.getSymbol()));

			chartDetails.setSymbol(String.valueOf(unmatchedChart.getSymbol()));
			chartDetails.setDate(System.currentTimeMillis());
			chartDetails.setDate(System.currentTimeMillis());
			chartDetails.setEodDate(dateFormat.format(calender.getTime()));
			if (rsiIds != null) {
				chartDetails.setSymbolData(rsiIds.get(unmatchedChart.getSymbol()));
			}

			chartDetailsList.add(chartDetails);

		}

		);

		List<ChartDetails> macdmatchedList = chartDetailsList.stream()
				.filter(chart -> macdList.contains(chart.getSymbol())).collect(Collectors.toList());

		List<ChartDetails> macdunMatchedList = chartDetailsList.stream()
				.filter(chart -> !macdList.contains(chart.getSymbol())).collect(Collectors.toList());

		macdmatchedList.forEach(chart ->

		{

			chart.setMacd(macdValues.get(chart.getSymbol()));

			chart.setMacd_color(macdColors.get(chart.getSymbol()));
		});

		macdunMatchedList.forEach(unmatchedChart ->

		{
			ChartDetails chartDetails = new ChartDetails();

			chartDetails.setMacd(macdValues.get(unmatchedChart.getSymbol()));

			chartDetails.setMacd_color(macdColors.get(unmatchedChart.getSymbol()));

			chartDetails.setSymbol(String.valueOf(unmatchedChart.getSymbol()));
			chartDetails.setDate(System.currentTimeMillis());
			chartDetails.setDate(System.currentTimeMillis());
			chartDetails.setEodDate(dateFormat.format(calender.getTime()));
			if (macdIds != null) {
				chartDetails.setSymbolData(macdIds.get(unmatchedChart.getSymbol()));
			}

			chartDetailsList.add(chartDetails);

		}

		);

		List<ChartDetails> adrmatchedList = chartDetailsList.stream()
				.filter(chart -> adrList.contains(chart.getSymbol())).collect(Collectors.toList());

		List<ChartDetails> adrunMatchedList = chartDetailsList.stream()
				.filter(chart -> !adrList.contains(chart.getSymbol())).collect(Collectors.toList());

		adrmatchedList.forEach(chart ->

		{

			chart.setAdr(adrValues.get(chart.getSymbol()));

		});

		adrunMatchedList.forEach(unmatchedChart ->

		{
			ChartDetails chartDetails = new ChartDetails();

			chartDetails.setAdr(adrValues.get(unmatchedChart.getSymbol()));

			chartDetails.setSymbol(String.valueOf(unmatchedChart.getSymbol()));
			chartDetails.setDate(System.currentTimeMillis());
			chartDetails.setDate(System.currentTimeMillis());
			chartDetails.setEodDate(dateFormat.format(calender.getTime()));
			if (adrIds != null) {
				chartDetails.setSymbolData(adrIds.get(unmatchedChart.getSymbol()));
			}

			chartDetailsList.add(chartDetails);
		}

		);

		System.out.println("data finals true chart details are inserted");

		System.out.println("final chart true size" + chartDetailsList.size());

		chartDetailsList.forEach(chartData -> {

			if ((chartData.getStoc_color() == 1) && (chartData.getRsi_color() == 1) && (chartData.getMacd_color() == 1))

			{

				chartData.setStock_chart_color(1);
			}

			if ((chartData.getStoc_color() == 2) && (chartData.getRsi_color() == 2) && (chartData.getMacd_color() == 2))

			{

				chartData.setStock_chart_color(2);
			}

			if ((chartData.getStoc_color() != chartData.getRsi_color())
					|| (chartData.getRsi_color() != chartData.getMacd_color())
					|| (chartData.getStoc_color() != chartData.getMacd_color()))

			{

				chartData.setStock_chart_color(0);
			}

		}

		);

		return chartDetailsList;
	}

}

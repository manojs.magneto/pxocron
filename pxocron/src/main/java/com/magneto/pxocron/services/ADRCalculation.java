package com.magneto.pxocron.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.dto.IndicatorIntermediate;
import com.magneto.pxocron.entities.ADROutput;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.repositories.ADRRepo;
import com.magneto.pxocron.repositories.SymbolDataRepo;


@Service
public class ADRCalculation {
	@Autowired
	SymbolDataRepo symbolDataRepo; 

	@Autowired
	ADRRepo adrRepo;

	@Autowired 

	ADRBatchInsertion adrBatchInsertion;
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Calendar start = Calendar.getInstance();
	Calendar end = Calendar.getInstance();
	List<SymbolData> symbolDataList;

	Map<String, Double> adrMap=new HashMap<>();
	Calendar startCalender = Calendar.getInstance();
	Calendar endCalender = Calendar.getInstance();
	Calendar tempCalender = Calendar.getInstance();


	public IndicatorIntermediate adrCalculation(HashMap<String, Object> paramaeters) {
		try {
			dateFixer(paramaeters);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(startCalender, endCalender);

		symbolDataList = symbolDataRepo.findAllByEodDateBetween(dateFormat.format(startCalender.getTime()),
				dateFormat.format(endCalender.getTime()));

		Map<String, Double> highData = symbolDataList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol))
				.entrySet().stream().collect(Collectors.toMap(map -> map.getKey(),
						map -> map.getValue().stream().findFirst().get().getHigh()));

		Map<String, Double> lowData = symbolDataList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol))
				.entrySet().stream().collect(Collectors.toMap(map -> map.getKey(),
						map -> map.getValue().stream().findFirst().get().getLow()));

		List<ADROutput> adrList = new ArrayList<>();

		highData.forEach((k, v) ->

		{

			Double high = v;

			Double low = Double.valueOf(lowData.get(k).toString());

			Double difference = high - low;

			ADROutput adrOutput = new ADROutput();
			adrOutput.setAdr(difference);
			adrOutput.setDate(dateFormat.format(tempCalender.getTime()));
			adrOutput.setSymbol(k);
			adrMap.put(k, difference);

			adrList.add(adrOutput);

		}

		);

		adrBatchInsertion.saveADR(adrList);

		IndicatorIntermediate intermediateValues = new IndicatorIntermediate();

		intermediateValues.setOriginalValues(adrMap);  
		start = Calendar.getInstance();
		end = Calendar.getInstance();
		// intermediateValues.setIds(todayIds);

		return intermediateValues;

		// return null;

	}
	
	public void dateFixer(HashMap<String, Object> paramaeters) throws ParseException {
		// TODO Auto-generated method stub

		Date date;
		String fixedDate = (String) paramaeters.get("date");

		if (fixedDate == null) {
			date = Calendar.getInstance().getTime();
		} else {
			date = date = dateFormat.parse(fixedDate);
		}

		endCalender.setTime(date);
		startCalender.setTime(date);
		tempCalender.setTime(date);
		// tempCalender.setTime(date);

		startCalender.add(Calendar.DATE, -7);

	}

}

package com.magneto.pxocron.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.repositories.SymbolDataRepo;

//stock data batch insertion which increases the performance 

@Service
public class StockBatchInsertion { 

	@Autowired
	SymbolDataRepo datarepo;

	private static final Logger LOGGER = LoggerFactory.getLogger(StockBatchInsertion.class);

	@Transactional
	public void saveStockBatch(List<SymbolData> stockList) {
		int size = stockList.size();
		int counter = 0;

		List<SymbolData> temporaryList = new ArrayList<>();

		for (SymbolData symbolData : stockList) {
			temporaryList.add(symbolData);

			if ((counter + 1) % 10000 == 0 || (counter + 1) == size) {

				System.out.println("storing the data");
				LOGGER.info("storing the data into database");
				datarepo.saveAll(temporaryList);

				LOGGER.info("inserted the data into database");

				temporaryList.clear();
			}

			counter++;
		}

	}

}

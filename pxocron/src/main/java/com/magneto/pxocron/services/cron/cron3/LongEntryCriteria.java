package com.magneto.pxocron.services.cron.cron3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.dao.ChartsAndTradesDao;
import com.magneto.pxocron.entities.ChartDetails;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.entities.TradeDetails;
import com.magneto.pxocron.entities.TradeReport;
import com.magneto.pxocron.repositories.Generationcharts;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.repositories.TradeDetailsRepo;
import com.magneto.pxocron.services.TradeDetailsBatchInsertion;
import com.magneto.pxocron.services.TradeReportBatchInsertion;

@Service
public class LongEntryCriteria {
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Autowired
	Generationcharts chartData;

	@Autowired
	SymbolDataRepo ohlcRepo;

	@Autowired
	TradeDetailsRepo traderepo;

	@Autowired
	ChartsAndTradesDao chartsandTradesDao;
	@Autowired
	TradeDetailsBatchInsertion tradeBatch;

	@Autowired
	TradeReportBatchInsertion reportBatch;

	List<TradeReport> finalReport;

	Calendar todayDate = Calendar.getInstance();
	Calendar yesterdayDate = Calendar.getInstance();
	List<TradeDetails> tradeList;
	List<TradeDetails> removalTradeList;
	
 

	public void generatinLongEntry() throws ParseException {
		// yesterdayDate.add(Calendar.DATE, -1);
		//dateFormat.parse("2021-01-26");
		todayDate.setTime(dateFormat.parse("2020-12-18"));
		//dateFormat.parse("2021-01-26");
		yesterdayDate.setTime(dateFormat.parse("2020-12-17"));

		tradeList = chartsandTradesDao.tradeData(dateFormat.format(yesterdayDate.getTime()));
		removalTradeList=new ArrayList<TradeDetails>();
		 finalReport = new ArrayList<TradeReport>();


		System.out.println("tradelist is------>" + tradeList);

		List<ChartDetails> chartDetails = chartsandTradesDao.chartData(dateFormat.format(todayDate.getTime()));
		System.out.println("chartDetails is------>" + chartDetails);
		System.out.println("chartDetails is------>" + chartDetails.size());

		List<ChartDetails> greenChartList = chartDetails.stream()
				.filter(stock -> stock.getStock_chart_color().equals(1)).collect(Collectors.toList());
		System.out.println("chartDetailsgreen is------>" + greenChartList);
		System.out.println("chartDetails size is------>" + greenChartList.size());

		List<TradeDetails> filteredTradeList = tradeList.stream().filter(trade -> trade.getChartColor().equals("green"))
				.collect(Collectors.toList());

		Map<String, Double> tempvalues = filteredTradeList.stream()
				.collect(Collectors.toMap(TradeDetails::getSymbol, TradeDetails::getTempentryHighValue));
		
		Map<String, List<TradeDetails>> tradeSymbol = filteredTradeList.stream().collect(Collectors.groupingBy(TradeDetails::getSymbol));

		Map<String, Integer> countValues = filteredTradeList.stream()
				.collect(Collectors.toMap(TradeDetails::getSymbol, TradeDetails::getCountdays));
		System.out.println("filteredTradeList temporaryhighvalues is------>" + tempvalues);

		System.out.println("filteredTradeList temporarycount is------>" + countValues);

		List<TradeDetails> tradeListBukcet = new ArrayList<>();

		greenChartList.forEach(eachChart ->

		{

			SymbolData symbolData = eachChart.getSymbolData();

			System.out.println("each symbol" + symbolData.getSymbol());

			int countdaysValue;

			if (countValues.get(symbolData.getSymbol()) == null) {
				countdaysValue = 0;

			} else {
				countdaysValue = countValues.get(symbolData.getSymbol()) + 1;

			}

			int finalValue = positiveCheck(countdaysValue);

			switch (finalValue) {
			// Case statements
			case 0:
				System.out.println("inside the first record");

				firstTradeRecord(symbolData.getSymbol(), symbolData.getHigh(), countdaysValue, tradeListBukcet,
						eachChart);

				break;
			case 1:
				System.out.println("inside the criteria part record");
				TradeDetails trade = tradeSymbol.get(symbolData.getSymbol()).get(0);

				tradeCheckForEntryCriteria(symbolData.getSymbol(), symbolData.getHigh(), countdaysValue,
						tempvalues.get(symbolData.getSymbol()), tradeListBukcet, trade,eachChart);

				break;

			}

		});

		tradeBatch.saveTradeDetailsBatch(tradeListBukcet);
		reportBatch.saveTradeReportsBatch(finalReport);
		//removalTradeList
		if(removalTradeList!=null)
		{
			
			System.out.println("TRADE REMOVAL PRINGINT=============>");
		//traderepo.deleteAll(removalTradeList);
		traderepo.deleteInBatch(removalTradeList);
		}
	
		
		System.out.println("the tradeListBucket--------->" + tradeListBukcet);
		System.out.println("the finaltradereport---------->" + finalReport);

		//yesterdayDate = Calendar.getInstance();

	}

	private int positiveCheck(int countdaysValue) {
		// TODO Auto-generated method stub

		if (countdaysValue == 0) {

			return 0;
		}

		else {

			return 1;
		}

	}

	private void tradeCheckForEntryCriteria(String symbol, double high, int countdaysValue, Double tempValue,
			List<TradeDetails> tradeListBukcet, TradeDetails trade, ChartDetails eachChart) {

		if (countdaysValue <= 5) {

			if (high > tempValue) {

				tempValue = tempValue + 0.01;

				TradeReport tradeReport = new TradeReport();

				tradeReport.setEntry_date(dateFormat.format(todayDate.getTime()));
				tradeReport.setEntry_price(tempValue);
				tradeReport.setSymbol(symbol);

				tradeReport.setTradeType("long");
				tradeReport.setDate(dateFormat.format(todayDate.getTime()));
				tradeReport.setChartDetails(eachChart);

				finalReport.add(tradeReport);
				removalTradeList.add(trade);

				// make here the trade entry in main trade table

			}

			else {

				trade.setCountdays(countdaysValue);
				trade.setTempentryHighValue(high);
				trade.setDate(dateFormat.format(todayDate.getTime()));
				// tradeList.get(trade);
				// TradeDetails tradeDetail = new TradeDetails();
				// tradeDetail.setCountdays(countdaysValue);
				// tradeDetail.setSymbol(symbol);
				// tradeDetail.setTempentryHighValue(high);
				// tradeDetail.setDate(dateFormat.format(todayDate.getTime()));
				// tradeDetail.setChartColor("green");
				// tradeDetail.setTradeType("long");
				tradeListBukcet.add(trade);

			}

		}

		else {

			// countdaysValue = 0;
//
			// TradeDetails tradeDetail = new TradeDetails();
			// tradeDetail.setCountdays(countdaysValue);
			// tradeDetail.setSymbol(symbol);
			// tradeDetail.setTempentryHighValue(high);
			// tradeDetail.setDate("todayDate");
			// tradeDetail.setTradeType("long");
			// tradeDetail.setChartColor("green");
			// tradeDetail.setChart(null);

			// tradeListBukcet.add(tradeDetail);

			removalTradeList.add(trade);
			System.out.println("TRADE LIST REMOVAL==========>" + removalTradeList);

		}
		// TODO Auto-generated method stub

	}

	private void firstTradeRecord(String symbol, Double high, int countdaysValue, List<TradeDetails> tradeListBukcet, ChartDetails eachChart) {

		System.out.println("the trade entry first record");

		TradeDetails tradeDetail = new TradeDetails();

		tradeDetail.setCountdays(countdaysValue);
		tradeDetail.setSymbol(symbol);
		tradeDetail.setTempentryHighValue(high); 
		tradeDetail.setDate(dateFormat.format(todayDate.getTime()));
		tradeDetail.setChartColor("green");
		tradeDetail.setChartDetails(eachChart);

		tradeDetail.setTradeType("long");

		System.out.println("the first trade record is =====>" + tradeDetail);
		tradeListBukcet.add(tradeDetail);

		// TODO Auto-generated method stub

	}

}

package com.magneto.pxocron.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magneto.pxocron.controllers.PxoController;
import com.magneto.pxocron.entities.ADROutput;
import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.repositories.ADRRepo;
import com.magneto.pxocron.repositories.StochasticCalculatorOutputRepo;

//stochastic batch insertion which increases the performance
@Service
public class ADRBatchInsertion {
	
	@Autowired
	ADRRepo adrRepo;



	private static final Logger LOGGER = LoggerFactory.getLogger(ADRBatchInsertion.class);

	@Transactional
	public void saveADR(List<ADROutput> adrList) {
		int size = adrList.size();
		int counter = 0;

		List<ADROutput> temporaryList = new ArrayList<>();

		for (ADROutput output : adrList) {
			temporaryList.add(output);

			if ((counter + 1) % 500 == 0 || (counter + 1) == size) {

				System.out.println("storing the data");
				LOGGER.info("storing the data into database");
				adrRepo.saveAll(temporaryList);

				LOGGER.info("inserted the data into database");

				temporaryList.clear();
			}

			counter++;
		}

	}

}
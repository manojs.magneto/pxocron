package com.magneto.pxocron.services.cron.cron3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.entities.ChartDetails;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.entities.TradeDetails;
import com.magneto.pxocron.entities.TradeReport;
import com.magneto.pxocron.repositories.Generationcharts;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.repositories.TradeReportRepo;
import com.magneto.pxocron.services.TradeReportBatchInsertion;


@Service
public class TradeExitProcess {
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


	@Autowired
	Generationcharts generationCharts;
	@Autowired
	TradeReportRepo tradeReportRepo;
	@Autowired
	SymbolDataRepo symbolDataRepo;
	List<ChartDetails> chartDetails;
	List<SymbolData> symbolList;
	List<TradeReport> tradeReportDetails;
	Map<String, Double> highValues;
	Map<String, Double> lowValues;
	Map<String, Double> openValues;
	Map<String, Double> targetProfitValues ;
	Map<String, Double> stopLossValues ;
	@Autowired
	TradeReportBatchInsertion reportBatch;
//	List<TradeReport> reportList = new ArrayList<TradeReport>(); 
	
	Calendar todayDate = Calendar.getInstance();

	// pull the final chart details and operate on the entry points with also adr
	// which was saved previously

	// calculate the profit loss and stop loss here

	// updating the table for these profittrget and stoploss values

	// iterate the final trade report and find all the adrs respective to that in
	// chart table and calculate the profit target
	// and stop loss for every entry and update the table for those entries
	//

	// compare the high and low for shorttrade and high trade perspective to the
	// things and calculate accordingly for final result

	// store the final exit price in the trade report table

	// compare trade report profit target and stop loss with the high and low values
	// of the ohlc data

	// making an relationship between trade report,chart data,ohlc data

	// get the

	// calculation for targetProfit and stopLoss
	public void targetProfitAndstopLossCalculation() {

		//chartDetails = generationCharts.findAllByDate("currentDate");

		//symbolList = symbolDataRepo.findAllByEodDate("currentDate");
		//dateFormat.parse("2021-01-26");
		
	 targetProfitValues = new HashMap<>();
	 stopLossValues = new HashMap<>();
		try {
			todayDate.setTime(dateFormat.parse("2021-01-29"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tradeReportDetails = tradeReportRepo.findAllByDate(dateFormat.format(todayDate.getTime()));

		//Map<String, Double> adrValues = chartDetails.stream()
			//	.collect(Collectors.toMap(ChartDetails::getSymbol, ChartDetails::getAdr));
		//highValues = symbolList.stream().collect(Collectors.toMap(SymbolData::getSymbol, SymbolData::getHigh));
		//lowValues = symbolList.stream().collect(Collectors.toMap(SymbolData::getSymbol, SymbolData::getLow));
		//openValues = symbolList.stream().collect(Collectors.toMap(SymbolData::getSymbol, SymbolData::getOpen));

		tradeReportDetails.stream().forEach(report -> {

			Double adrValue =report.getChartDetails().getAdr();
			if (adrValue != null) {
				Double profitTarget = 2 * adrValue;
				Double stopLoss = 1 * adrValue;
				
				report.setTarget_profit(profitTarget);
				report.setStop_loss(stopLoss);
				targetProfitValues.put(report.getSymbol(), profitTarget);
				stopLossValues.put(report.getSymbol(), stopLoss);
			}

		});

		System.out.println("targetprofitvalues are =========>" + targetProfitValues);
		System.out.println("stopLossValues are =========>" + stopLossValues);

		//exitCalculationforLongTrades();
		//exitCalculationforShortTrades();
		System.out.println("LONG AND SHORT TRADE  DETAILS are =========>" + tradeReportDetails);

	}

	// calculation for long trades

	public void exitCalculationforLongTrades() {
		
		System.out.println("EXIT CALCULATION FOR LONG TRADES ENTERED");

		List<TradeReport> longTrades = tradeReportDetails.stream()
				.filter(report -> report.getTradeType().equals("long")).collect(Collectors.toList());

		longTrades.stream().forEach(trade -> {

			Double targetExitprofitPrice =  trade.getChartDetails().getSymbolData().getHigh()+targetProfitValues.get(trade.getSymbol());
			
		int profitValue=	Double.compare(targetExitprofitPrice, trade.getChartDetails().getSymbolData().getHigh());
			if (profitValue==0||profitValue>0) {

				trade.setExit_price(trade.getChartDetails().getSymbolData().getHigh());
				tradeReportDetails.add(trade);

			}
			Double targetExitPriceLoss = trade.getChartDetails().getSymbolData().getHigh()-stopLossValues.get(trade.getSymbol()) ;
			
			int lossValue=	Double.compare(targetExitPriceLoss, trade.getChartDetails().getSymbolData().getHigh());
			if (lossValue==0||lossValue<0) {

				trade.setExit_price(trade.getChartDetails().getSymbolData().getHigh());
				tradeReportDetails.add(trade);

			}
			// for buy signal 
			Double targetExitPriceOpen = targetProfitValues.get(trade.getSymbol()) + trade.getChartDetails().getSymbolData().getHigh();
			if (targetExitPriceOpen < trade.getChartDetails().getSymbolData().getOpen()) {

				trade.setExit_price(trade.getChartDetails().getSymbolData().getOpen());
				tradeReportDetails.add(trade);

			}

		}

		);
		reportBatch.saveTradeReportsBatch(tradeReportDetails);
	}

	// calculation for short trades
	public void exitCalculationforShortTrades() {

		List<TradeReport> shortTrades = tradeReportDetails.stream()
				.filter(report -> report.getTradeType().equals("short")).collect(Collectors.toList());

		shortTrades.stream().forEach(trade -> {

			if (targetProfitValues.get(trade.getSymbol()).equals(lowValues.get(trade.getSymbol()))) {

				trade.setExit_price(lowValues.get(trade.getSymbol()) - targetProfitValues.get(trade.getSymbol()));
				tradeReportDetails.add(trade);

			}

			if (stopLossValues.get(trade.getSymbol()).equals(lowValues.get(trade.getSymbol()))) {

				trade.setExit_price(lowValues.get(trade.getSymbol()) + trade.getStop_loss());
				tradeReportDetails.add(trade);

			}

			// for sell signal

			if (targetProfitValues.get(trade.getSymbol()) > openValues.get(trade.getSymbol())) {

				trade.setExit_price(openValues.get(trade.getSymbol()));
				tradeReportDetails.add(trade);

			}

		}

		);

	}

	/*
	 * • Change in Colour: If the Price does not hit the ‘Profit Target’ NOR the
	 * ‘Stop Loss’, and there is a day with a ‘Black’ bar, we have to EXIT at the
	 * ‘Open’ of the ‘Next’ day/bar following the Black bar
	 */
	public void NeitherProfitTargetNorstopLoss() {

	}
}

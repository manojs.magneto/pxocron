package com.magneto.pxocron.services.cron.cron3;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.repositories.Generationcharts;
import com.magneto.pxocron.repositories.SymbolDataRepo;

@Service
public class TradeEntryProcess {

	@Autowired
	LongEntryCriteria longEntryCriteria;
	@Autowired
	ShortEntryCriteria shortEntryCriteria;

	public void tradeEntryProcess()  {

		try {
			longEntryCriteria.generatinLongEntry();
			shortEntryCriteria.generatingShortEntry();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//shortEntryCriteria.generatingShortEntry();

	}

}

package com.magneto.pxocron.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.controllers.PxoController;
import com.magneto.pxocron.dto.IndicatorIntermediate;
import com.magneto.pxocron.entities.MACDCalculatorOutput;
import com.magneto.pxocron.entities.RelativeStrength;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.repositories.MacdCalculatorOutputRepo;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.utils.CurrentDateWeekendChecker;
import com.magneto.pxocron.utils.WeekendChecker;

@Service
public class MACDCalculator {

	private static final Logger LOGGER = LoggerFactory.getLogger(MACDCalculator.class);

	@Autowired
	SymbolDataRepo symbolDataRepo;

	@Autowired
	MacdCalculatorOutputRepo macdCalculatorOutputRepo;

	@Autowired
	MACDCalculatorBatchInsertion batchInsertion;
	Calendar startCalenderFinal = Calendar.getInstance();

	private final double EMA_12_factor = 0.1538;
	private final double EMA_26_factor = 0.0741;

	private final double signalLine_factor = 0.2;

	private double previousEMA12 = 15.38;
	private double previousEMA26 = 7.41;
	private double previoussignalLine = 3.2;

	private List<Double> ema12List;
	private List<Double> ema26List;
	private List<Double> macdLine;
	private Map<String, Double> macdLineMap = new LinkedHashMap();

	private List<Double> signalLine;
	private List<Double> macdHistogram;
	private List<Double> todayValues;
	private Map<String, Double> ema12ListMap;
	private Map<String, Double> ema26ListMap;
	private Map<String, Double> signalLineMap;
	private Map<String, Double> macdHistogramMap;

	private Map<String, Integer> result;

	private List<String> colorList;
	Map<String, Integer> colorMap;

	WeekendChecker weekendChecker = new WeekendChecker();
	CurrentDateWeekendChecker weekendCheckeDater = new CurrentDateWeekendChecker();

	Calendar startCalender = Calendar.getInstance();
	Calendar endCalender = Calendar.getInstance();
	Calendar tempCalender = Calendar.getInstance();

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Map<String, SymbolData> todayIds;

	public IndicatorIntermediate MACDCalculation(HashMap<String, Object> paramaeters) throws ParseException {
		dateFixer(paramaeters);

		ema12ListMap = new HashMap<>();
		ema26ListMap = new HashMap<>();
		signalLineMap = new HashMap<>();
		macdHistogramMap = new HashMap<>();

		result = new HashMap<>();

		colorMap = new HashMap<>();

		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(startCalender, endCalender);

		// weekendCheckeDater.weekendCheckerDate(endCalender);

		LOGGER.info("today calender value is {}", dateFormat.format(endCalender.getTime()));

		LOGGER.info("yesterday calender value is {}", dateFormat.format(startCalender.getTime()));

		// weekendChecker.weekendChecker(startCalender, endCalender);
		// startCalender.add(Calendar.DATE, -2);

		List<SymbolData> todayList = symbolDataRepo.findAllByEodDate(dateFormat.format(endCalender.getTime()));

		todayIds = todayList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol)).entrySet().stream()
				.collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue().stream().findFirst().get()));
		;

		List<MACDCalculatorOutput> macdyesterdayList = macdCalculatorOutputRepo
				.findAllByEodDate(dateFormat.format(startCalender.getTime()));

		ema12List = new ArrayList<Double>();
		ema26List = new ArrayList<Double>();
		macdLine = new ArrayList<Double>();
		signalLine = new ArrayList<Double>();
		macdHistogram = new ArrayList<Double>();

		// fetching the data and mapping into symbol,close
		Map<String, Double> groupedTodayData = todayList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol))
				.entrySet().stream().collect(Collectors.toMap(map -> map.getKey(),
						map -> map.getValue().stream().findFirst().get().getClose()));

		Map<String, Double> groupedmacdema12 = macdyesterdayList.stream()
				.collect(Collectors.groupingBy(MACDCalculatorOutput::getSymbol)).entrySet().stream().collect(Collectors
						.toMap(map -> map.getKey(), map -> map.getValue().stream().findFirst().get().getEma12()));

		Map<String, Double> groupedmacdema26 = macdyesterdayList.stream()
				.collect(Collectors.groupingBy(MACDCalculatorOutput::getSymbol)).entrySet().stream().collect(Collectors
						.toMap(map -> map.getKey(), map -> map.getValue().stream().findFirst().get().getEma26()));

		Map<String, Double> groupedmacdsignalline = macdyesterdayList.stream()
				.collect(Collectors.groupingBy(MACDCalculatorOutput::getSymbol)).entrySet().stream().collect(Collectors
						.toMap(map -> map.getKey(), map -> map.getValue().stream().findFirst().get().getSignalLine()));

		LOGGER.info("todayList value is {}", todayList);

		LOGGER.info("macdyesterdayList value is {}", macdyesterdayList);
		LOGGER.info("groupedmacdema12 value is {}", groupedmacdema12);

		LOGGER.info("groupedmacdema26 value is {}", groupedmacdema26);

		todayValues = new ArrayList(groupedTodayData.values());

		groupedTodayData.forEach((symbol, close) -> {
			// yes, we can put logic here

			if (groupedmacdema12.containsKey(symbol)) {

				Double todayema12 = EMA_12_factor
						* (Double.valueOf(groupedTodayData.get(symbol)) - groupedmacdema12.get(symbol))
						+ groupedmacdema12.get(symbol);

				Double todayema26 = EMA_26_factor
						* (Double.valueOf(groupedTodayData.get(symbol)) - groupedmacdema26.get(symbol))
						+ groupedmacdema26.get(symbol);
				ema12ListMap.put(symbol, todayema12);
				ema26ListMap.put(symbol, todayema26);

			}
		});

		LOGGER.info("todayema12 value is {}", ema12ListMap);

		LOGGER.info("todayema26 value is {}", ema26ListMap);

		// calculating the macd line

		ema12ListMap.forEach((k, v) ->

		{

			macdLineMap.put(k, ema12ListMap.get(k) - ema26ListMap.get(k));

		}

		);

		// calculating the signal line

		macdLineMap.forEach((symbol, value) -> {

			double signalLineCalculated = signalLine_factor
					* (macdLineMap.get(symbol) - groupedmacdsignalline.get(symbol)) + groupedmacdsignalline.get(symbol);
			signalLine.add(signalLineCalculated);

			signalLineMap.put(symbol, signalLineCalculated);
		});

		macdLineMap.forEach((symbol, value) -> {

			macdHistogramMap.put(symbol, macdLineMap.get(symbol) - signalLineMap.get(symbol));

		});
		LOGGER.info("signalLineMap value is {}", signalLineMap);
		LOGGER.info("macdLineMap value is {}", macdLineMap);

		// calculating the macd histogram

		LOGGER.info("macdHistogram value is {}", macdHistogramMap);

		macdHistogramMap.forEach((symbol, value) -> {

			if (value > 0)
				colorMap.put(symbol, 1);
			else
				colorMap.put(symbol, 2);

		});
		LOGGER.info("result value is {}", colorMap);

		List<MACDCalculatorOutput> macdList = new ArrayList<>();

		macdHistogramMap.forEach((k, v) ->

		{

			MACDCalculatorOutput macdCalculatorOutput = new MACDCalculatorOutput();

			macdCalculatorOutput.setEodDate(dateFormat.format(tempCalender.getTime()));
			macdCalculatorOutput.setEma12(ema12ListMap.get(k));
			macdCalculatorOutput.setEma26(ema26ListMap.get(k));
			macdCalculatorOutput.setSignalLine(signalLineMap.get(k));
			macdCalculatorOutput.setMacdLine(macdLineMap.get(k));
			macdCalculatorOutput.setMacdHistogram(macdHistogramMap.get(k));
			macdCalculatorOutput.setSymbol(k);
			macdCalculatorOutput.setColor(colorMap.get(k));
			macdCalculatorOutput.setSymbolData(todayIds.get(k));

			macdList.add(macdCalculatorOutput);

		}

		);
		startCalender = Calendar.getInstance();
		batchInsertion.saveMacd(macdList);

		IndicatorIntermediate intermediateValues = new IndicatorIntermediate();

		intermediateValues.setOriginalValues(macdLineMap);
		intermediateValues.setColors(colorMap);
		intermediateValues.setIds(todayIds);

		return intermediateValues;
	}

	public void dateFixer(HashMap<String, Object> paramaeters) throws ParseException {
		// TODO Auto-generated method stub

		Date date;
		String fixedDate = (String) paramaeters.get("date");

		if (fixedDate == null) {
			date = Calendar.getInstance().getTime();
		} else {
			date = date = dateFormat.parse(fixedDate);
		}

		endCalender.setTime(date);
		startCalender.setTime(date);
		tempCalender.setTime(date);

		startCalender.add(Calendar.DATE, -1);

		HolidaysAnWeekendChecker.singleWeekendChecker(endCalender);
		HolidaysAnWeekendChecker.singleWeekendChecker(tempCalender);

	}
}

package com.magneto.pxocron.services;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.dao.StockExchangeDao;
import com.magneto.pxocron.dto.IndicatorIntermediate;
import com.magneto.pxocron.dto.StockDetails;
import com.magneto.pxocron.entities.RelativeStrength;
import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.repositories.RelativeStrengthOutput;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.utils.CurrentDateWeekendChecker;
import com.magneto.pxocron.utils.WeekendChecker;

@Service
public class RelativeStrengthIndexCalculator {

	private static final Logger LOGGER = LoggerFactory.getLogger(RelativeStrengthIndexCalculator.class);

	@Autowired
	SymbolDataRepo symbolDataRepo;

	@Autowired
	RelativeStrengthOutput strengthRepo;

	@Autowired
	StockExchangeDao stockDao;

	@Autowired
	RelativeStrengthBatchInsertion rsibatchInsertion;

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	Calendar startCalender = Calendar.getInstance();
	Calendar endCalender = Calendar.getInstance();
	Map<String, List<SymbolData>> symbolDatadDateMapping;

	List<Double> changeList;
	Map<String, Double> changeMap ;
	Map<String, Double> avgGainMap;
	Map<String, Double> avgLossMap;
	List<RelativeStrength> rsList;

	List<Double> gain;
	List<Double> loss;

	List<Double> averageGain;
	List<Double> averageLoss;
	List<Double> rs;
	Map<String, Double> rsMap ;
	Map<String, Double> rsiMap ;
	Map<String, Double> gainMap;
	Map<String, Double> lossMap;

	List<Double> rsi;
	Map<String,Integer> colorMap;

	WeekendChecker weekendChecker = new WeekendChecker();

	CurrentDateWeekendChecker weekendCheckeDater = new CurrentDateWeekendChecker();
	Calendar tempCalender = Calendar.getInstance();


	public IndicatorIntermediate relativeStrengthIndexCalculation(HashMap<String, Object> paramaeters)
			throws ParseException {

		dateFixer(paramaeters);

		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(startCalender, endCalender);

		// weekendCheckeDater.weekendCheckerDate(endCalender);

		// weekendChecker.weekendChecker(startCalender, endCalender);
		// startCalender.add(Calendar.DATE, -2);

		changeList = new ArrayList<Double>();
		rs = new ArrayList<Double>();
		averageGain = new ArrayList<Double>();
		averageLoss = new ArrayList<Double>();
		rsMap = new HashMap<>();
		rsiMap = new HashMap<>();
		changeMap = new LinkedHashMap();
		avgGainMap = new HashMap<>();
		avgLossMap = new HashMap<>();
		rsList = new ArrayList<>();
		colorMap = new HashMap<>();

		LOGGER.info("the startcalendar {} value is", startCalender.getTime());
		LOGGER.info("the endcalendar {} value is", endCalender.getTime());

		List<RelativeStrength> relativeStrengthList = stockDao
				.relativeStrengthData(dateFormat.format(startCalender.getTime()));

		// previous days average gain and average loss where key is the symbol and value
		// is the average gain value ,average loss value

		Map<String, Double> previousAvgGain = relativeStrengthList.stream()
				.collect(Collectors.groupingBy(RelativeStrength::getSymbol)).entrySet().stream()
				.collect(Collectors.toMap(map -> map.getKey(),
						map -> map.getValue().stream().findFirst().get().getStrengthAvgProfit()));

		Map<String, Double> previousAvgLoss = relativeStrengthList.stream()
				.collect(Collectors.groupingBy(RelativeStrength::getSymbol)).entrySet().stream()
				.collect(Collectors.toMap(map -> map.getKey(),
						map -> map.getValue().stream().findFirst().get().getStrengthAvgLoss()));

		LOGGER.info("the previousAvgGain {} value is", previousAvgGain);
		LOGGER.info("the previousAvgLoss {} value is", previousAvgLoss);

		// fetching the today data
		List<SymbolData> todayList = symbolDataRepo.findAllByEodDate(dateFormat.format(endCalender.getTime()));

		// fetching the one day back data
		List<SymbolData> yesterdayList = symbolDataRepo.findAllByEodDate(dateFormat.format(startCalender.getTime()));

		// grouping the data and fetching the data as key value pairs,where key is the
		// symbol and value is the close value for one day ago data

		Map<String, Double> groupedYesterdayData = yesterdayList.stream()
				.collect(Collectors.groupingBy(SymbolData::getSymbol)).entrySet().stream().collect(Collectors
						.toMap(map -> map.getKey(), map -> map.getValue().stream().findFirst().get().getClose()));

		// grouping the data and fetching the data as key value pairs,where key is the
		// symbol and value is the close value for today data

		Map<String, Double> groupedTodayData = todayList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol))
				.entrySet().stream().collect(Collectors.toMap(map -> map.getKey(),
						map -> map.getValue().stream().findFirst().get().getClose()));

		// fetching for the ids to store it in db

		Map<String, SymbolData> todayIds = todayList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol))
				.entrySet().stream()
				.collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue().stream().findFirst().get()));
		;

		LOGGER.info("yesterday size {}", groupedYesterdayData.size());
		LOGGER.info("today size {}", groupedTodayData.size());

		groupedYesterdayData.entrySet().stream().forEach(item -> {
			LOGGER.info("the entrysetyesterdate {} {} value is", item.getKey() + item.getValue());

		});

		groupedTodayData.entrySet().stream().forEach(item -> {
			LOGGER.info("the groupedTodayData {} value is", item.getKey() + item.getValue());

		});

		// making the changelist,checking for the same symbol and getting the close
		// value and doing subtraction and adding it to the change list

		todayList.forEach(item -> {

			// changeList.add(Double.valueOf(groupedTodayData.get(item.getSymbol())) -
			// Double.valueOf(groupedYesterdayData.get(item.getSymbol())));

			double left = Double.valueOf(groupedTodayData.get(item.getSymbol()));

			if (groupedYesterdayData.containsKey(item.getSymbol())) {

				double right = Double.valueOf(groupedYesterdayData.get(item.getSymbol()));

				changeMap.put(item.getSymbol(), left - right);
			}

		});

		LOGGER.info("the changeList value is  {}", changeMap);

		// checking the condition for gain value, displaying the original value if it is
		// greater than 0,displaying the 0 if it is negative

		gainMap = changeMap.entrySet().stream()
				.collect(Collectors.toMap(entry -> entry.getKey(), entry -> Math.max(0, entry.getValue())));

		// checking the condition for the loss value,displaying the absolute value if it
		// is less than 0,displaying the 0 if it is 0

		lossMap = changeMap.entrySet().stream().collect(Collectors.toMap(entry -> entry.getKey(),
				entry -> Math.max(0, Math.abs(Math.min(entry.getValue(), 0)))));

		LOGGER.info("the gainMap value is  {}", gainMap);
		LOGGER.info("the lossMap value is  {}", lossMap);

		/*
		 * List<Map<String,Double>>
		 * mapList=smaCalculation.smaFormulaCalculation(dateFormat.format(endCalender.
		 * getTime()));
		 * 
		 * 
		 * Map<String,Double> positiveMap=mapList.get(0); Map<String,Double>
		 * negativeMap= mapList.get(1);
		 */

		// calculating the average gain

		changeMap.forEach((symbol, change) -> {
			// yes, we can put logic here

			if (previousAvgGain.containsKey(symbol)) {

				Double todayGain = ((previousAvgGain.get(symbol) * 6) + gainMap.get(symbol)) / 7;

				Double todayLoss = ((previousAvgLoss.get(symbol) * 6) + lossMap.get(symbol)) / 7;
				avgGainMap.put(symbol, todayGain);
				avgLossMap.put(symbol, todayLoss);

			}
		});

		LOGGER.info("the averageGainmap value is  {}", avgGainMap);

		LOGGER.info("the averageLossmap value is  {}", avgLossMap);

		// calculating the rs

		avgGainMap.forEach((k, v) ->

		{
			Double value = avgGainMap.get(k) / avgLossMap.get(k);
			if (Double.isNaN(value) || Double.POSITIVE_INFINITY == value || Double.NEGATIVE_INFINITY == value) {

				value = 0.0;
			}
			rsMap.put(k, value);

		}

		);

		LOGGER.info("the rs value is  {}", rsMap);

		// calculating the rsi

		avgLossMap.forEach((k, v) ->

		{

			if (avgLossMap.get(k) == 0)
				rsiMap.put(k, (double) 100);

			else
				rsiMap.put(k, 100 - (100 / (1 + rsMap.get(k))));

		}

		);

		LOGGER.info("the rsi value is  {}", rsiMap);

		rsiMap.forEach((k, v) ->

		{
			if (v >= 50.0) {

				colorMap.put(k, 1);
			}

			else {
				colorMap.put(k, 2);

			}

		});
		rsiMap.forEach((k, v) ->

		{

			RelativeStrength relativeStrength = new RelativeStrength();

			relativeStrength.setEodDate(dateFormat.format(tempCalender.getTime()));
			relativeStrength.setRs(rsMap.get(k));
			relativeStrength.setRsi(rsiMap.get(k));
			relativeStrength.setStrengthAvgLoss(avgLossMap.get(k));
			relativeStrength.setStrengthAvgProfit(avgGainMap.get(k));
			relativeStrength.setStrengthChange(changeMap.get(k));
			relativeStrength.setStrengthLoss(lossMap.get(k));
			relativeStrength.setStrengthProfit(gainMap.get(k));
			relativeStrength.setColor(colorMap.get(k));
			relativeStrength.setSymbolData(todayIds.get(k));
			;
			relativeStrength.setSymbol(k);
			rsList.add(relativeStrength);

		}

		);

		startCalender = Calendar.getInstance();

		// storing the final outcome into the database

		rsibatchInsertion.saveRsi(rsList);
		IndicatorIntermediate intermediateValues = new IndicatorIntermediate();

		intermediateValues.setOriginalValues(rsiMap);
		intermediateValues.setColors(colorMap);
		intermediateValues.setIds(todayIds);

		return intermediateValues;
	}

	public void dateFixer(HashMap<String, Object> paramaeters) throws ParseException {
		// TODO Auto-generated method stub
		
		Date date;
		String fixedDate=(String) paramaeters.get("date");
		
		
		if(fixedDate==null)
		{
			date=Calendar.getInstance().getTime();
		}
		else
		{
			date=date = dateFormat.parse(fixedDate);
		}

		endCalender.setTime(date);
		startCalender.setTime(date);
		tempCalender.setTime(date);

		startCalender.add(Calendar.DATE, -1);
		
		HolidaysAnWeekendChecker.singleWeekendChecker(endCalender);
		HolidaysAnWeekendChecker.singleWeekendChecker(tempCalender);

	}

}

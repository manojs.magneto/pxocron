package com.magneto.pxocron.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.dto.AllCronDataResponse;
import com.magneto.pxocron.dto.AllCronResponse;
import com.magneto.pxocron.dto.Cron;
import com.magneto.pxocron.dto.DataResponse;
import com.magneto.pxocron.dto.Response;
import com.magneto.pxocron.entities.CronDetailsUs;
import com.magneto.pxocron.repositories.DailyCronDetailsRepo;

@Service
public class DailyCronDetails {

	@Autowired
	DailyCronDetailsRepo repo;

	public void savingCronDetails(CronDetailsUs details) {

		repo.save(details);

	}

	public AllCronDataResponse findAll() {

		AllCronDataResponse allResposne = new AllCronDataResponse();

		long totalStart = 0;
		long totalEnd = 0;

		
		allResposne.setAlljobsstatus(2);
		CronDetailsUs cronDetail = repo.findTopByOrderByDateDesc();
		DataResponse response = new DataResponse();
		List<DataResponse> detailList = new ArrayList<>();
		if (cronDetail.getCron1_start() != 0) {
			response.setCron("cron1");
			response.setStartTime(cronDetail.getCron1_start());
			response.setEndTime(cronDetail.getCron1_end());
			response.setStatus(cronDetail.getCron1_status());

			totalStart = totalStart + cronDetail.getCron1_start();
			totalEnd = totalEnd + cronDetail.getCron1_end();

			detailList.add(response);
		}

		if (cronDetail.getCron2_start() != 0) {
			DataResponse response1 = new DataResponse();
			totalStart = totalStart + cronDetail.getCron2_start();
			totalEnd = totalEnd + cronDetail.getCron2_end();

			response1.setCron("cron2");
			response1.setStartTime(cronDetail.getCron2_start());
			response1.setEndTime(cronDetail.getCron2_end());
			response1.setStatus(cronDetail.getCron2_status());
			detailList.add(response1);
		}

		if (cronDetail.getCron3_start() != 0) {
			DataResponse response2 = new DataResponse();
			totalStart = totalStart + cronDetail.getCron3_start();
			totalEnd = totalEnd + cronDetail.getCron3_end();

			response2.setCron("cron3");
			response2.setStartTime(cronDetail.getCron3_start());
			response2.setEndTime(cronDetail.getCron3_end());
			response2.setStatus(cronDetail.getCron3_status());
			detailList.add(response2);
		}

		if (cronDetail.getCron4_start() != 0) {
			DataResponse response3 = new DataResponse();
			totalStart = totalStart + cronDetail.getCron4_start();
			totalEnd = totalEnd + cronDetail.getCron4_end();

			response3.setCron("cron4");
			response3.setStartTime(cronDetail.getCron4_start());
			response3.setEndTime(cronDetail.getCron4_end());
			response3.setStatus(cronDetail.getCron4_status());
			detailList.add(response3);
		}

		if (cronDetail.getCron5_start() != 0) {
			DataResponse response4 = new DataResponse();
			totalStart = totalStart + cronDetail.getCron5_start();
			totalEnd = totalEnd + cronDetail.getCron5_end();

			response4.setCron("cron5");
			response4.setStartTime(cronDetail.getCron5_start());
			response4.setEndTime(cronDetail.getCron5_end());
			response4.setStatus(cronDetail.getCron5_status());
			detailList.add(response4);
		}
		if (cronDetail.getCron6_start() != 0) {
			DataResponse response5 = new DataResponse();

			totalStart = totalStart + cronDetail.getCron6_start();
			totalEnd = totalEnd + cronDetail.getCron6_end();

			response5.setCron("cron6");
			response5.setStartTime(cronDetail.getCron6_start());
			response5.setEndTime(cronDetail.getCron6_end());
			response5.setStatus(cronDetail.getCron6_status());
			detailList.add(response5);
		}

		if (cronDetail.getCron7_start() != 0) {
			DataResponse response6 = new DataResponse();
			totalStart = totalStart + cronDetail.getCron7_start();
			totalEnd = totalEnd + cronDetail.getCron7_end();

			response6.setCron("cron7");
			response6.setStartTime(cronDetail.getCron7_start());
			response6.setEndTime(cronDetail.getCron7_end());
			response6.setStatus(cronDetail.getCron7_status());
			detailList.add(response6);
		}

		if (cronDetail.getCron8_start() != 0) {
			DataResponse response7 = new DataResponse();
			totalStart = totalStart + cronDetail.getCron8_start();
			totalEnd = totalEnd + cronDetail.getCron8_end();

			response7.setCron("cron8");
			response7.setStartTime(cronDetail.getCron8_start());
			response7.setEndTime(cronDetail.getCron8_end());
			response7.setStatus(cronDetail.getCron8_status());
			detailList.add(response7);
		}

		if (cronDetail.getCron9_start() != 0) {
			DataResponse response8 = new DataResponse();
			totalStart = totalStart + cronDetail.getCron9_start();
			totalEnd = totalEnd + cronDetail.getCron9_end();

			response8.setCron("cron9");
			response8.setStartTime(cronDetail.getCron9_start());
			response8.setEndTime(cronDetail.getCron9_end());
			response8.setStatus(cronDetail.getCron9_status());
			detailList.add(response8);
		}

		allResposne.setAllCronJobs(detailList);
		allResposne.setDate(cronDetail.getDate());
		allResposne.setAlljobsstarttime(totalStart);
		allResposne.setAlljobsendtime(totalEnd);
		return allResposne;

	}

}

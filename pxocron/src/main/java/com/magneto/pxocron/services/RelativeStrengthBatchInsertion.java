package com.magneto.pxocron.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magneto.pxocron.controllers.PxoController;
import com.magneto.pxocron.entities.RelativeStrength;
import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.repositories.RelativeStrengthOutput;
import com.magneto.pxocron.repositories.StochasticCalculatorOutputRepo;

//batch insertion for rsi which increases the overall performance

@Service
public class RelativeStrengthBatchInsertion {

	@Autowired
	RelativeStrengthOutput rsiRepo;

	private static final Logger LOGGER = LoggerFactory.getLogger(RelativeStrengthBatchInsertion.class);
	@Transactional
	public void saveRsi(List<RelativeStrength> rsiList) {

		int size = rsiList.size();
		int counter = 0;

		List<RelativeStrength> temporaryList = new ArrayList<>();

		for (RelativeStrength relativeStrength : rsiList) {
			temporaryList.add(relativeStrength);

			if ((counter + 1) %500 == 0 || (counter + 1) == size) {

				System.out.println("storing the data");
				LOGGER.info("storing the data into database");
				

				rsiRepo.saveAll(temporaryList);
				

				LOGGER.info("inserted the data into database");

				temporaryList.clear();

			}

			counter++;
		}

		System.out.println(" done storing the data");

	}

}
package com.magneto.pxocron.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.assertj.core.api.Assertions;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.magneto.pxocron.controllers.PxoController;
import com.magneto.pxocron.dao.StockExchangeDao;
import com.magneto.pxocron.dto.IndicatorIntermediate;
import com.magneto.pxocron.dto.StockDetails;
import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.repositories.StochasticCalculatorOutputRepo;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.utils.SingleDateChecker;
import com.magneto.pxocron.utils.WeekendChecker;

@Service
public class StochasticCalculator {

	private static final Logger LOGGER = LoggerFactory.getLogger(StochasticCalculator.class);

	@Autowired
	StockExchangeDao stockExchangeDao;

	@Autowired
	StochasticCalculatorOutputRepo stochasticOutputRepo;

	@Autowired
	SymbolDataRepo symbolDataRepo;

	@Autowired

	StochasticBatchInsertion batchInsertion;

	@Autowired
	StochasticCalculatorOutputRepo stochasticRepo;

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Calendar start = Calendar.getInstance();
	Calendar end = Calendar.getInstance();
	Calendar compare = Calendar.getInstance();
	Calendar mapCompare = Calendar.getInstance();
	Calendar CurrentDate = Calendar.getInstance();
	Calendar startCalender = Calendar.getInstance();
	Calendar endCalender = Calendar.getInstance();
	Calendar startCalenderPercentD = Calendar.getInstance();
	Calendar endCalenderPercentD = Calendar.getInstance();
	Calendar tempCalender1 = Calendar.getInstance();

	Map<String, Double> groupedYesterdayDatapercentageDwithoutSma;
	Map<String, Double> groupedYesterdayDatapercentageD;

	Map<String, Optional<SymbolData>> symbolDataMapMaxValues;

	Map<String, Double> symbolDataMapMaxValuesMapped;

	Map<String, Double> symbolDataMapMinValuesMapped;
	Map<String, Double> toaypercentageDResultwithoutSma;
	Map<String, Double> toaypercentageDResult;

	Map<String, Optional<SymbolData>> symbolDataMapMinValues;

	Map<String, Double> temporaryMap;
	Map<String, Double> percentageDwithoutSMA;
	Map<String, Double> percentageDAverage;

	List<Double> lowValues;
	List<Double> highValues;
	List<Double> numerator;
	List<Double> denominator;
	List<Double> percentageK;
	List<Double> percentageDWihtoutSMA;

	List<Double> percentageD;
	Map<String, Integer> colorMap;

	double high;
	double low;
	WeekendChecker weekendChecker = new WeekendChecker();
	List<SymbolData> symbolDataList;

	List<SymbolData> filteredSymbolDataList;

	List<String> filteredList;

	List<SymbolData> symbolDataTodayList;

	List<StochasticOutput> stochasticList;

	Map<String, Double> percentageKMap;

	Map<String, SymbolData> todayIds;
	Calendar tempCalender = Calendar.getInstance();
	Calendar storableDate = Calendar.getInstance();


	public IndicatorIntermediate stochasticHightLowCalculation(HashMap<String, Object> paramaeters) throws Exception

	{
		toaypercentageDResultwithoutSma = new HashMap<>();
		toaypercentageDResult = new HashMap<>();
		colorMap = new HashMap<>();

		dateFixer(paramaeters);
		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(startCalender, endCalender);

		LOGGER.info("the startcalendar {} value is", startCalender.getTime());
		LOGGER.info("the endcalendar {} value is", endCalender.getTime());

		symbolDataList = symbolDataRepo.findAllByEodDateBetween(dateFormat.format(startCalender.getTime()),
				dateFormat.format(endCalender.getTime()));

		symbolDataTodayList = symbolDataRepo.findAllByEodDate(dateFormat.format(endCalender.getTime()));

		// today ids
		todayIds = symbolDataTodayList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol)).entrySet()
				.stream()
				.collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue().stream().findFirst().get()));
		;

		filteredList = symbolDataList.stream().map(stock -> stock.getSymbol()).collect(Collectors.toList());

		filteredSymbolDataList = symbolDataList.stream().filter(stock -> filteredList.contains(stock.getSymbol()))
				.collect(Collectors.toList());

		symbolDataMapMaxValuesMapped = new HashMap<String, Double>();
		symbolDataMapMinValuesMapped = new HashMap<String, Double>();
		percentageKMap = new HashMap<>();
		percentageK = new ArrayList<Double>();

		// fetching the maximum of last 14 days

		symbolDataMapMaxValues = filteredSymbolDataList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol,
				TreeMap::new, Collectors.maxBy(Comparator.comparing(SymbolData::getHigh))));

		// fetching the minimum of last 14 days

		symbolDataMapMinValues = filteredSymbolDataList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol,
				TreeMap::new, Collectors.minBy(Comparator.comparing(SymbolData::getLow))));

		// mapping the maximum values to key value pairs where key is symbol and value
		// maximum of high value for last 14 days
		symbolDataMapMaxValues.entrySet().stream().forEach(item -> {

			symbolDataMapMaxValuesMapped.put(item.getKey(), Double.valueOf(item.getValue().get().getHigh()));

		});

		LOGGER.info("the symbolDataMapMaxValuesMapped {} value is", symbolDataMapMaxValues);

		// mapping the minimum values to key value pairs where key is symbol and value
		// is minimum of low values for last 14 days

		symbolDataMapMinValues.entrySet().stream().forEach(item -> {

			symbolDataMapMinValuesMapped.put(item.getKey(), Double.valueOf(item.getValue().get().getLow()));
			// lowValues.add()

		});

		LOGGER.info("the symbolDataMapMinValuesMapped {} value is", symbolDataMapMaxValues);

		// converting the previously calculated maps in to the lists of values
		// containing high,low values

		// calculating the percentage k value where numberator is todays close value
		// -lowest of low,denominator is highest of high - lowest of low

		for (SymbolData data : symbolDataTodayList) {

			double numerator = Double.valueOf(data.getClose())
					- Double.valueOf(symbolDataMapMinValuesMapped.get(data.getSymbol()));

			double denominator = Double.valueOf(symbolDataMapMaxValuesMapped.get(data.getSymbol()))
					- Double.valueOf(symbolDataMapMinValuesMapped.get(data.getSymbol()));

			LOGGER.info("the numerator {} value is", numerator);
			LOGGER.info("the denominator {} value is", denominator);

			// numerator / denominator);

			double value = (numerator / denominator);
			LOGGER.info("the value {} value is", value);

			if (Double.isNaN(value))

				value = 0.0;

			percentageKMap.put(data.getSymbol(), value * 100);

		}

		LOGGER.info("the percentageKMap {} value is", percentageKMap);


		return percentageDCalculationWithoutSMA();

	}

	// calculating the percentage d value is average of two value of %k

	public IndicatorIntermediate percentageDCalculationWithoutSMA() throws ParseException {

		
		tempCalender.add(Calendar.DATE, -2);
		
		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(tempCalender, endCalender);


		stochasticList = stochasticOutputRepo.getAllBetweenDates(dateFormat.format(tempCalender.getTime()),
				dateFormat.format(endCalender.getTime()));
		
		LOGGER.info("the startdate {} value is",dateFormat.format(tempCalender.getTime()));
		LOGGER.info("the enddate {} value is", dateFormat.format(endCalender.getTime()));

		LOGGER.info("the stochasticfetchedlist {} value is", stochasticList);

		Map<String, List<Double>> mapList = stochasticList.stream()
				.collect(Collectors.groupingBy(StochasticOutput::getSymbol,
						Collectors.mapping(StochasticOutput::getPercentageK, Collectors.toList())));

		mapList.entrySet().stream().forEach(e -> {
			if (percentageKMap.containsKey(e.getKey())) {

				e.getValue().add(percentageKMap.get(e.getKey()));

			}

		});

		percentageDwithoutSMA = mapList.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
				e -> e.getValue().stream().mapToDouble(Double::doubleValue).average().getAsDouble()));

		//percentageDWihtoutSMA = new ArrayList(percentageDwithoutSMA.values());

		LOGGER.info("the percentageDWihtoutSMA  {} value is", percentageDwithoutSMA);
		
		return percentageDCalculation();

	}

	public IndicatorIntermediate percentageDCalculation() {
		
		
		tempCalender1.add(Calendar.DATE, -2);
		
		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(tempCalender1, endCalender);


		stochasticList = stochasticOutputRepo.getAllBetweenDates(dateFormat.format(tempCalender1.getTime()),
				dateFormat.format(endCalender.getTime()));

		/*
		 * Map<String, Double> percentageDMap =
		 * stochasticList.stream().collect(Collectors
		 * .groupingBy(StochasticOutput::getSymbol,
		 * Collectors.averagingDouble(StochasticOutput::getPercentageD)));
		 */

		Map<String, List<Double>> mapList = stochasticList.stream()
				.collect(Collectors.groupingBy(StochasticOutput::getSymbol,
						Collectors.mapping(StochasticOutput::getPercentageDWihtoutSMA, Collectors.toList())));

		mapList.entrySet().stream().forEach(e -> {
			if (percentageDwithoutSMA.containsKey(e.getKey())) {

				e.getValue().add(percentageDwithoutSMA.get(e.getKey()));

			}

		});

		percentageDAverage = mapList.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
				e -> e.getValue().stream().mapToDouble(Double::doubleValue).average().getAsDouble()));

		percentageD = new ArrayList(percentageDAverage.values());

		LOGGER.info("the percentageDAverage  {} value is", percentageDAverage);

		return colorCoding(percentageDAverage);

	}

	public IndicatorIntermediate colorCoding(Map<String, Double> merged) {
		merged.forEach((k, v) -> {
			if (v > 50.0) {

				colorMap.put(k, 1);
			} else {
				colorMap.put(k, 2);

			}

		}

		);

		return savingStochasticOutput();
	}

	private IndicatorIntermediate savingStochasticOutput() {
		List<StochasticOutput> list = new ArrayList<StochasticOutput>();

		// storing the final outcome into the database

		percentageDAverage.forEach((k, v) ->

		{

			StochasticOutput stochasticOutput = new StochasticOutput();

			stochasticOutput.setSymbol(k);
			stochasticOutput.setHighestValues(symbolDataMapMaxValuesMapped.get(k));
			stochasticOutput.setLowestValues(symbolDataMapMinValuesMapped.get(k));
			stochasticOutput.setColor(colorMap.get(k));
			if (percentageKMap.get(k) != null) {
				stochasticOutput.setPercentageK(percentageKMap.get(k));
			} else {
				stochasticOutput.setPercentageK(0.0);

			}
			stochasticOutput.setPercentageDWihtoutSMA(percentageDwithoutSMA.get(k));
			stochasticOutput.setPercentageD(percentageDAverage.get(k));

			stochasticOutput.setDate(dateFormat.format(storableDate.getTime()));
			stochasticOutput.setSymbolData(todayIds.get(k));
			list.add(stochasticOutput);

		}

		);

		batchInsertion.saveStochastic(list);

		IndicatorIntermediate intermediateValues = new IndicatorIntermediate();

		intermediateValues.setOriginalValues(percentageDAverage);
		intermediateValues.setColors(colorMap);
		intermediateValues.setIds(todayIds);

		return intermediateValues;
	}

	public void dateFixer(HashMap<String, Object> paramaeters) throws ParseException {
		// TODO Auto-generated method stub
		Date date;
		String fixedDate = (String) paramaeters.get("date");

		if (fixedDate == null) {
			date = Calendar.getInstance().getTime();
		} else {
			date = date = dateFormat.parse(fixedDate);
		}

		endCalender.setTime(date);
		startCalender.setTime(date);
		tempCalender.setTime(date);
		tempCalender1.setTime(date);
		storableDate.setTime(date);

		startCalender.add(Calendar.DATE, -13);
		HolidaysAnWeekendChecker.singleWeekendChecker(endCalender);
		HolidaysAnWeekendChecker.singleWeekendChecker(tempCalender);
		HolidaysAnWeekendChecker.singleWeekendChecker(tempCalender1);
		HolidaysAnWeekendChecker.singleWeekendChecker(storableDate);


	}

}

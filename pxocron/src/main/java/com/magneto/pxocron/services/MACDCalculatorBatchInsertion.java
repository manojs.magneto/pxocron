package com.magneto.pxocron.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magneto.pxocron.entities.MACDCalculatorOutput;
import com.magneto.pxocron.entities.RelativeStrength;
import com.magneto.pxocron.repositories.MacdCalculatorOutputRepo;
import com.magneto.pxocron.repositories.RelativeStrengthOutput;

@Service
public class MACDCalculatorBatchInsertion {

	@Autowired
	MacdCalculatorOutputRepo macdRepo;

	private static final Logger LOGGER = LoggerFactory.getLogger(MACDCalculatorBatchInsertion.class);

	@Transactional
	public void saveMacd(List<MACDCalculatorOutput> macdList) {

		int size = macdList.size();
		int counter = 0;

		List<MACDCalculatorOutput> temporaryList = new ArrayList<>();

		for (MACDCalculatorOutput macdOutput : macdList) {
			temporaryList.add(macdOutput);

			if ((counter + 1) % 500 == 0 || (counter + 1) == size) {

				System.out.println("storing the data");
				LOGGER.info("storing the data into database");

				macdRepo.saveAll(temporaryList);

				LOGGER.info("inserted the data into database");

				temporaryList.clear();

			}

			counter++;
		}

		System.out.println(" done storing the data");

	}

}

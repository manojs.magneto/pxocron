package com.magneto.pxocron.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.dto.IndicatorIntermediate;
import com.magneto.pxocron.entities.RelativeStrength;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.utils.CurrentDateWeekendChecker;
import com.magneto.pxocron.utils.WeekendChecker;

@Service
public class RsiStockEntryPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(RsiStockEntryPoint.class);
	CurrentDateWeekendChecker weekendCheckeDater = new CurrentDateWeekendChecker();

	@Autowired
	SymbolDataRepo symbolDataRepo;

	@Autowired
	RelativeStrengthBatchInsertion rsibatchInsertion;

	Calendar startCalender = Calendar.getInstance();
	Calendar endCalender = Calendar.getInstance();
	Calendar startCalenderFinal = Calendar.getInstance();

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	List<SymbolData> symbolDataList;

	private static final Map<String, Double> changeList = null;

	Map<String, Double> averagegainList;

	Map<String, Map<String, Double>> finalMapListPositive;

	Map<String, Map<String, Double>> finalMapListNegative;
	Map<String, Double> gainAverageMap;

	Map<String, Double> lossAverageMap;
	Map<String, Double> positiveMap;
	Map<String, Double> negativeMap;
	Double right;
	Map<String, List<Double>> duplicatepositiveMap;

	Map<String, List<Double>> gainMapAverage;
	Map<String, List<Double>> duplicatenegativeMap;
	Map<String, Double> rsMap;
	Map<String, Double> rsiMap;
	Map<String, String> list1;
	Map<String, String> list2;
	List<RelativeStrength> rsList;
	Map<String, Double> changeMapToday;
	Map<String, Double> gainMapToday;
	Map<String, Double> lossMapToday;
	Calendar tempCalender = Calendar.getInstance();

	WeekendChecker weekendChecker = new WeekendChecker();

	Map<String, Integer> colorMap;

	public IndicatorIntermediate rsiCalculation(HashMap<String, Object> paramaeters) throws ParseException {

		changeMapToday = new LinkedHashMap();
		gainMapToday = new LinkedHashMap();
		lossMapToday = new LinkedHashMap();
		rsMap = new HashMap<>();
		rsiMap = new HashMap<>();
		duplicatepositiveMap = new LinkedHashMap<>();
		gainMapAverage = new HashMap<>();
		colorMap = new HashMap<>();

		dateFixer(paramaeters);

		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(startCalender, endCalender);

		// fetching the symbols data
		symbolDataList = symbolDataRepo.findAllByEodDateBetween(dateFormat.format(startCalender.getTime()),
				dateFormat.format(endCalender.getTime()));

		// grouping by date,grouping by symbol

		Map<String, Map<String, String>> dataByDateAndSymbol = symbolDataList.stream()
				.collect(Collectors.groupingBy(SymbolData::getEodDate, TreeMap::new, Collectors.groupingBy(
						SymbolData::getSymbol,
						Collectors.mapping(stock -> String.valueOf(stock.getClose()), Collectors.joining(", ")))));

		duplicatepositiveMap = new HashMap<>();
		duplicatenegativeMap = new HashMap<>();

		finalMapListPositive = new HashMap<>();

		finalMapListNegative = new HashMap<>();
		dataByDateAndSymbol.forEach((date, dateList) -> {

			dateList.forEach((symbol, symbolList) -> {

				LOGGER.info("the date symbol values {},{},{}", date, symbol, symbolList);

			});

		});

		Set<String> keySet = null;
		keySet = dataByDateAndSymbol.keySet();

		LOGGER.info("the keysetvalues {}", keySet);

		String lastKey = null;
		for (String key : keySet) {
			if (null == lastKey) {
				lastKey = key;
				continue;
			}

			list1 = dataByDateAndSymbol.get(key);

			list2 = dataByDateAndSymbol.get(lastKey);

			LOGGER.info("the key {} value is", key);

			LOGGER.info("the lastKey {} value is", lastKey);

			LOGGER.info("the list1 {} value is", list1);

			LOGGER.info("the list2 {} value is", list2);
			positiveMap = new HashMap<>();
			negativeMap = new HashMap<>();

			list1.entrySet().stream().forEach(e -> {

				Double left = Double.valueOf(e.getValue().split(",")[0]);

				if (list2.containsKey(e.getKey())) {

					right = Double.valueOf(list2.get(e.getKey()).split(",")[0]);

					Double change = left - right;

					Double gainresult = positiveCheck(change);

					Double lossresult = negativeCheck(change);
					LOGGER.info("the change {} value is", change);

					LOGGER.info("the gainresult {} {} value is", e.getKey(), gainresult);

					LOGGER.info("the lossresult {}  {}value is", e.getKey(), lossresult);

					if (duplicatepositiveMap.containsKey(e.getKey()))

					{

						duplicatepositiveMap.get(e.getKey()).add(gainresult);

						duplicatenegativeMap.get(e.getKey()).add(lossresult);

					}

					else {

						duplicatepositiveMap.put(e.getKey(), new ArrayList<>(Arrays.asList(gainresult)));

						duplicatenegativeMap.put(e.getKey(), new ArrayList<>(Arrays.asList(lossresult)));

					}

				}

			});

			LOGGER.info("the duplicatepositiveMap value is {}", duplicatepositiveMap);
			LOGGER.info("the duplicatenegativeMap value is {}", duplicatenegativeMap);

			// LOGGER.info("the positiveMap {} value is", positiveMap.entrySet());
			// LOGGER.info("the negativeMap {} value is", negativeMap.entrySet());

			// finalMapListPositive.put(key, positiveMap);

			// finalMapListNegative.put(key, negativeMap);

			lastKey = key;
		}

		// endCalender.add(Calendar.DATE, -1);
		// List<SymbolData> todayList =
		// symbolDataRepo.findAllByEodDate(dateFormat.format(endCalender.getTime()));

		LOGGER.info("the list1 value is  {}", list1);
		LOGGER.info("the list2 value is  {}", list2);
		if (list1 != null && list2 != null) {
			list1.forEach((symbol, value) -> {

				// changeList.add(Double.valueOf(groupedTodayData.get(item.getSymbol())) -
				// Double.valueOf(groupedYesterdayData.get(item.getSymbol())));

				double left = Double.valueOf(list1.get(symbol).split(",")[0]);

				if (list2.containsKey(symbol)) {
					double right = Double.valueOf(list2.get(symbol).split(",")[0]);

					changeMapToday.put(symbol, left - right);
				}

			});
		}

		LOGGER.info("the changeMapToday value is  {}", changeMapToday);

		// checking the condition for gain value, displaying the original value if it is
		// greater than 0,displaying the 0 if it is negative

		gainMapToday = changeMapToday.entrySet().stream()
				.collect(Collectors.toMap(entry -> entry.getKey(), entry -> Math.max(0, entry.getValue())));

		// checking the condition for the loss value,displaying the absolute value if it
		// is less than 0,displaying the 0 if it is 0

		lossMapToday = changeMapToday.entrySet().stream().collect(Collectors.toMap(entry -> entry.getKey(),
				entry -> Math.max(0, Math.abs(Math.min(entry.getValue(), 0)))));

		gainAverageMap = duplicatepositiveMap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
				e -> e.getValue().stream().mapToDouble(Double::doubleValue).average().getAsDouble()));

		lossAverageMap = duplicatenegativeMap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
				e -> e.getValue().stream().mapToDouble(Double::doubleValue).average().getAsDouble()));

		LOGGER.info("the gainAverageMap {} value is", gainAverageMap);
		LOGGER.info("the lossAverageMap {} value is", lossAverageMap);
		LOGGER.info("the gainMapToday value is  {}", gainMapToday);

		LOGGER.info("the lossMapToday value is  {}", lossMapToday);

		gainAverageMap.forEach((k, v) ->

		{
			Double value = gainAverageMap.get(k) / lossAverageMap.get(k);
			if (Double.isNaN(value) || Double.POSITIVE_INFINITY == value || Double.NEGATIVE_INFINITY == value) {

				value = 0.0;
			}
			rsMap.put(k, value);

		}

		);

		LOGGER.info("the rs value is  {}", rsMap);

		// calculating the rsi

		lossAverageMap.forEach((k, v) ->

		{

			if (lossAverageMap.get(k) == 0)
				rsiMap.put(k, (double) 100);

			else
				rsiMap.put(k, 100 - (100 / (1 + rsMap.get(k))));

		}

		);

		LOGGER.info("the rsi value is  {}", rsiMap);

		rsiMap.forEach((k, v) ->

		{
			if (v >= 50.0) {

				colorMap.put(k, 1);
			}

			else {
				colorMap.put(k, 2);

			}

		});

		rsList = new ArrayList<>();
		rsiMap.forEach((k, v) ->

		{

			RelativeStrength relativeStrength = new RelativeStrength();

			relativeStrength.setEodDate(dateFormat.format(tempCalender.getTime()));
			relativeStrength.setRs(rsMap.get(k));
			relativeStrength.setRsi(rsiMap.get(k));
			relativeStrength.setStrengthAvgLoss(lossAverageMap.get(k));
			relativeStrength.setStrengthAvgProfit(gainAverageMap.get(k));
			relativeStrength.setStrengthChange(changeMapToday.get(k));
			relativeStrength.setStrengthLoss(lossMapToday.get(k));
			relativeStrength.setStrengthProfit(gainMapToday.get(k));
			relativeStrength.setSymbol(k);
			relativeStrength.setColor(colorMap.get(k));
			rsList.add(relativeStrength);

		}

		);
		// resetting the calender
		startCalender = Calendar.getInstance();
		endCalender = Calendar.getInstance();

		// storing the final outcome into the database

		rsibatchInsertion.saveRsi(rsList);

		IndicatorIntermediate response = new IndicatorIntermediate();
		response.setOriginalValues(rsiMap);
		response.setColors(colorMap);
		return response;
	}

	private Double negativeCheck(Double change) {

		if (change < 0.0) {
			change = Math.abs(change);
		}

		else {

			change = 0.0;
		}

		return change;
	}

	private Double positiveCheck(Double change) {
		if (change > 0.0) {
			change = change;
		} else {
			change = 0.0;
		}

		return change;
	}

	public void dateFixer(HashMap<String, Object> paramaeters) throws ParseException {
		Date date;
		String fixedDate = (String) paramaeters.get("date");

		if (fixedDate == null) {
			date = Calendar.getInstance().getTime();
		} else {
			date = date = dateFormat.parse(fixedDate);
		}

		endCalender.setTime(date);
		startCalender.setTime(date);
		tempCalender.setTime(date);
		startCalender.add(Calendar.DATE, -8);
		endCalender.add(Calendar.DATE, -1);
		tempCalender.add(Calendar.DATE, -1);

		HolidaysAnWeekendChecker.singleWeekendChecker(endCalender);

		HolidaysAnWeekendChecker.singleWeekendChecker(tempCalender);

	}

}

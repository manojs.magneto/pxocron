package com.magneto.pxocron.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.entities.TradeDetails;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.repositories.TradeDetailsRepo;

//stock data batch insertion which increases the performance 

@Service
public class TradeDetailsBatchInsertion { 

	@Autowired
	TradeDetailsRepo tradeDetailsRepo;

	private static final Logger LOGGER = LoggerFactory.getLogger(TradeDetailsBatchInsertion.class);

	@Transactional
	public void saveTradeDetailsBatch(List<TradeDetails> tradeList) {
		int size = tradeList.size();
		int counter = 0;

		List<TradeDetails> temporaryList = new ArrayList<>();

		for (TradeDetails symbolData : tradeList) {
			temporaryList.add(symbolData);

			if ((counter + 1) % 10000 == 0 || (counter + 1) == size) {

				System.out.println("storing the data");
				LOGGER.info("storing the data into database");
				tradeDetailsRepo.saveAll(temporaryList);

				LOGGER.info("inserted the data into database");

				temporaryList.clear();
			}

			counter++;
		}

	}

}

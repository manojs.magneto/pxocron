package com.magneto.pxocron.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magneto.pxocron.entities.ChartDetails;
import com.magneto.pxocron.entities.RelativeStrength;
import com.magneto.pxocron.repositories.Generationcharts;
import com.magneto.pxocron.repositories.RelativeStrengthOutput;


@Service
public class ChartDetailsBatchInsertion {
	
	@Autowired
	Generationcharts chartRepo;

	private static final Logger LOGGER = LoggerFactory.getLogger(ChartDetailsBatchInsertion.class);
	@Transactional
	public void chartStorage(List<ChartDetails> rsiList) {

		int size = rsiList.size();
		int counter = 0;

		List<ChartDetails> temporaryList = new ArrayList<>();

		for (ChartDetails relativeStrength : rsiList) {
			temporaryList.add(relativeStrength);

			if ((counter + 1) % 10 == 0 || (counter + 1) == size) {

				System.out.println("storing the data");
				LOGGER.info("storing the data into database");
				chartRepo.flush();

				chartRepo.saveAll(temporaryList);
				chartRepo.flush();

				LOGGER.info("inserted the data into database");

				temporaryList.clear();

			}

			counter++;
		}

		System.out.println(" chart details done storing the data");

	}

	public List<ChartDetails> findAllByDate(String date) {
		return chartRepo.findAllByDate(date);	
	}

}

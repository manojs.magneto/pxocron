package com.magneto.pxocron.services;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.controllers.PxoController;
import com.magneto.pxocron.dto.IndicatorIntermediate;
import com.magneto.pxocron.entities.MACDCalculatorOutput;
import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.utils.CurrentDateWeekendChecker;
import com.magneto.pxocron.utils.HolidaysChecker;
import com.magneto.pxocron.utils.SingleDateChecker;
import com.magneto.pxocron.utils.WeekendChecker;

@Service
public class MacdStockEntryPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(MacdStockEntryPoint.class);

	@Autowired
	SymbolDataRepo symbolDataRepo;

	@Autowired
	MACDCalculatorBatchInsertion batchInsertion;
	List<SymbolData> symbolDataList;

	Calendar checkCalendar = Calendar.getInstance();
	Calendar checkCalendar1 = Calendar.getInstance();

	Calendar checkCalendar2 = Calendar.getInstance();

	Calendar startCalender = Calendar.getInstance();
	Calendar endCalender = Calendar.getInstance();

	Calendar startCalenderema12 = Calendar.getInstance();
	Calendar endCalenderema12 = Calendar.getInstance();

	Calendar startCalender26 = Calendar.getInstance();
	Calendar endCalender26 = Calendar.getInstance();
	Calendar startCalender12Normal = Calendar.getInstance();
	Calendar endCalender12Normal = Calendar.getInstance();
	Calendar startCalender26Normal = Calendar.getInstance();
	Calendar endCalender26Normal = Calendar.getInstance();
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	Map<String, Double> closeAverage;
	Map<String, Double> ema12;
	Map<String, Integer> result;

	Map<String, Double> ema12LastEntry;

	Map<String, Double> ema26LastEntry;

	Map<String, Double> ema26;

	Map<String, Double> lastMacdLine;
	Map<String, Double> histogram;

	Map<String, Map<String, Double>> ema12Entries;

	Map<String, Map<String, Double>> ema26Entries;

	Map<String, Map<String, Double>> macdEntries;

	Map<String, List<Double>> macdLine;
	Map<String, Double> signalLine;

	private final double EMA_12_factor = 0.1538;
	private final double EMA_26_factor = 0.0741;

	Map<String, Double> ema12Temp;
	Map<String, Double> ema26Temp;

	String lastKey = null;
	WeekendChecker weekendChecker = new WeekendChecker();
	CurrentDateWeekendChecker weekendCheckeDater = new CurrentDateWeekendChecker();
	Calendar tempCalender = Calendar.getInstance();
	Calendar storableDate = Calendar.getInstance();

	public IndicatorIntermediate emaCalculation(HashMap<String, Object> paramaeters) throws ParseException {
		dateFixer(paramaeters);

		ema12();
		ema26();
		ema12NormalCalculation();
		ema26NormalCalculations();
		lastMacdLine();
		return signalLine();
		// return null;

		// finalMacdOutput();

	}

	private Map<String, Double> finalMacdOutput() {
		//
		return lastMacdLine;
	}

	public void ema12() throws ParseException {

		ema12LastEntry = new LinkedHashMap<>();

		ema26LastEntry = new LinkedHashMap<>();

		ema26 = new LinkedHashMap<>();

		lastMacdLine = new LinkedHashMap<>();
		histogram = new LinkedHashMap<>();

		ema12Entries = new LinkedHashMap<>();

		macdLine = new LinkedHashMap<>();
		signalLine = new LinkedHashMap<>();
		closeAverage = new LinkedHashMap<>();
		ema12 = new LinkedHashMap<>();
		result = new LinkedHashMap<>();

		checkCalendar1.add(Calendar.DATE, -1);

		LOGGER.info(" before the checkcalendrer1 calender {} values", checkCalendar1.getTime());

		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(endCalenderema12, checkCalendar1);
		// checkCalendar1 = tempCalender.getInstance();
		// weekendCheckeDater.weekendCheckerDate(endCalenderema12);

		// weekendChecker.weekendChecker(startCalenderema12, endCalenderema12);
		// startCalenderema12.add(Calendar.DATE, -21);

		// startCalenderema12.add(Calendar.DATE, -12);

		LOGGER.info(" before startCalenderema12 calender {} values", startCalenderema12.getTime());

		LOGGER.info(" after endCalenderema12 calender {} values", endCalenderema12.getTime());

		Date tempDate = endCalenderema12.getTime();
		startCalenderema12.setTime(tempDate);
		startCalenderema12.add(Calendar.DATE, -12);

		// startCalenderema12.add(Calendar.DATE, -21);

		int count1 = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(startCalenderema12, endCalenderema12);

		LOGGER.info("startCalenderema12 calender {} values", startCalenderema12.getTime());

		LOGGER.info("endCalenderema12 calender {} values", endCalenderema12.getTime());

		// fetching the symbols data
		symbolDataList = symbolDataRepo.findAllByEodDateBetween(dateFormat.format(startCalenderema12.getTime()),
				dateFormat.format(endCalenderema12.getTime()));

		LOGGER.info("symbolDataList {} values", symbolDataList);

		Map<String, List<Double>> mapList = symbolDataList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol,
				Collectors.mapping(SymbolData::getClose, Collectors.toList())));

		LOGGER.info("mapList {} values", mapList);

		closeAverage = mapList.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey,
						e -> e.getValue().stream().map(x -> Double.parseDouble(String.valueOf(x)))
								.mapToDouble(Double::doubleValue).average().getAsDouble()));

		List<SymbolData> todayList = symbolDataRepo.findAllByEodDate(dateFormat.format(endCalenderema12.getTime()));

		LOGGER.info("closeAverage ema12 {} values", closeAverage);

		LOGGER.info("todayList {} values", todayList);

		Map<String, Double> groupedTodayData = todayList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol))
				.entrySet().stream().collect(Collectors.toMap(map -> map.getKey(),
						map -> Double.valueOf(map.getValue().stream().findFirst().get().getClose())));

		LOGGER.info("groupedTodayData {} values", groupedTodayData);

		closeAverage.forEach((symbol, averageclose) -> {
			// yes, we can put logic here
			// $D$2*(B36-D35)+D35

			if (groupedTodayData.containsKey(symbol)) {
				ema12.put(symbol, closeAverage.get(symbol));

				ema12Entries.put(dateFormat.format(endCalenderema12.getTime()), ema12);

			}

		});

		// LOGGER.info("calenderema12 {} values",
		// dateFormat.format(startCalenderema12.getTime()));

		LOGGER.info("ema12Entries {} values", ema12Entries);

		startCalenderema12 = Calendar.getInstance();

		endCalenderema12 = Calendar.getInstance();

	}

	public void ema26() throws ParseException {
		ema26Entries = new LinkedHashMap<>();
		macdEntries = new LinkedHashMap<>();

		checkCalendar2.add(Calendar.DATE, -1);

		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(endCalender26, checkCalendar2);

		LOGGER.info("startCalenderema26  before calender {} values", startCalender26.getTime());

		LOGGER.info("endCalenderema26 after calender {} values", endCalender26.getTime());

		// checkCalendar2=tempCalender.getInstance();

		Date tempDate = endCalender26.getTime();
		startCalender26.setTime(tempDate);

		startCalender26.add(Calendar.DATE, -26);

		int count1 = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(startCalender26, endCalender26);

		LOGGER.info("startCalenderema26 calender {} values", startCalender26.getTime());

		LOGGER.info("endCalenderema26 calender {} values", endCalender26.getTime());
		// fetching the symbols data
		symbolDataList = symbolDataRepo.findAllByEodDateBetween(dateFormat.format(startCalender26.getTime()),
				dateFormat.format(endCalender26.getTime()));

		Map<String, List<Double>> mapList = symbolDataList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol,
				Collectors.mapping(SymbolData::getClose, Collectors.toList())));

		closeAverage = mapList.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey,
						e -> e.getValue().stream().map(x -> Double.parseDouble(String.valueOf(x)))
								.mapToDouble(Double::doubleValue).average().getAsDouble()));
		List<SymbolData> todayList = symbolDataRepo.findAllByEodDate(dateFormat.format(endCalender26.getTime()));

		LOGGER.info("closeAverage ema26 {} values", closeAverage);

		Map<String, Double> groupedTodayData = todayList.stream().collect(Collectors.groupingBy(SymbolData::getSymbol))
				.entrySet().stream().collect(Collectors.toMap(map -> map.getKey(),
						map -> Double.valueOf(map.getValue().stream().findFirst().get().getClose())));

		closeAverage.forEach((symbol, averageclose) -> {
			// yes, we can put logic here
			// $D$2*(B36-D35)+D35

			if (groupedTodayData.containsKey(symbol)) {
				ema26.put(symbol, closeAverage.get(symbol));

				ema26Entries.put(dateFormat.format(endCalender26.getTime()), ema26);

			}

		});

		// LOGGER.info("calenderpeakema26 {} values",
		// dateFormat.format(startCalender.getTime()));

		LOGGER.info("ema26Entries  general{} values", ema26Entries);

		startCalender26 = Calendar.getInstance();
		endCalender26 = Calendar.getInstance();
	}

	public void ema12NormalCalculation() throws ParseException {

		// checkCalendar1.add(Calendar.DATE, -1);
		System.out.println(" before start calender ema12 normal value is" + endCalender12Normal.getTime());

		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(endCalender12Normal, checkCalendar);
		// checkCalendar1=tempCalender.getInstance();
		System.out.println("after start calender ema12 normal value is" + endCalender12Normal.getTime());

		while (checkCalendar.after(endCalender12Normal)) {

			if (endCalender12Normal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
					|| endCalender12Normal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
					|| HolidaysChecker.holidaysList().contains(dateFormat.format(endCalender12Normal.getTime())))

			{
				LOGGER.info("startCalenderema12 calender {} values", endCalender12Normal.getTime());

				System.out.println("start calender ema12 normal value is" + endCalender12Normal.getTime());

				LOGGER.info("EMA NORMAL CALCUATIONS SKIPPING THE DATES values");

				endCalender12Normal.add(Calendar.DATE, 1);

				continue;
			}

			// weekendCheckeDater.weekendCheckerDate(endCalender12Normal);

			// weekendChecker.weekendChecker(startCalender12Normal, endCalender12Normal);
			// startCalender12Normal.add(Calendar.DATE, -20);

			List<SymbolData> todayList = symbolDataRepo
					.findAllByEodDate(dateFormat.format(endCalender12Normal.getTime()));

			// LOGGER.info("for todayList values {} values", todayList);

			Map<String, Double> groupedTodayData = todayList.stream()
					.collect(Collectors.groupingBy(SymbolData::getSymbol)).entrySet().stream()
					.collect(Collectors.toMap(map -> map.getKey(),
							map -> Double.valueOf(map.getValue().stream().findFirst().get().getClose())));

			// LOGGER.info("for groupedTodayData12 values {} values", groupedTodayData);

			for (Entry<String, Map<String, Double>> entry : ema12Entries.entrySet()) {
				lastKey = entry.getKey();
			}

			ema12Temp = ema12Entries.get(lastKey);

			ema12 = new HashMap<>();

			// LOGGER.info("for 8 values {} values", i);

			groupedTodayData.forEach((symbol, value) -> {

				if (ema12Temp != null && ema12Temp.containsKey(symbol)) {
					ema12.put(symbol, EMA_12_factor * (groupedTodayData.get(symbol) - ema12Temp.get(symbol))
							+ ema12Temp.get(symbol));

					// LOGGER.info("each ema12 {} values", ema12);

					ema12Entries.put(dateFormat.format(endCalender12Normal.getTime()), ema12);

				}
			});
			// LOGGER.info("each ema12 date values {}
			// values",dateFormat.format(endCalender.getTime()));

			// LOGGER.info("each ema12 8 values {} values",ema12);
			// LOGGER.info("ema12 values {} values", ema12);

			if (ema12.size() > 1) {
				ema12LastEntry = ema12;
			}

			endCalender12Normal.add(Calendar.DATE, +1);

		}
		// LOGGER.info("ema12Entriesnormalcalculations {} values", ema12Entries);

		// LOGGER.info("ema12 values {} values", ema12);

		// LOGGER.info("ema12LastEntry values {} values", ema12LastEntry);

		LOGGER.info("ema12Entries daily values{} values", ema12Entries);
		LOGGER.info("latest ema12 value{} values", ema12);

		LOGGER.info("ema12latestetry value{} values", ema12LastEntry);

		startCalender12Normal = Calendar.getInstance();
		endCalender12Normal = Calendar.getInstance();
	}

	public void ema26NormalCalculations() throws ParseException {

		// checkCalendar1.add(Calendar.DATE, -1);
		System.out.println(" before start calender ema26 normal value is" + endCalender26Normal.getTime());

		int count = HolidaysAnWeekendChecker.holidaysAndWeekendChecking(endCalender26Normal, checkCalendar);
		checkCalendar1 = tempCalender.getInstance();
		System.out.println("after start calender ema26 normal value is" + endCalender26Normal.getTime());

		while (checkCalendar.after(endCalender26Normal)) {

			if (endCalender26Normal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
					|| endCalender26Normal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
					|| HolidaysChecker.holidaysList().contains(dateFormat.format(endCalender26Normal.getTime())))

			{

				endCalender26Normal.add(Calendar.DATE, 1);

				continue;
			}

			// weekendCheckeDater.weekendCheckerDate(endCalender26Normal);

			// weekendChecker.weekendChecker(startCalender26Normal, endCalender26Normal);
			// startCalender12Normal.add(Calendar.DATE, -20);

			List<SymbolData> todayList = symbolDataRepo
					.findAllByEodDate(dateFormat.format(endCalender26Normal.getTime()));

			// LOGGER.info("for todayList values {} values", todayList);

			Map<String, Double> groupedTodayData = todayList.stream()
					.collect(Collectors.groupingBy(SymbolData::getSymbol)).entrySet().stream()
					.collect(Collectors.toMap(map -> map.getKey(),
							map -> Double.valueOf(map.getValue().stream().findFirst().get().getClose())));

			// LOGGER.info("for groupedTodayData12 values {} values", groupedTodayData);

			for (Entry<String, Map<String, Double>> entry : ema26Entries.entrySet()) {
				lastKey = entry.getKey();
			}

			ema26Temp = ema26Entries.get(lastKey);

			ema26 = new HashMap<>();

			// LOGGER.info("for 8 values {} values", i);

			groupedTodayData.forEach((symbol, value) -> {

				if (ema26Temp != null && ema26Temp.containsKey(symbol)) {
					ema26.put(symbol, EMA_26_factor * (groupedTodayData.get(symbol) - ema26Temp.get(symbol))
							+ ema26Temp.get(symbol));

					ema26Entries.put(dateFormat.format(endCalender26Normal.getTime()), ema26);

				}
			});
			// LOGGER.info("each ema12 date values {}
			// values",dateFormat.format(endCalender.getTime()));

			// LOGGER.info("each ema12 8 values {} values",ema12);
			LOGGER.info("ema26 values {} values", ema26);
			String currentKey = dateFormat.format(endCalender26Normal.getTime());
			if (ema26.size() > 1) {
				ema26LastEntry = ema26;
			}
			macdNormalCalculation(currentKey, ema12Entries, ema26Entries);

			endCalender26Normal.add(Calendar.DATE, +1);

		}
		LOGGER.info("ema26Entriesnormalcalculations {} values", ema26Entries);

		// LOGGER.info("ema26 values {} values", ema26);

		LOGGER.info("ema12LastEntry values {} values", ema12LastEntry);

		LOGGER.info("ema26LastEntry values {} values", ema26LastEntry);

		LOGGER.info("macdLine  all calculated  {} values", macdLine);

		startCalender26Normal = Calendar.getInstance();
		endCalender26Normal = Calendar.getInstance();

	}

	public void macdNormalCalculation(String currentKey, Map<String, Map<String, Double>> ema12Entries2,
			Map<String, Map<String, Double>> ema26Entries2) {

		LOGGER.info("each value of currentKey  {} values", currentKey);

		Map<String, Double> ema12 = ema12Entries.get(currentKey);

		Map<String, Double> ema26 = ema26Entries2.get(currentKey);

		LOGGER.info("each entryvalue ema12  all calculated  {} values", ema12);
		LOGGER.info("each entryvalue ema26 all calculated  {} values", ema26);

		if (ema12 != null && ema26 != null) {
			ema12.forEach((symbol, value) -> {

				if (ema12.containsKey(symbol) && ema26.containsKey(symbol))

				{

					if (macdLine.containsKey(symbol))

					{

						macdLine.get(symbol).add(ema12.get(symbol) - ema26.get(symbol));

					}

					else {

						macdLine.put(symbol, new ArrayList<>(Arrays.asList(ema12.get(symbol) - ema26.get(symbol))));

					}
				}

			});

		}

		LOGGER.info("each entryvalue ema12  all calculated  {} values", ema12);
		LOGGER.info("each MACD VALUE  {} values", macdLine);

	}

	public void lastMacdLine() {

		ema26LastEntry.forEach((symbol, value) -> {

			lastMacdLine.put(symbol, (ema12LastEntry.get(symbol) - ema26LastEntry.get(symbol)));

		});
		LOGGER.info("lastMacdLine {} values", lastMacdLine);

	}

	public IndicatorIntermediate signalLine() {

		signalLine = macdLine.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
				e -> e.getValue().stream().mapToDouble(Double::doubleValue).average().getAsDouble()));

		LOGGER.info("signalLine {} values", signalLine);
		histogram();
		result();
		return storableMacd();

	}

	public void histogram() {
		String currentKey = dateFormat.format(endCalender.getTime());

		lastMacdLine.forEach((symbol, value) -> {

			if (signalLine.containsKey(symbol))
				histogram.put(symbol, (lastMacdLine.get(symbol) - signalLine.get(symbol)));

		});

	}

	public void result() {
		lastMacdLine.forEach((symbol, value) -> {

			if (signalLine.containsKey(symbol))

			{

				if (lastMacdLine.get(symbol) > signalLine.get(symbol))

					result.put(symbol, 1);

				else
					result.put(symbol, 2);
			}

		});

	}

	public IndicatorIntermediate storableMacd() {
		List<MACDCalculatorOutput> macdList = new ArrayList<>();

		histogram.forEach((k, v) ->

		{

			MACDCalculatorOutput macdCalculatorOutput = new MACDCalculatorOutput();

			macdCalculatorOutput.setEodDate(dateFormat.format(storableDate.getTime()));
			macdCalculatorOutput.setEma12(ema12LastEntry.get(k));
			macdCalculatorOutput.setEma26(ema26LastEntry.get(k));
			macdCalculatorOutput.setSignalLine(signalLine.get(k));
			macdCalculatorOutput.setMacdLine(lastMacdLine.get(k));
			macdCalculatorOutput.setMacdHistogram(histogram.get(k));
			macdCalculatorOutput.setSymbol(k);
			// macdCalculatorOutput.setResult(result.get(k));
			macdCalculatorOutput.setColor(result.get(k));
			macdList.add(macdCalculatorOutput);

		}

		);

		batchInsertion.saveMacd(macdList);

		IndicatorIntermediate response = new IndicatorIntermediate();
		response.setOriginalValues(lastMacdLine);
		response.setColors(result);

		return response;

	}

	public void dateFixer(HashMap<String, Object> paramaeters) throws ParseException {
		Date date;
		String fixedDate = (String) paramaeters.get("date");

		if (fixedDate == null) {
			date = Calendar.getInstance().getTime();
		} else {
			date = date = dateFormat.parse(fixedDate);
		}

		startCalenderema12.setTime(date);
		endCalenderema12.setTime(date);
		endCalender12Normal.setTime(date);
		startCalender26.setTime(date);
		endCalender26.setTime(date);
		endCalender26Normal.setTime(date);
		checkCalendar1.setTime(date);
		checkCalendar2.setTime(date);
		tempCalender.setTime(date);
		storableDate.setTime(date);
		checkCalendar.setTime(date);

		// it runned on 24
		// TODO Auto-generated method stub
		// startCalenderema12.add(Calendar.DATE, -21);
		endCalenderema12.add(Calendar.DATE, -9);

		endCalender12Normal.add(Calendar.DATE, -8);
		// startCalender26.add(Calendar.DATE, -35);
		endCalender26Normal.add(Calendar.DATE, -8);
		endCalender26.add(Calendar.DATE, -9);

		storableDate.add(Calendar.DATE, -1);
		LOGGER.info("in setter endCalenderema12 value{} values", endCalenderema12.getTime());
		LOGGER.info("in setter endCalender26 value{} values", endCalender26.getTime());

		LOGGER.info("in setter endCalender12Normal value{} values", endCalender12Normal.getTime());

		LOGGER.info("in setter endCalender26Normal value{} values", endCalender26Normal.getTime());

		// HolidaysAnWeekendChecker.singleWeekendChecker(endCalenderema12);
		// HolidaysAnWeekendChecker.singleWeekendChecker(endCalender12Normal);
		// HolidaysAnWeekendChecker.singleWeekendChecker(endCalender26);
		// HolidaysAnWeekendChecker.singleWeekendChecker(endCalender26Normal);
		HolidaysAnWeekendChecker.singleWeekendChecker(storableDate);

	}

}

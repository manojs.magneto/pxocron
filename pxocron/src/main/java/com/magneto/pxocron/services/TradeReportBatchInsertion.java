package com.magneto.pxocron.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.entities.TradeDetails;
import com.magneto.pxocron.entities.TradeReport;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.repositories.TradeDetailsRepo;
import com.magneto.pxocron.repositories.TradeReportRepo;

//stock data batch insertion which increases the performance 

@Service
public class TradeReportBatchInsertion { 

	@Autowired
	TradeReportRepo tradeReportRepo;

	private static final Logger LOGGER = LoggerFactory.getLogger(TradeReportBatchInsertion.class);

	@Transactional
	public void saveTradeReportsBatch(List<TradeReport> tradeReportList) {
		int size = tradeReportList.size();
		int counter = 0;

		List<TradeReport> temporaryList = new ArrayList<>();

		for (TradeReport tradeReport : tradeReportList) {
			temporaryList.add(tradeReport);

			if ((counter + 1) % 10000 == 0 || (counter + 1) == size) {

				System.out.println("storing the data");
				LOGGER.info("storing the data into database");
				tradeReportRepo.saveAll(temporaryList);

				LOGGER.info("inserted the data into database");

				temporaryList.clear();
			}

			counter++;
		}

	}

}

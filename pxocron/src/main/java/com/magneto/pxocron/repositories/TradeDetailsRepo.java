package com.magneto.pxocron.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.entities.TradeDetails;

@Repository
public interface TradeDetailsRepo extends JpaRepository<TradeDetails,Long> {

	List<TradeDetails> findAllByDate(String date);
	
	
	
	
	

}

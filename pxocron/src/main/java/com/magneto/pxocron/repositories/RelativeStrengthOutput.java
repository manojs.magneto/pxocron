package com.magneto.pxocron.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.entities.RelativeStrength;
import com.magneto.pxocron.entities.StochasticOutput;


@Repository
public interface RelativeStrengthOutput  extends JpaRepository<RelativeStrength,Integer> {

	List<RelativeStrength> findAllByEodDate(String date);
	
	
	@Query(value = "from RelativeStrength t where date BETWEEN :startDate AND :endDate")
	public List<RelativeStrength> getAllBetweenEodDates(@Param("startDate")String startDate,@Param("endDate")String endDate);
	

}
 
package com.magneto.pxocron.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.entities.StochasticOutput;
import com.magneto.pxocron.entities.SymbolData;

@Repository

public interface StochasticCalculatorOutputRepo extends JpaRepository<StochasticOutput, Integer> {

	@Query(value = "from StochasticOutput t where date BETWEEN :startDate AND :endDate")
	public List<StochasticOutput> getAllBetweenEodDates(@Param("startDate") String startDate,
			@Param("endDate") String endDate);

	List<StochasticOutput> findAllByDate(String date);

	@Query(value = "from StochasticOutput t where date BETWEEN :startDate AND :endDate")

	public List<StochasticOutput> getAllBetweenDates(@Param("startDate") String startDate,
			@Param("endDate") String endDate);

}

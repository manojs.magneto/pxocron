package com.magneto.pxocron.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.dto.StockDetails;
import com.magneto.pxocron.entities.CronDetailsUs;
import com.magneto.pxocron.entities.SymbolData;

@Repository
public interface SymbolDataRepo extends JpaRepository<SymbolData, Long> {

	List<SymbolData> findAllByEodDate(String date);

	List<SymbolData> findAllByEodDateOrderByEodDateAsc(Date publicationDate);

	List<SymbolData> findAllByEodDateOrderByIdAsc(Date publicationDate);
	//@EntityGraph(attributePaths = { "chartDetails" })

	@EntityGraph(attributePaths = { "stochastic","rsi","macd","chartDetails","adrOutput"})
	List<SymbolData> findAllByEodDateBetween(String string, String string2);

	@Query(value = "from SymbolData t where date BETWEEN :startDate AND :endDate")
	public List<SymbolData> getAllBetweenEodDates(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(value = "from SymbolData t where date BETWEEN :startDate AND :endDate")
	public List<SymbolData> getAllBetweenEodDatesOrderBySymbolAsc(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate);
	
	
	SymbolData findTopByOrderByEodDateDesc();

}

package com.magneto.pxocron.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.entities.CronDetailsUs;
import com.magneto.pxocron.entities.MACDCalculatorOutput;


@Repository
public interface DailyCronDetailsRepo  extends JpaRepository<CronDetailsUs,Integer> {

	CronDetailsUs findTopByOrderByDateDesc();


}

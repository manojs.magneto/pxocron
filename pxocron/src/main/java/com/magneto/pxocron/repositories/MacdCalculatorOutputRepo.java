package com.magneto.pxocron.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.entities.MACDCalculatorOutput;
import com.magneto.pxocron.entities.RelativeStrength;
import com.magneto.pxocron.entities.SymbolData;

@Repository
public interface MacdCalculatorOutputRepo  extends JpaRepository<MACDCalculatorOutput,Integer>{

	
	List<MACDCalculatorOutput> findAllByEodDate(String date);

}

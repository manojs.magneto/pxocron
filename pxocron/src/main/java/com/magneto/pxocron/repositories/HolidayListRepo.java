package com.magneto.pxocron.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.entities.HolidayList;

@Repository
public interface HolidayListRepo extends JpaRepository<HolidayList, Integer> {
	
	

}

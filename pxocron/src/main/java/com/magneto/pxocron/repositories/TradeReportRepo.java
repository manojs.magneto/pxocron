package com.magneto.pxocron.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.entities.TradeReport;


@Repository
public interface TradeReportRepo extends JpaRepository<TradeReport,Long> {

	List<TradeReport> findAllByDate(String date);

}
 
package com.magneto.pxocron.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.entities.ADROutput;
import com.magneto.pxocron.entities.CronDetailsUs;
@Repository
public interface ADRRepo  extends JpaRepository<ADROutput,Long>{

}

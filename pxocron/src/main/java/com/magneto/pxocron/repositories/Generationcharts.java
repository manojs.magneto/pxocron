package com.magneto.pxocron.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.entities.ChartDetails;
import com.magneto.pxocron.entities.StochasticOutput;

@Repository
public interface Generationcharts extends JpaRepository<ChartDetails, Long> {

	List<ChartDetails> findAllByDate(String date);
	@EntityGraph(attributePaths = { "symbolData"})
	List<ChartDetails> findAllByEodDate(String eodDate);

}

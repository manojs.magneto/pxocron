package com.magneto.pxocron.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.magneto.pxocron.dto.StockDetails;
import com.magneto.pxocron.entities.Symbol;
import com.magneto.pxocron.entities.SymbolData;


@Repository
public interface SymbolRepo extends JpaRepository<SymbolData,Integer>{
	
	
	@Query( value="select symbol from Symbol  WHERE exchange = 'NYSE' OR exchange = 'NASDAQ' OR exchange = 'AMEX'")
	List<String> symbolListFromExchange();

	

}

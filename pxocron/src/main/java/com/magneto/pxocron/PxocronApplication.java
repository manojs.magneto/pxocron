package com.magneto.pxocron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.magneto.pxocron.controllers.PxoController;
import com.magneto.pxocron.mappers.StockDetailsCsvToStockDetailsPojoMapper;
@EnableScheduling
@SpringBootApplication
@EnableRetry
@EnableCaching
public class PxocronApplication {
	
	
	
	
    private static final Logger LOGGER=LoggerFactory.getLogger(PxocronApplication.class);


	public static void main(String[] args) {
		SpringApplication.run(PxocronApplication.class, args);
		
		LOGGER.info("*********************************JAVA-VERSION-1.8***********");
		LOGGER.info("*********************************SPRING BOOT SUITE-4.8***********");
		LOGGER.info("*********************************RUNNING ON PORT 8081***********");


		
		
	

	}

}

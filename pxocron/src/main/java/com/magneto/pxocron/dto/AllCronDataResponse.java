package com.magneto.pxocron.dto;

import java.util.List;

import com.magneto.pxocron.entities.CronDetailsUs;

public class AllCronDataResponse {
	int alljobsstatus;
	long alljobsstarttime;
	long alljobsendtime;
	long date;

	List<DataResponse> allCronJobs;

	public int getAlljobsstatus() {
		return alljobsstatus;
	}

	public void setAlljobsstatus(int alljobsstatus) {
		this.alljobsstatus = alljobsstatus;
	}

	public long getAlljobsstarttime() {
		return alljobsstarttime;
	}

	public void setAlljobsstarttime(long alljobsstarttime) {
		this.alljobsstarttime = alljobsstarttime;
	}

	public long getAlljobsendtime() {
		return alljobsendtime;
	}

	public void setAlljobsendtime(long alljobsendtime) {
		this.alljobsendtime = alljobsendtime;
	}

	public List<DataResponse> getAllCronJobs() {
		return allCronJobs;
	}

	public void setAllCronJobs(List<DataResponse> allCronJobs) {
		this.allCronJobs = allCronJobs;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

}

package com.magneto.pxocron.dto.entrinoapi_2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"name",
"mic",
"acronym",
"city",
"country",
"country_code",
"website",
"first_stock_price_date",
"last_stock_price_date"
})
public class StockExchange {

@JsonProperty("id")
private String id;
@JsonProperty("name")
private String name;
@JsonProperty("mic")
private String mic;
@JsonProperty("acronym")
private String acronym;
@JsonProperty("city")
private Object city;
@JsonProperty("country")
private String country;
@JsonProperty("country_code")
private String countryCode;
@JsonProperty("website")
private Object website;
@JsonProperty("first_stock_price_date")
private String firstStockPriceDate;
@JsonProperty("last_stock_price_date")
private String lastStockPriceDate;

@JsonProperty("id")
public String getId() {
return id;
}

@JsonProperty("id")
public void setId(String id) {
this.id = id;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("mic")
public String getMic() {
return mic;
}

@JsonProperty("mic")
public void setMic(String mic) {
this.mic = mic;
}

@JsonProperty("acronym")
public String getAcronym() {
return acronym;
}

@JsonProperty("acronym")
public void setAcronym(String acronym) {
this.acronym = acronym;
}

@JsonProperty("city")
public Object getCity() {
return city;
}

@JsonProperty("city")
public void setCity(Object city) {
this.city = city;
}

@JsonProperty("country")
public String getCountry() {
return country;
}

@JsonProperty("country")
public void setCountry(String country) {
this.country = country;
}

@JsonProperty("country_code")
public String getCountryCode() {
return countryCode;
}

@JsonProperty("country_code")
public void setCountryCode(String countryCode) {
this.countryCode = countryCode;
}

@JsonProperty("website")
public Object getWebsite() {
return website;
}

@JsonProperty("website")
public void setWebsite(Object website) {
this.website = website;
}

@JsonProperty("first_stock_price_date")
public String getFirstStockPriceDate() {
return firstStockPriceDate;
}

@JsonProperty("first_stock_price_date")
public void setFirstStockPriceDate(String firstStockPriceDate) {
this.firstStockPriceDate = firstStockPriceDate;
}

@JsonProperty("last_stock_price_date")
public String getLastStockPriceDate() {
return lastStockPriceDate;
}

@JsonProperty("last_stock_price_date")
public void setLastStockPriceDate(String lastStockPriceDate) {
this.lastStockPriceDate = lastStockPriceDate;
}

}
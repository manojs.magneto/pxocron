package com.magneto.pxocron.dto;

import java.util.List;

import org.springframework.http.ResponseEntity;

public class AllCronResponse {

	String alljobsstatus;
	long alljobsstarttime;
	long alljobsendtime;

	List<Object> allCronJobs;

	public String getAlljobsstatus() {
		return alljobsstatus;
	}

	public void setAlljobsstatus(String alljobsstatus) {
		this.alljobsstatus = alljobsstatus;
	}

	public long getAlljobsstarttime() {
		return alljobsstarttime;
	}

	public void setAlljobsstarttime(long alljobsstarttime) {
		this.alljobsstarttime = alljobsstarttime;
	}

	public long getAlljobsendtime() {
		return alljobsendtime;
	}

	public void setAlljobsendtime(long alljobsendtime) {
		this.alljobsendtime = alljobsendtime;
	}

	public List<Object> getAllCronJobs() {
		return allCronJobs;
	}

	public void setAllCronJobs(List<Object> allCronJobs) {
		this.allCronJobs = allCronJobs;
	}


	
	
	

}

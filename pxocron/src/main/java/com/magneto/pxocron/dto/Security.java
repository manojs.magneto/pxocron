package com.magneto.pxocron.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"company_id",
"stock_exchange_id",
"name",
"code",
"currency",
"ticker",
"composite_ticker",
"figi",
"composite_figi",
"share_class_figi",
"primary_listing"
})
public class Security {

@JsonProperty("id")
private String id;
@JsonProperty("company_id")
private String companyId;
@JsonProperty("stock_exchange_id")
private String stockExchangeId;
@JsonProperty("name")
private String name;
@JsonProperty("code")
private String code;
@JsonProperty("currency")
private String currency;
@JsonProperty("ticker")
private String ticker;
@JsonProperty("composite_ticker")
private String compositeTicker;
@JsonProperty("figi")
private String figi;
@JsonProperty("composite_figi")
private String compositeFigi;
@JsonProperty("share_class_figi")
private String shareClassFigi;
@JsonProperty("primary_listing")
private Boolean primaryListing;

@JsonProperty("id")
public String getId() {
return id;
}

@JsonProperty("id")
public void setId(String id) {
this.id = id;
}

@JsonProperty("company_id")
public String getCompanyId() {
return companyId;
}

@JsonProperty("company_id")
public void setCompanyId(String companyId) {
this.companyId = companyId;
}

@JsonProperty("stock_exchange_id")
public String getStockExchangeId() {
return stockExchangeId;
}

@JsonProperty("stock_exchange_id")
public void setStockExchangeId(String stockExchangeId) {
this.stockExchangeId = stockExchangeId;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("code")
public String getCode() {
return code;
}

@JsonProperty("code")
public void setCode(String code) {
this.code = code;
}

@JsonProperty("currency")
public String getCurrency() {
return currency;
}

@JsonProperty("currency")
public void setCurrency(String currency) {
this.currency = currency;
}

@JsonProperty("ticker")
public String getTicker() {
return ticker;
}

@JsonProperty("ticker")
public void setTicker(String ticker) {
this.ticker = ticker;
}

@JsonProperty("composite_ticker")
public String getCompositeTicker() {
return compositeTicker;
}

@JsonProperty("composite_ticker")
public void setCompositeTicker(String compositeTicker) {
this.compositeTicker = compositeTicker;
}

@JsonProperty("figi")
public String getFigi() {
return figi;
}

@JsonProperty("figi")
public void setFigi(String figi) {
this.figi = figi;
}

@JsonProperty("composite_figi")
public String getCompositeFigi() {
return compositeFigi;
}

@JsonProperty("composite_figi")
public void setCompositeFigi(String compositeFigi) {
this.compositeFigi = compositeFigi;
}

@JsonProperty("share_class_figi")
public String getShareClassFigi() {
return shareClassFigi;
}

@JsonProperty("share_class_figi")
public void setShareClassFigi(String shareClassFigi) {
this.shareClassFigi = shareClassFigi;
}

@JsonProperty("primary_listing")
public Boolean getPrimaryListing() {
return primaryListing;
}

@JsonProperty("primary_listing")
public void setPrimaryListing(Boolean primaryListing) {
this.primaryListing = primaryListing;
}

}
package com.magneto.pxocron.dto;

import java.util.Map;

import com.magneto.pxocron.entities.SymbolData;

public class IndicatorIntermediate {
	
	
	Map<String,Double> originalValues;
	Map<String,Integer> colors;
	Map<String,SymbolData> ids;
	public Map<String, Double> getOriginalValues() {
		return originalValues;
	}
	public void setOriginalValues(Map<String, Double> originalValues) {
		this.originalValues = originalValues;
	}
	
	public Map<String, SymbolData> getIds() {
		return ids;
	}
	public void setIds(Map<String, SymbolData> ids) {
		this.ids = ids;
	}
	public Map<String, Integer> getColors() {
		return colors;
	}
	public void setColors(Map<String, Integer> colors) {
		this.colors = colors;
	}
	

	
	
	

}

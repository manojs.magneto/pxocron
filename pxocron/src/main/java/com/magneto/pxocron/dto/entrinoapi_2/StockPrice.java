package com.magneto.pxocron.dto.entrinoapi_2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"date",
"intraperiod",
"frequency",
"open",
"high",
"low",
"close",
"volume",
"adj_open",
"adj_high",
"adj_low",
"adj_close",
"adj_volume",
"security"
})
public class StockPrice {

@JsonProperty("date")
private String date;
@JsonProperty("intraperiod")
private Boolean intraperiod;
@JsonProperty("frequency")
private String frequency;
@JsonProperty("open")
private Float open;
@JsonProperty("high")
private Float high;
@JsonProperty("low")
private Float low;
@JsonProperty("close")
private Float close;
@JsonProperty("volume")
private Integer volume;
@JsonProperty("adj_open")
private Double adjOpen;
@JsonProperty("adj_high")
private Double adjHigh;
@JsonProperty("adj_low")
private Double adjLow;
@JsonProperty("adj_close")
private Double adjClose;
@JsonProperty("adj_volume")
private Integer adjVolume;
@JsonProperty("security")
private Security security;

@JsonProperty("date")
public String getDate() {
return date;
}

@JsonProperty("date")
public void setDate(String date) {
this.date = date;
}

@JsonProperty("intraperiod")
public Boolean getIntraperiod() {
return intraperiod;
}

@JsonProperty("intraperiod")
public void setIntraperiod(Boolean intraperiod) {
this.intraperiod = intraperiod;
}

@JsonProperty("frequency")
public String getFrequency() {
return frequency;
}

@JsonProperty("frequency")
public void setFrequency(String frequency) {
this.frequency = frequency;
}


public Float getOpen() {
	return open;
}

public void setOpen(Float open) {
	this.open = open;
}

public Float getHigh() {
	return high;
}

public void setHigh(Float high) {
	this.high = high;
}

public Float getLow() {
	return low;
}

public void setLow(Float low) {
	this.low = low;
}

public Float getClose() {
	return close;
}

public void setClose(Float close) {
	this.close = close;
}

@JsonProperty("volume")
public Integer getVolume() {
return volume;
}

@JsonProperty("volume")
public void setVolume(Integer volume) {
this.volume = volume;
}

@JsonProperty("adj_open")
public Double getAdjOpen() {
return adjOpen;
}

@JsonProperty("adj_open")
public void setAdjOpen(Double adjOpen) {
this.adjOpen = adjOpen;
}

@JsonProperty("adj_high")
public Double getAdjHigh() {
return adjHigh;
}

@JsonProperty("adj_high")
public void setAdjHigh(Double adjHigh) {
this.adjHigh = adjHigh;
}

@JsonProperty("adj_low")
public Double getAdjLow() {
return adjLow;
}

@JsonProperty("adj_low")
public void setAdjLow(Double adjLow) {
this.adjLow = adjLow;
}

@JsonProperty("adj_close")
public Double getAdjClose() {
return adjClose;
}

@JsonProperty("adj_close")
public void setAdjClose(Double adjClose) {
this.adjClose = adjClose;
}

@JsonProperty("adj_volume")
public Integer getAdjVolume() {
return adjVolume;
}

@JsonProperty("adj_volume")
public void setAdjVolume(Integer adjVolume) {
this.adjVolume = adjVolume;
}

@JsonProperty("security")
public Security getSecurity() {
return security;
}

@JsonProperty("security")
public void setSecurity(Security security) {
this.security = security;
}

}
package com.magneto.pxocron.dto.entrinoapi_2;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"stock_prices",
"stock_exchange",
"next_page"
})
public class EntrinoStockVariation {

@JsonProperty("stock_prices")
private List<StockPrice> stockPrices = null;
@JsonProperty("stock_exchange")
private StockExchange stockExchange;
@JsonProperty("next_page")
private String nextPage;

@JsonProperty("stock_prices")
public List<StockPrice> getStockPrices() {
return stockPrices;
}
 
@JsonProperty("stock_prices")
public void setStockPrices(List<StockPrice> stockPrices) {
this.stockPrices = stockPrices;
}

@JsonProperty("stock_exchange")
public StockExchange getStockExchange() {
return stockExchange;
}

@JsonProperty("stock_exchange")
public void setStockExchange(StockExchange stockExchange) {
this.stockExchange = stockExchange;
}

@JsonProperty("next_page")
public String getNextPage() {
return nextPage;
}

@JsonProperty("next_page")
public void setNextPage(String nextPage) {
this.nextPage = nextPage;
}

}
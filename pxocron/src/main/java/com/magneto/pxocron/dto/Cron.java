package com.magneto.pxocron.dto;

import java.util.HashMap;
import java.util.List;

public class Cron {
	
	
	String name;
	HashMap<String,Object> parameters;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public HashMap<String, Object> getParameters() {
		return parameters;
	}
	public void setParameters(HashMap<String, Object> parameters) {
		this.parameters = parameters;
	}
	
	

}

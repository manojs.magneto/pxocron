package com.magneto.pxocron.dto;

import java.util.List;

public class InputStockRequest {
	
	
	private List<Cron> crons;

	public List<Cron> getCrons() {
		return crons;
	}

	public void setCrons(List<Cron> crons) {
		this.crons = crons;
	}
	
	
	
	
}

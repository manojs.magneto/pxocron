package com.magneto.pxocron.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"stock_prices",
"security",
"next_page"
})
public class EntrinoStock {

@JsonProperty("stock_prices")
private List<StockPrice> stockPrices = null;
@JsonProperty("security")
private Security security;
@JsonProperty("next_page")
private String nextPage;

@JsonProperty("stock_prices")
public List<StockPrice> getStockPrices() {
return stockPrices;
}

@JsonProperty("stock_prices")
public void setStockPrices(List<StockPrice> stockPrices) {
this.stockPrices = stockPrices;
}

@JsonProperty("security")
public Security getSecurity() {
return security;
}

@JsonProperty("security")
public void setSecurity(Security security) {
this.security = security;
}

@JsonProperty("next_page")
public String getNextPage() {
return nextPage;
}

@JsonProperty("next_page")
public void setNextPage(String nextPage) {
this.nextPage = nextPage;
}

}
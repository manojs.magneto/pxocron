package com.magneto.pxocron.dto;

public class Response {

	String cron;
	long startTime;
	long endTime;
	String status;
	
	long date;
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public long getEndTime() {
		return endTime;
	}
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public String getCron() {
		return cron;
	}
	public void setCron(String cron) {
		this.cron = cron;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	} 

	
	
}

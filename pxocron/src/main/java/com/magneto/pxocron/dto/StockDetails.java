package com.magneto.pxocron.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.opencsv.bean.CsvBindByName;

import io.github.millij.poi.ss.model.annotations.SheetColumn;


@Entity
@Table(name = "stockexchange")

public class StockDetails {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)


	private Integer firstKey;
    
	private String adjustedClose;
	
	private String close;

	private String code;
	
	private String date;


	private String ex;


	private String high;

	
	private String low;



	private String open;
    
	private String volume;



	

	


	


	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getOpen() {
		return open;
	}
	public void setOpen(String open) {
		this.open = open;
	}
	
	public String getLow() {
		return low;
	}
	public void setLow(String low) {
		this.low = low;
	}
	public String getClose() {
		return close;
	}
	public void setClose(String close) {
		this.close = close;
	}
	public String getAdjustedClose() {
		return adjustedClose;
	}
	public void setAdjustedClose(String adjustedClose) {
		this.adjustedClose = adjustedClose;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	
	
	public Integer getFirstKey() {
		return firstKey;
	}
	public void setFirstKey(Integer firstKey) {
		this.firstKey = firstKey;
	}
	public String getEx() {
		return ex;
	}
	public void setEx(String ex) {
		this.ex = ex;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getHigh() {
		return high;
	}
	public void setHigh(String high) {
		this.high = high;
	}
	@Override
	public String toString() {
		return "StockDetails [firstKey=" + firstKey + ", code=" + code + ", ex=" + ex + ", date=" + date + ", open="
				+ open + ", high=" + high + ", low=" + low + ", close=" + close + ", adjustedClose=" + adjustedClose
				+ ", volume=" + volume + "]";
	}
	
	public StockDetails() {
		// TODO Auto-generated constructor stub
	}
	public StockDetails(Integer firstKey, String adjustedClose, String close, String code, String date, String ex,
			String high, String low, String open, String volume) {
		super();
		this.firstKey = firstKey;
		this.adjustedClose = adjustedClose;
		this.close = close;
		this.code = code;
		this.date = date;
		this.ex = ex;
		this.high = high;
		this.low = low;
		this.open = open;
		this.volume = volume;
	}
	
	
	
	
	
	
	

}

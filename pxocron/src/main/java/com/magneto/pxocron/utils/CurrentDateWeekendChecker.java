package com.magneto.pxocron.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CurrentDateWeekendChecker {
	private static final Logger LOGGER = LoggerFactory.getLogger(WeekendChecker.class);
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	Calendar c1Duplicate = null;

	public void weekendCheckerDate(Calendar c1) {

		int sundays = 0;
		int saturdays = 0;
		int total = 0;

		if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			total = 2;

			LOGGER.info("sundays {} values", sundays);

		}

		if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			total = 1;
			LOGGER.info("saturdays {} values", saturdays);

		}

		c1.add(Calendar.DATE, -total);

		LOGGER.info("Calendar c1 {} values", dateFormat.format(c1.getTime()));
	}

}

package com.magneto.pxocron.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.repositories.HolidayListRepo;

//list of holidays 

//@Service
public class HolidaysChecker {

	//@Autowired
	//HolidayListRepo holidayRepo;

	// @Cacheable("holidaysList")
	public static List<String> holidaysList() {

		//return holidayRepo.findAll().stream().map(holiday -> holiday.getObserved_date()).collect(Collectors.toList());
		
		
		return Arrays.asList("2020-01-01","2020-01-20","2020-02-17","2020-04-10","2020-05-25","2020-07-03","2020-09-07","2020-11-26","2020-12-25","2021-01-01","2021-01-18","2021-02-15","2021-04-02","2021-05-31","2021-07-04","2021-09-06","2021-11-25","2021-12-24","2021-01-29");

	}

}

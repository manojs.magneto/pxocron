package com.magneto.pxocron.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SingleDateChecker {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SingleDateChecker.class);


	static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public static int singleDateChecking(Calendar c1) {

		System.out.println("===================================>");

		System.out.println("the   single entered date " + c1.getTime());

		int sunday = 0;
		int saturday = 0;
		int holidayCount = 0;
		int total = 0;

		while (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
				|| HolidaysChecker.holidaysList().contains(dateFormat.format(c1.getTime())))

		{

			saturday = 0;
			sunday = 0;
			holidayCount = 0;
			if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
				
				saturday++;
				LOGGER.info("the total saturday {} value is", saturday);

			}

			if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
				
				sunday++;
				LOGGER.info("the total sunday {} value is", sunday);

			}

			if (HolidaysChecker.holidaysList().contains(dateFormat.format(c1.getTime()))) {
			

				holidayCount++;
				LOGGER.info("the holidaycount {} value is", holidayCount);

			}

			total = total + (saturday + sunday + holidayCount);

			c1.add(Calendar.DATE, -(saturday + sunday + holidayCount));

		}

		System.out.println("the final  single end calender " + c1.getTime());
		System.out.println("===================================>");

		LOGGER.info("the  final single data checker {} value is", total);
		
		LOGGER.info("the problematic calender  {} value is", dateFormat.format(c1.getTime()));
		return total;
	}

	public static int singleDateChecking(Calendar c1, Calendar tempCalendar) {
		System.out.println("===================================>");

		System.out.println("the   single entered date " + c1.getTime());

		int sunday = 0;
		int saturday = 0;
		int holidayCount = 0;
		int total = 0;

		if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
				|| HolidaysChecker.holidaysList().contains(dateFormat.format(c1.getTime())))

		{

			saturday = 0;
			sunday = 0;
			holidayCount = 0;
			if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
				
				saturday++;
				LOGGER.info("the total saturday {} value is", saturday);

			}

			if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
				
				sunday++;
				LOGGER.info("the total sunday {} value is", sunday);

			}

			if (HolidaysChecker.holidaysList().contains(dateFormat.format(c1.getTime()))) {
			

				holidayCount++;
				LOGGER.info("the holidaycount {} value is", holidayCount);

			}

			total = total + (saturday + sunday + holidayCount);

			tempCalendar.add(Calendar.DATE, -(saturday + sunday + holidayCount));

		}

		System.out.println("the single end calender " + tempCalendar.getTime());
		System.out.println("===================================>");

		LOGGER.info("the single data checker {} value is", total);
		
		
		singleDateChecking(tempCalendar);
		
		
		return total;
	}

}

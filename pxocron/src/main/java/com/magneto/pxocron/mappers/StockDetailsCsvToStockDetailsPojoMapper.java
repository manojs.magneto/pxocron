package com.magneto.pxocron.mappers;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.magneto.pxocron.PxocronApplication;
import com.magneto.pxocron.dao.StockExchangeDao;
import com.magneto.pxocron.dto.EntrinoStock;
import com.magneto.pxocron.dto.StockDetails;
import com.magneto.pxocron.dto.StockPrice;
import com.magneto.pxocron.dto.entrinoapi_2.EntrinoStockVariation;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.repositories.StochasticCalculatorOutputRepo;
import com.magneto.pxocron.repositories.StockDetailsRepo;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.repositories.SymbolRepo;
import com.magneto.pxocron.services.StockBatchInsertion;
import com.magneto.pxocron.utils.StockCode;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;

@Service
public class StockDetailsCsvToStockDetailsPojoMapper {
	String line = "";
	String cvsSplitBy = ",";

	private static final Logger LOGGER = LoggerFactory.getLogger(StockDetailsCsvToStockDetailsPojoMapper.class);
	@Autowired
	StockDetailsRepo repo;
	@Autowired
	StockBatchInsertion stockBatchInsertion;

	@Autowired
	StockExchangeDao stockExchangeDao;

	SimpleDateFormat dateFormat;

	public void csvToPojoConverter(ResponseEntity<byte[]> csvData) throws IOException, ParseException

	{

		dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		List<String> symbolList = stockExchangeDao.symbolList();

		if (csvData != null && csvData.getStatusCodeValue() == 200 && csvData.getBody() != null
				&& csvData.getBody().length > 0) {

			ByteArrayInputStream inputStream = new ByteArrayInputStream(csvData.getBody());

			InputStreamReader is = new InputStreamReader(inputStream);

			BufferedReader br = new BufferedReader(is);

			List<SymbolData> detailsBucket = new ArrayList<SymbolData>();
			line = br.readLine();

			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] sheet = line.split(cvsSplitBy);

				if (!(sheet.length < 2)) {

					System.out.println("stockdata" + " " + sheet[0] + " " + sheet[1] + " " + sheet[2] + " " + sheet[3]
							+ " " + sheet[4] + " " + sheet[5] + " " + sheet[6] + " " + sheet[7] + " " + sheet[8]);

					System.out.println(sheet.length);

					System.out.println(sheet[0]);

					// LOGGER.info("storing in db"+sheet.length);

					SymbolData symbolData = new SymbolData();

					// System.out.println("date is "+dateFormat.format(sheet[2].toString()));

					// String dates=dateFormat.format(sheet[2]);

					// symbolData.setSymbol(sheet[0]);
					// symbolData.setExchange(sheet[1]);
					// symbolData.setEod_date(unixTime(sheet[2]));
					symbolData.setEodDate(sheet[2]);

					symbolData.setOpen(Float.valueOf(sheet[3]));
					symbolData.setHigh(Float.valueOf(sheet[4]));
					symbolData.setLow(Float.valueOf(sheet[5]));
					symbolData.setClose(Float.valueOf(sheet[6]));
					symbolData.setVolume(Float.valueOf(sheet[8]));
					symbolData.setCon_status(1);
					symbolData.setOpt_status(1);
					symbolData.setStatus(1);
					symbolData.setStatusOHLC(1);
					symbolData.setOpt_status(1);
					symbolData.setCreated_on(Instant.now().getEpochSecond());
					detailsBucket.add(symbolData);

				}

			}
 
			System.out.println("intial bucket size" + detailsBucket.size());

			/*
			 * List<SymbolData> filteredDetailsBucket = detailsBucket.parallelStream()
			 * .filter(stock ->
			 * symbolList.contains(stock.getSymbol())).collect(Collectors.toList());
			 * System.out.println("filtered bucket size"+filteredDetailsBucket.size());
			 */

			// datarepo.saveAll(filteredDetailsBucket);
			stockBatchInsertion.saveStockBatch(detailsBucket);

		}

	}

	public void entriApiStorage(ResponseEntity<EntrinoStock> csvData) {
		dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		EntrinoStock stock = csvData.getBody();

		List<SymbolData> symbolDataList = new ArrayList<SymbolData>();

		stock.getStockPrices().stream().forEach(eachStock -> {

			SymbolData symbolData = new SymbolData();

			symbolData.setSymbol(stock.getSecurity().getTicker());
			symbolData.setExchange(1);
			symbolData.setCompany(stock.getSecurity().getName());

			symbolData.setOpen(eachStock.getOpen());
			symbolData.setHigh(eachStock.getHigh());
			symbolData.setLow(eachStock.getLow());
			symbolData.setClose(eachStock.getClose());
			symbolData.setVolume(eachStock.getVolume());
			symbolData.setCon_status(1);
			symbolData.setOpt_status(1);
			symbolData.setStatus(1);
			symbolData.setStatusOHLC(1);
			symbolData.setOpt_status(1);
			symbolData.setEodDate(eachStock.getDate());

			symbolData.setEod_unix(unixTime(eachStock.getDate()));

			symbolDataList.add(symbolData);

		});

		stockBatchInsertion.saveStockBatch(symbolDataList);

	}

	private long unixTime(String date) {
		Date parsedDate = null;
		try {
			parsedDate = dateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long unixTime = (long) parsedDate.getTime() / 1000;
		return unixTime;
	}

	public void entriApiStorageVersion(ResponseEntity<EntrinoStockVariation> csvData) {
		
		dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		EntrinoStockVariation stock = csvData.getBody();

		List<SymbolData> symbolDataList = new ArrayList<SymbolData>();

		stock.getStockPrices().stream().forEach(eachStock->
		
		{
			SymbolData symbolData = new SymbolData();

			symbolData.setSymbol(eachStock.getSecurity().getTicker());
			symbolData.setExchange(1);
			symbolData.setCompany(eachStock.getSecurity().getName());

			symbolData.setOpen(eachStock.getOpen());
			symbolData.setHigh(eachStock.getHigh());
			symbolData.setLow(eachStock.getLow());
			symbolData.setClose(eachStock.getClose());
			symbolData.setVolume(eachStock.getVolume());
			symbolData.setCon_status(1);
			symbolData.setOpt_status(1);
			symbolData.setStatus(1);
			symbolData.setStatusOHLC(1);
			symbolData.setOpt_status(1);
			symbolData.setEodDate(eachStock.getDate());

			symbolData.setEod_unix(unixTime(eachStock.getDate()));

			symbolDataList.add(symbolData);
			
			
			
			
		});

		stockBatchInsertion.saveStockBatch(symbolDataList);
		
		
	}

}

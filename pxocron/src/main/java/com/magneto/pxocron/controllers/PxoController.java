package com.magneto.pxocron.controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.magneto.pxocron.PxocronApplication;
import com.magneto.pxocron.dto.AllCronDataResponse;
import com.magneto.pxocron.dto.AllCronResponse;
import com.magneto.pxocron.dto.Cron;
import com.magneto.pxocron.dto.EntrinoStock;
import com.magneto.pxocron.dto.InputStockRequest;
import com.magneto.pxocron.dto.Mail;
import com.magneto.pxocron.dto.Response;
import com.magneto.pxocron.dto.entrinoapi_2.EntrinoStockVariation;
import com.magneto.pxocron.entities.CronDetailsUs;
import com.magneto.pxocron.entities.SymbolData;
import com.magneto.pxocron.exceptions.CronException;
import com.magneto.pxocron.integration.StockApiIntegration;
import com.magneto.pxocron.mappers.StockDetailsCsvToStockDetailsPojoMapper;
import com.magneto.pxocron.repositories.SymbolDataRepo;
import com.magneto.pxocron.repositories.SymbolRepo;
import com.magneto.pxocron.services.ChartDetailsGeneration;
import com.magneto.pxocron.services.DailyCronDetails;
import com.magneto.pxocron.services.MACDCalculator;
import com.magneto.pxocron.services.MacdStockEntryPoint;
import com.magneto.pxocron.services.RelativeStrengthIndexCalculator;
import com.magneto.pxocron.services.RsiStockEntryPoint;
import com.magneto.pxocron.services.StochasticCalculator;
import com.magneto.pxocron.services.StochasticEntryPoint;
import com.magneto.pxocron.services.cron.cron3.TradeEntryProcess;
import com.magneto.pxocron.services.cron.cron3.TradeExitProcess;
import com.magneto.pxocron.utils.HolidaysChecker;
import com.magneto.pxocron.utils.MailService;

import io.github.millij.poi.SpreadsheetReadException;

@RestController

public class PxoController {
	
	
	@Autowired
	MailService mailService;
	@Autowired
	SymbolDataRepo repo;

	@Autowired
	TradeEntryProcess tradeEntryProcess;
	@Autowired
	TradeExitProcess tradeExitProcess;

	private static final Logger LOGGER = LoggerFactory.getLogger(PxoController.class);

	@Autowired
	StochasticEntryPoint stochasticEntryPoint;
	@Autowired

	DailyCronDetails dailyCrons;
	@Autowired
	RsiStockEntryPoint rsiStockEntrPoint;
	@Autowired
	MacdStockEntryPoint macdStockEntryPoint;
	@Autowired
	RelativeStrengthIndexCalculator relativeStrength;

	@Autowired
	StochasticCalculator stoCal;

	@Autowired
	SymbolRepo symbolRepo;

	@Autowired
	StockDetailsCsvToStockDetailsPojoMapper csvMapper;

	@Autowired
	StockApiIntegration stockApi;

	@Autowired
	MACDCalculator madeCalculator;

	@Autowired
	ChartDetailsGeneration chartDetails;

	@Autowired
	DailyCronDetails cronDetails;

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	CompletableFuture<Void> future1;
	CompletableFuture<Void> future2;
	CompletableFuture<Void> future3;
	CompletableFuture<Void> future4;
	CompletableFuture<Void> future5;
	ResponseEntity<Object> cronResponse;
	@Value("#{'${symbolslist}'.split(',')}")
	private List<String> list;

	Calendar calender = Calendar.getInstance();
	Calendar currentCalender = Calendar.getInstance();

	@Value("${intrinolatestdata}")

	private String intrinolatestdata;

	@Value("${intrinodatespecific}")

	private String intrinodatespecific;

	@Value("${intrinodateandsymbolspecific}")

	private String intrinodateandsymbolspecific;

	@Value("${intrinosymbolspecific}")

	private String intrinosymbolspecific;

	// rest endpoint stock execution running different jobs for different input
	// parameters
	@RequestMapping(value = "/stockData", method = RequestMethod.GET)

	public ResponseEntity<Object> stockCalculations(@RequestBody Cron inputRequest) throws IOException, ParseException, CronException {

	//Boolean value= validateDateforSkip(inputRequest);
		
		Boolean value=Boolean.TRUE;
	
	
	if(value.equals(Boolean.TRUE))
	{
		
		switch (inputRequest.getName()) { 
		case "all":

			cronResponse = runallCronJobs(inputRequest.getParameters(), 1);

			break;
		case "cron1":

			cronResponse = runCron1Job(inputRequest.getParameters(), 0);

			break;
		case "cron2":

			cronResponse = cron2Selector(inputRequest.getParameters(), 0);
			break;

		case "cron3":
			cronResponse = runCron3Job(inputRequest.getParameters());

		}
		
	}
	
	else
		
	{
		 return new ResponseEntity<>("PLEASE CHECK THE DATE YOU ARE EXECUTING CRONS FOR ,IT MIGHT HAVE WEEKEND OR HOLIDAY", HttpStatus.BAD_REQUEST);
	}

		return cronResponse;

	}
	
	@RequestMapping(value = "/email", method = RequestMethod.GET)

	public void mailcheck() throws IOException, ParseException, CronException {

		Mail mail = new Mail();
        mail.setMailFrom("manoj.suravarapu@gmail.com");
        mail.setMailTo("manoj.suravarapu@gmail.com");
        mail.setMailSubject("Spring Boot - Email Example");
        mail.setMailContent("Learn How to send Email using Spring Boot!!!\n\nThanks\nwww.technicalkeeda.com");
        
        mailService.sendEmail(mail);
	}

	// running cron jobs via schedular mechanism which involves multi threading
	// processing

	private Boolean validateDateforSkip(Cron inputRequest) throws ParseException {
	
		Date date;
		Boolean value=Boolean.TRUE;
		String fixedDate = (String)inputRequest.getParameters().get("date");

		if (fixedDate == null) {
			date = Calendar.getInstance().getTime();
		} else {
			date = dateFormat.parse(fixedDate);
		}

		calender.setTime(date);
		
		if (calender.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY||calender.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY||HolidaysChecker.holidaysList().contains(dateFormat.format(calender.getTime()))) {
			
			value=Boolean.FALSE;

		}

		
		
		//Date date1=calender.getInstance().getTime();
		//Date date2=currentCalender.getInstance().getTime();
		
		
		
		return value;
		
		
	}

	private ResponseEntity<Object> runCron3Job(HashMap<String, Object> parameters) {
		// TODO Auto-generated method stub

		tradeEntryProcess.tradeEntryProcess();
		tradeExitProcess.targetProfitAndstopLossCalculation();
		tradeExitProcess.exitCalculationforLongTrades();
		
		return null;

	}

	private ResponseEntity<Object> cron2Selector(HashMap<String, Object> paramaeters, int i) throws ParseException, CronException {

		// TODO Auto-generated method stub

		ResponseEntity<Object> finalcron2Response = null;
		
		
		//Boolean value=cron2ExecutionValidation(paramaeters);
		Boolean value=Boolean.TRUE;
		
		if(value.equals(Boolean.TRUE))
		{

		if (paramaeters.get("cron2Selector").equals("smaSelector")) {

			finalcron2Response = runCron2forSma(i, paramaeters);

		}
		if (paramaeters.get("cron2Selector").equals("emaSelector")) {
			finalcron2Response = runcron2forEma(i, paramaeters);

		}
		
		}
		
		else
		{
			
			 return new ResponseEntity<>("PLEASE EXECUTE THE CRON1 FIRST", HttpStatus.BAD_REQUEST);
			
			//throw new CronException();
		}

		return finalcron2Response;
	}

	@Retryable(value = RuntimeException.class, maxAttempts = 3)

	private ResponseEntity<Object> runCron1Job(HashMap<String, Object> hashMap, int i) throws IOException {

		CronDetailsUs cronDetails = new CronDetailsUs();
		cronDetails.setCron1_start(Instant.now().getEpochSecond());

		LOGGER.info("inside the cron1 parameterised method");

		String response;

		long start = Instant.now().getEpochSecond();

		LOGGER.info("cron stockApiResponse started");

		ArrayList<String> list = (ArrayList) hashMap.get("symbolList");

		String startDate = (String) (hashMap.get("startDate"));

		String endDate = (String) hashMap.get("endDate");

//symbolspecific
		if (list != null && (startDate == null && endDate == null))

		{
			String modifiedUrl = intrinosymbolspecific.replaceAll("currentdate",
					dateFormat.format(Calendar.getInstance().getTime()));

			for (String stock : list) {

				String finalUrl = modifiedUrl.replaceAll("replacethis", stock);

				ResponseEntity<EntrinoStock> stockApiResponse = stockApi.intrinoRestApiCall(finalUrl);

				csvMapper.entriApiStorage(stockApiResponse);

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
		// date and symbols specific

		if (list != null && (startDate != null && endDate != null))

		{

			LOGGER.info("inside the date and symbol parameterised method");

			String modifiedUrl1 = intrinodateandsymbolspecific.replaceAll("startDate", startDate);

			String modifiedUrl2 = modifiedUrl1.replaceAll("endDate", endDate);

			for (String stock : list) {

				String modifiedUrl3 = modifiedUrl2.replaceAll("replacethis", stock);
				ResponseEntity<EntrinoStock> stockApiResponse = stockApi.intrinoRestApiCall(modifiedUrl3);

				csvMapper.entriApiStorage(stockApiResponse);

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
//date specific
		if (list == null && (startDate != null && endDate != null))

		{
			String modifiedUrl = intrinodatespecific + startDate;

			ResponseEntity<EntrinoStockVariation> stockApiResponse = stockApi.intrinoRestApiVersionCall(modifiedUrl);

			csvMapper.entriApiStorageVersion(stockApiResponse);

		}
//latest date
		if (list == null && (startDate == null && endDate == null))

		{

			ResponseEntity<EntrinoStockVariation> stockApiResponse = stockApi
					.intrinoRestApiVersionCall(intrinolatestdata);

			csvMapper.entriApiStorageVersion(stockApiResponse);

		}

		long end = Instant.now().getEpochSecond();
		response = "success";

		cronDetails.setCron1_end(end);
		cronDetails.setCron1_status(2);
		cronDetails.setDate(Instant.now().getEpochSecond());

		if (i == 0) {
			dailyCrons.savingCronDetails(cronDetails);
		}

		Response cronResponse = new Response();
		cronResponse.setStartTime(start);
		cronResponse.setEndTime(end);
		cronResponse.setStatus(response);
		cronResponse.setCron("cron1");

		AllCronResponse responseall = new AllCronResponse();
		List<Object> responseList = Arrays.asList(cronResponse);

		responseall.setAllCronJobs(responseList);
		responseall.setAlljobsstarttime(start);
		responseall.setAlljobsendtime(end);
		responseall.setAlljobsstatus("success");
		return new ResponseEntity<>(responseall, HttpStatus.OK);

	}

	private ResponseEntity<Object> runallCronJobs(HashMap<String, Object> hashMap, int i) throws IOException, ParseException, CronException {
		long start = System.currentTimeMillis();

		CronDetailsUs cronDetails = new CronDetailsUs();

		cronDetails.setCron1_start(Instant.now().getEpochSecond());

		ResponseEntity<Object> cron1response = runCron1Job(hashMap, i);

		cronDetails.setCron1_end(Instant.now().getEpochSecond());

		cronDetails.setCron2_start(Instant.now().getEpochSecond());
		ResponseEntity<Object> cron2response = cron2Selector(hashMap, i);

		cronDetails.setCron2_end(Instant.now().getEpochSecond());

		cronDetails.setDate(Instant.now().getEpochSecond());
		dailyCrons.savingCronDetails(cronDetails);

		AllCronResponse cronResponse1 = (AllCronResponse) cron1response.getBody();
		AllCronResponse cronResponse2 = (AllCronResponse) cron2response.getBody();

		Response response1 = new Response();

		Response recievedResponse1 = (Response) cronResponse1.getAllCronJobs().get(0);
		Response recievedResponse2 = (Response) cronResponse2.getAllCronJobs().get(0);

		response1.setEndTime(cronResponse1.getAlljobsendtime());
		response1.setStartTime(cronResponse1.getAlljobsstarttime());
		response1.setStatus(cronResponse1.getAlljobsstatus());
		response1.setCron(recievedResponse1.getCron());

		Response response2 = new Response();

		response2.setEndTime(cronResponse2.getAlljobsendtime());
		response2.setStartTime(cronResponse2.getAlljobsstarttime());
		response2.setStatus(cronResponse2.getAlljobsstatus());
		response2.setCron(recievedResponse2.getCron());

		List<Object> responseList = Arrays.asList(response1, response2);

		long end = System.currentTimeMillis();
		AllCronResponse responseall = new AllCronResponse();

		responseall.setAllCronJobs(responseList);
		responseall.setAlljobsstarttime(start);
		responseall.setAlljobsendtime(end);
		responseall.setAlljobsstatus("success");
		return new ResponseEntity<>(responseall, HttpStatus.OK);

	}

	@Retryable(value = { RuntimeException.class }, maxAttempts = 3)

	private ResponseEntity<Object> runCron2forSma(int i, HashMap<String, Object> paramaeters)
			throws ParseException, CronException {

		Boolean value=cron2ExecutionValidation(paramaeters);
		
		

		String response;
		CronDetailsUs cronDetails = new CronDetailsUs();

		long start = System.currentTimeMillis();

		try {

			cronDetails.setCron2_start(Instant.now().getEpochSecond());

			chartDetails.chartGenerationForIndicatorEntryPoints(paramaeters);

			cronDetails.setCron2_end(Instant.now().getEpochSecond());
			cronDetails.setCron2_status(2);
			cronDetails.setDate(Instant.now().getEpochSecond());

			if (i == 0) {

				dailyCrons.savingCronDetails(cronDetails);
			}

		} catch (Exception e) { // TODO Auto-generated catch block
			LOGGER.error("exception while executing the cron2 job", e);
			response = "failure";
			long end = System.currentTimeMillis();

			Response cronResponse = new Response();
			cronResponse.setStartTime(start);
			cronResponse.setEndTime(end);
			cronResponse.setStatus(response);
			cronResponse.setCron(response);
			cronResponse.setCron("cron2");
			AllCronResponse responseall = new AllCronResponse();
			List<Object> responseList = Arrays.asList(cronResponse);

			responseall.setAllCronJobs(responseList);
			responseall.setAlljobsstarttime(start);
			responseall.setAlljobsendtime(end);
			responseall.setAlljobsstatus("success");
			return new ResponseEntity<>(responseall, HttpStatus.BAD_REQUEST);

		}

		long end = System.currentTimeMillis();
		response = "success";

		Response cronResponse = new Response();
		cronResponse.setStartTime(start);
		cronResponse.setEndTime(end);
		cronResponse.setStatus(response);
		cronResponse.setCron("cron2");

		AllCronResponse responseall = new AllCronResponse();
		List<Object> responseList = Arrays.asList(cronResponse);

		responseall.setAllCronJobs(responseList);
		responseall.setAlljobsstarttime(start);
		responseall.setAlljobsendtime(end);
		responseall.setAlljobsstatus("success");
		return new ResponseEntity<>(responseall, HttpStatus.OK);

	}

	private Boolean cron2ExecutionValidation(HashMap<String, Object> paramaeters) throws ParseException, CronException {
		Date date;
		Boolean value=Boolean.TRUE;
		String fixedDate = (String) paramaeters.get("date");

		if (fixedDate == null) {
			date = Calendar.getInstance().getTime();
		} else {
			date = dateFormat.parse(fixedDate);
		}

		calender.setTime(date);
		
		Date date1=calender.getInstance().getTime();
		Date date2=currentCalender.getInstance().getTime();
		
		

		if (dateFormat.format(date1).equals(dateFormat.format(date2))) {

			SymbolData symbolData = repo.findTopByOrderByEodDateDesc();

			String cron1LatestDate = symbolData.getEodDate();

			if (cron1LatestDate.equals(fixedDate)) {
				
				
				value= Boolean.TRUE;
				
				

			}

			else {
				value= Boolean.FALSE;

			}

		}
		return value;
	}

	private ResponseEntity<Object> runcron2forEma(int i, HashMap<String, Object> paramaeters) {

		String response;

		long start = System.currentTimeMillis();
		CronDetailsUs cronDetails = new CronDetailsUs();

		try {
			cronDetails.setCron2_start(Instant.now().getEpochSecond());
			// stoCal.stochasticHightLowCalculation();
			chartDetails.chartGenerationForNormalIndicators(paramaeters);

			cronDetails.setCron2_end(Instant.now().getEpochSecond());
			cronDetails.setCron2_status(2);
			cronDetails.setDate(Instant.now().getEpochSecond());

			if (i == 0) {

				dailyCrons.savingCronDetails(cronDetails);
			}

			// macdStockEntryPoint.emaCalculation();
			// stochasticEntryPoint.stochasticHighLowCalculation();
			// rsiStockEntrPoint.rsiCalculation();

		} catch (Exception e) { // TODO Auto-generated catch block
			LOGGER.error("exception while executing the cron2 job", e);
			response = "failure";
			long end = System.currentTimeMillis();

			Response cronResponse = new Response();
			cronResponse.setStartTime(start);
			cronResponse.setEndTime(end);
			cronResponse.setStatus(response);
			cronResponse.setCron(response);
			cronResponse.setCron("cron2");
			AllCronResponse responseall = new AllCronResponse();
			List<Object> responseList = Arrays.asList(cronResponse);

			responseall.setAllCronJobs(responseList);
			responseall.setAlljobsstarttime(start);
			responseall.setAlljobsendtime(end);
			responseall.setAlljobsstatus("success");
			return new ResponseEntity<>(responseall, HttpStatus.BAD_REQUEST);

		}

		long end = System.currentTimeMillis();
		response = "success";

		Response cronResponse = new Response();
		cronResponse.setStartTime(start);
		cronResponse.setEndTime(end);
		cronResponse.setStatus(response);
		cronResponse.setCron("cron2");

		AllCronResponse responseall = new AllCronResponse();
		List<Object> responseList = Arrays.asList(cronResponse);

		responseall.setAllCronJobs(responseList);
		responseall.setAlljobsstarttime(start);
		responseall.setAlljobsendtime(end);
		responseall.setAlljobsstatus("success");
		return new ResponseEntity<>(responseall, HttpStatus.OK);

	}

	// cron job intializer also checks the list of holidays and also executes for
	// only monday to friday at specific time.

	@Scheduled(cron = "0 0/25 0/12 * * MON-FRI ")
	public void weekendsAndHolidayChecker() throws Exception {

		Date date = new Date();

		String todayDate = dateFormat.format(date);

		LOGGER.info("cron job started at {}", todayDate);

		HolidaysChecker holidaysChecker = new HolidaysChecker();
		List<String> holidayList = holidaysChecker.holidaysList();

		boolean exists = holidayList.stream().anyMatch(holiday -> holiday.equals(todayDate));

		if (!exists) {

			runallCronJobs(new HashMap<>(), 1);
		}

	}

	
	@GetMapping("/stockData/cronDetails")
	public AllCronDataResponse retrieveCronDetails() {

		return cronDetails.findAll();
	}

}
